package com.worktool.serverapi.logwork.user.activity;

/**
 * Created by THAIHOANG on 11/6/2017.
 */

import com.google.gson.annotations.SerializedName;

/**
 {
 "sessionId": "c643ef5a-34df-4c4c-a1ac-26c253e66202",
 "empId": "0010",
 "checkInTime": "2017-11-02T16:46:45.965Z",
 "checkOutTime": "2017-11-02T16:47:59.911Z",
 "sessionName": "CA2",
 "fullName": "Tester 03",
 "text": "2017-11-02 23:47:59: Tester 03 vừa ra ca làm",
 "action": "checkout"
 },
 {
 "sessionId": "70ca964c-b0b3-4a1c-9934-4e36827a4d67",
 "empId": "0015",
 "checkInTime": "2017-11-02T16:30:59.566Z",
 "sessionName": "CA2",
 "fullName": "Ngo Dien",
 "text": "2017-11-06 21:40:49: Ngo Dien vừa vào ca làm",
 "action": "checkin"
 }
 */
public class RecentActivity {
    @SerializedName("sessionId")
    private String mSessionId;
    @SerializedName("empId")
    private String mEmpId;
    @SerializedName("checkInTime")
    private String mCheckInTime;
    @SerializedName("checkOutTime")
    private String mCheckOutTime;
    @SerializedName("sessionName")
    private String mSessionName;
    @SerializedName("fullName")
    private String mFullName;
    @SerializedName("text")
    private String mText;
    @SerializedName("action")
    private String mAction;

    public String getSessionId() {
        return mSessionId;
    }

    public void setSessionId(String sessionId) {
        mSessionId = sessionId;
    }

    public String getEmpId() {
        return mEmpId;
    }

    public void setEmpId(String empId) {
        mEmpId = empId;
    }

    public String getCheckInTime() {
        return mCheckInTime;
    }

    public void setCheckInTime(String checkInTime) {
        mCheckInTime = checkInTime;
    }

    public String getCheckOutTime() {
        return mCheckOutTime;
    }

    public void setCheckOutTime(String checkOutTime) {
        mCheckOutTime = checkOutTime;
    }

    public String getSessionName() {
        return mSessionName;
    }

    public void setSessionName(String sessionName) {
        mSessionName = sessionName;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getAction() {
        return mAction;
    }

    public void setAction(String action) {
        mAction = action;
    }
}
