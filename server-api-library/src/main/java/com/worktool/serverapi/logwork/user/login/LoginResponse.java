package com.worktool.serverapi.logwork.user.login;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 {
 "id": "HBhbpF3U5bMi14LJbhBLjWf5XFl6jDI7gySJoIhQIefa01De6NwG2FZPe7yaeVYU",
 "ttl": 1209600,
 "created": "2017-10-07T05:41:33.028Z",
 "userId": "string",
 "roles": ["admin"]
 }
 */
public class LoginResponse {
    @SerializedName("id")
    private String id;
    @SerializedName("ttl")
    private long ttl;
    @SerializedName("created")
    private String created;
    @SerializedName("userId")
    private String userId;
    @SerializedName("roles")
    private List<String> roles;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTtl() {
        return ttl;
    }

    public void setTtl(long ttl) {
        this.ttl = ttl;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
