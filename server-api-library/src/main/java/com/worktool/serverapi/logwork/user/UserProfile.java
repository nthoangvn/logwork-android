package com.worktool.serverapi.logwork.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

/**
 {
 "empId": "string",
 "fullName": "Tester 01",
 "empType": "string",
 "branch": "HCM",
 "empRole": "string",
 "wageLevel": "string",
 "benefits": {},
 "avatarUrl": "string",
 "phoneNumber": "123456",
 "address": "HCM",
 "createdAt": "2017-10-07T02:50:02.976Z",
 "updatedAt": "2017-10-07T02:50:02.976Z",
 "username": "tester01",
 "email": "tester01@gmail.com",
 "emailVerified": false
 }
 */
public class UserProfile implements Parcelable {
    @SerializedName("empId")
    private String empId;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("empType")
    private String empType;
    @SerializedName("branch")
    private String mBranch;
    @SerializedName("empRole")
    private String empRole;
    @SerializedName("wageLevel")
    private String wageLevel; // Optional
    @SerializedName("benefits")
    private List<String> benefits; // Optional
    @SerializedName("profilePicture")
    private String profilePicture; // Optional
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("address")
    private String address;
    @SerializedName("startWorkAt")
    private String startWorkAt; // Optional
    @SerializedName("endProbationAt")
    private String endProbationAt; // Optional
    @SerializedName("birthDay")
    private String birthDay; // Optional
    @SerializedName("createdAt")
    private String createdAt; // Optional
    @SerializedName("updatedAt")
    private String updatedAt; // Optional
    @SerializedName("username")
    private String username; // Optional
    @SerializedName("email")
    private String email;
    @SerializedName("emailVerified")
    private String emailVerified; // Optional
    @SerializedName("roles")
    private List<String> roles;

    public UserProfile() {
    }


    protected UserProfile(Parcel in) {
        empId = in.readString();
        fullName = in.readString();
        empType = in.readString();
        mBranch = in.readString();
        empRole = in.readString();
        wageLevel = in.readString();
        benefits = in.createStringArrayList();
        profilePicture = in.readString();
        phoneNumber = in.readString();
        address = in.readString();
        startWorkAt = in.readString();
        endProbationAt = in.readString();
        birthDay = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        username = in.readString();
        email = in.readString();
        emailVerified = in.readString();
        roles = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(empId);
        dest.writeString(fullName);
        dest.writeString(empType);
        dest.writeString(mBranch);
        dest.writeString(empRole);
        dest.writeString(wageLevel);
        dest.writeStringList(benefits);
        dest.writeString(profilePicture);
        dest.writeString(phoneNumber);
        dest.writeString(address);
        dest.writeString(startWorkAt);
        dest.writeString(endProbationAt);
        dest.writeString(birthDay);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(username);
        dest.writeString(email);
        dest.writeString(emailVerified);
        dest.writeStringList(roles);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserProfile> CREATOR = new Creator<UserProfile>() {
        @Override
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        @Override
        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmpType() {
        return empType;
    }

    public void setEmpType(String empType) {
        this.empType = empType;
    }

    public String getEmpRole() {
        return empRole;
    }

    public void setEmpRole(String empRole) {
        this.empRole = empRole;
    }

    public String getWageLevel() {
        return wageLevel;
    }

    public void setWageLevel(String wageLevel) {
        this.wageLevel = wageLevel;
    }

    public List<String> getBenefits() {
        return benefits;
    }

    public void setBenefits(List<String> benefits) {
        this.benefits = benefits;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStartWorkAt() {
        return startWorkAt;
    }

    public void setStartWorkAt(String startWorkAt) {
        this.startWorkAt = startWorkAt;
    }

    public String getEndProbationAt() {
        return endProbationAt;
    }

    public void setEndProbationAt(String endProbationAt) {
        this.endProbationAt = endProbationAt;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(String emailVerified) {
        this.emailVerified = emailVerified;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getBranch() {
        return mBranch;
    }

    public void setBranch(String branch) {
        mBranch = branch;
    }
}
