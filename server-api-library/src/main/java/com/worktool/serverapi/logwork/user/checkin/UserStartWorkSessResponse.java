package com.worktool.serverapi.logwork.user.checkin;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

import com.google.gson.annotations.SerializedName;

/**
 {
 "sessionId": "bd1dc2d6-f0ae-4ca5-a160-fc65565e46df",
 "empId": "string",
 "checkInTime": "2017-10-07T06:03:26.077Z"
 }
 */
public class UserStartWorkSessResponse {
    @SerializedName("sessionId")
    private String sessionId;
    @SerializedName("empId")
    private String empId;
    @SerializedName("checkInTime")
    private String checkInTime;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }
}
