package com.worktool.serverapi.logwork.jobposition;

/**
 * Created by THAIHOANG on 11/13/2017.
 */

import com.google.gson.annotations.SerializedName;

/**
 {
 "title": "Nhân viên pha chế",
 "id": "59f2b440db823a3e849a21f8"
 }
 */
public class JobPosition {
    @SerializedName("title")
    private String mTitle;
    @SerializedName("id")
    private String mId;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }
}
