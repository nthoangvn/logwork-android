package com.worktool.serverapi.logwork.workscheduler;

/**
 * Created by THAIHOANG on 11/15/2017.
 */

import com.google.gson.annotations.SerializedName;

/**
 {
 "weekId": "string",
 "readOnly": false,
 }
 */
public class SubmitWorkSchedulerRequest {
    @SerializedName("weekId")
    private String mWeekId;
    @SerializedName("readOnly")
    private boolean mReadOnly;

    public String getWeekId() {
        return mWeekId;
    }

    public void setWeekId(String weekId) {
        mWeekId = weekId;
    }

    public boolean isReadOnly() {
        return mReadOnly;
    }

    public void setReadOnly(boolean readOnly) {
        mReadOnly = readOnly;
    }
}
