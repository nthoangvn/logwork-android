package com.worktool.serverapi.logwork.user.push;

/**
 * Created by THAIHOANG on 10/23/2017.
 */

import com.google.gson.annotations.SerializedName;

/**
 {
 "firebaseToken": "string",
 "empId": "string",
 "createdAt": "2017-10-23T15:47:14.819Z",
 "updatedAt": "2017-10-23T15:47:14.819Z"
 }
 */
public class RegisterNotificationData {
    @SerializedName("firebaseToken")
    private String mFirebaseToken;
    @SerializedName("empId")
    private String mEmpId;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("updatedAt")
    private String mUpdatedAt;

    public String getFirebaseToken() {
        return mFirebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        mFirebaseToken = firebaseToken;
    }

    public String getEmpId() {
        return mEmpId;
    }

    public void setEmpId(String empId) {
        mEmpId = empId;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }
}
