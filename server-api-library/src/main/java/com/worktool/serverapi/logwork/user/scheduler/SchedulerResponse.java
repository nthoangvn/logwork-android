package com.worktool.serverapi.logwork.user.scheduler;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by THAIHOANG on 10/8/2017.
 */

public class SchedulerResponse {
    @SerializedName("weekId")
    private String mWeekId;
    @SerializedName("userId")
    private String mUserId;
    @SerializedName("fullName")
    private String mFullName;
    @SerializedName("workScheduler")
    private SchedulerData mWorkScheduler;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("updatedAt")
    private String mUpdatedAt;
    @SerializedName("id")
    private String mId;
    @SerializedName("readOnly")
    private boolean mReadOnly;

    public String getWeekId() {
        return mWeekId;
    }

    public void setWeekId(String mWeekId) {
        this.mWeekId = mWeekId;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String mFullName) {
        this.mFullName = mFullName;
    }

    public SchedulerData getWorkScheduler() {
        return mWorkScheduler;
    }

    public void setWorkScheduler(SchedulerData mWorkScheduler) {
        this.mWorkScheduler = mWorkScheduler;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public boolean isReadOnly() {
        return mReadOnly;
    }

    public void setReadOnly(boolean mReadOnly) {
        this.mReadOnly = mReadOnly;
    }
}
