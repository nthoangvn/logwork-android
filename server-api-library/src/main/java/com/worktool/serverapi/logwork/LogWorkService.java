package com.worktool.serverapi.logwork;

import com.worktool.serverapi.logwork.benefit.Benefit;
import com.worktool.serverapi.logwork.jobposition.JobPosition;
import com.worktool.serverapi.logwork.notification.Notification;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.worktool.serverapi.logwork.user.activity.RecentActivity;
import com.worktool.serverapi.logwork.user.checkin.AdminStartWorkSessRequest;
import com.worktool.serverapi.logwork.user.checkin.StartWorkSessRequest;
import com.worktool.serverapi.logwork.user.checkin.UserStartWorkSessResponse;
import com.worktool.serverapi.logwork.user.checkout.AdminStopSessionRequest;
import com.worktool.serverapi.logwork.user.checkout.StopWorkSessResponse;
import com.worktool.serverapi.logwork.user.login.LoginRequest;
import com.worktool.serverapi.logwork.user.login.LoginResponse;
import com.worktool.serverapi.logwork.user.logout.LogoutResponse;
import com.worktool.serverapi.logwork.user.push.RegisterNotificationData;
import com.worktool.serverapi.logwork.user.push.RegisterNotificationResponse;
import com.worktool.serverapi.logwork.user.push.UnregisterNotificationResponse;
import com.worktool.serverapi.logwork.user.register.RegisterRequest;
import com.worktool.serverapi.logwork.user.scheduler.SchedulerData;
import com.worktool.serverapi.logwork.user.scheduler.SchedulerResponse;
import com.worktool.serverapi.logwork.user.scheduler.UpdateUserSchedulerData;
import com.worktool.serverapi.logwork.user.scheduler.UpdateWorkSchedulerResponse;
import com.worktool.serverapi.logwork.user.scheduler.WeekSchedulerResponse;
import com.worktool.serverapi.logwork.user.session.WorkSession;
import com.worktool.serverapi.logwork.user.session.WorkSessionInfo;
import com.worktool.serverapi.logwork.wage.WageLevel;
import com.worktool.serverapi.logwork.workscheduler.SubmitWorkSchedulerRequest;
import com.worktool.serverapi.logwork.workscheduler.SubmitWorkSchedulerResponse;

import java.util.List;

import okhttp3.Response;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by THAIHOANG on 10/6/2017.
 */

public interface LogWorkService {
    @POST("users")
    Call<UserProfile> userRegister(@Body RegisterRequest registerRequest);

//    @Multipart
//    @POST("users")
//    Call<UserProfile> userRegister(@Part("description") RequestBody description,
//                                   @Part MultipartBody.Part file);

    @POST("users/login")
    Call<LoginResponse> userLogin(@Body LoginRequest loginRequest);

    @POST("users/logout")
    Call<LogoutResponse> userLogout(@Query("access_token") String accessToken);

    @GET("users/me")
    Call<UserProfile> getOwnInfo(@Query("access_token") String accessToken);

    @GET("users")
    Call<List<UserProfile>> getUserList(@Query("access_token") String accessToken);

    @POST("users/{id}/start-work-session")
    Call<UserStartWorkSessResponse> userStartWorkSession(@Path("id") String userId,
                                                         @Query("access_token") String accessToken,
                                                         @Body StartWorkSessRequest startWorkSessRequest);

    @POST("users/{id}/work-session/{sessionId}/end-work-session")
    Call<StopWorkSessResponse> userStopWorkSession(@Path("id") String userId,
                                                   @Path("sessionId") String sessionId,
                                                   @Query("access_token") String accessToken);

    @POST("users/admin-start-work-session")
    Call<UserStartWorkSessResponse> adminStartWorkSession(@Query("access_token") String accessToken,
                                                         @Body AdminStartWorkSessRequest startWorkSessRequest);

    @POST("users/admin-end-work-session")
    Call<StopWorkSessResponse> adminStopWorkSession(@Query("access_token") String accessToken,
                                                    @Body AdminStopSessionRequest stopWorkSessRequest);


    @POST("users/{id}/register-push-token")
    Call<RegisterNotificationResponse> registerNotification(
            @Path("id") String userId,
            @Query("access_token") String accessToken,
            @Body RegisterNotificationData registerNotificationData);

    @DELETE("users/{id}/unregister-push-token/{firebaseToken}")
    Call<UnregisterNotificationResponse> unregisterNotification(
            @Path("id") String userId,
            @Path("firebaseToken") String firebaseToken,
            @Query("access_token") String accessToken
    );

    @GET("Notifications")
    Call<List<Notification>> getNotifications(
            @Query("access_token") String accessToken
    );

    @POST("Notifications")
    Call<Notification> createNotification(
            @Query("access_token") String accessToken,
            @Body Notification newNotification
    );

    @GET("Benefits/{id}")
    Call<Benefit> getBenefit(
            @Path("id") String benefitId,
            @Query("access_token") String accessToken
    );

    @GET("Benefits")
    Call<List<Benefit>> getAllBenefits(
            @Query("access_token") String accessToken
    );

    @GET("WageLevels/{id}")
    Call<WageLevel> getWageLevel(
            @Path("id") String wageId,
            @Query("access_token") String accessToken
    );

    @GET("users/{id}/work-session")
    Call<List<WorkSession>> getWorkSession(
            @Path("id") String userId,
            @Query("access_token") String accessToken);

    @GET("WorkSessions")
    Call<List<WorkSession>> getAllWorkSessions(
            @Query("access_token") String accessToken
    );

    @PUT("WorkSessions/{id}")
    Call<WorkSession> updateWorkSession(
            @Path("id") String sessionId,
            @Query("access_token") String accessToken,
            @Body WorkSession workSessionData
    );

    @GET("WorkSessionInfos")
    Call<List<WorkSessionInfo>> getWorkSessionInfos(
            @Query("access_token") String accessToken
    );

    @GET("users/get-recent-activities")
    Call<List<RecentActivity>> getRecentActivities(
            @Query("access_token") String accessToken
    );

    @POST("users/{id}/register-work-scheduler")
    Call<SchedulerResponse> registerWorkScheduler(
            @Path("id") String userId,
            @Query("access_token") String accessToken,
            @Body SchedulerData schedulerRequest);

    @GET("users/{id}/get-work-scheduler")
    Call<SchedulerResponse> getWorkScheduler(
            @Path("id") String userId,
            @Query("access_token") String accessToken,
            @Query("weekId") String weekId);

    @GET("users/get-work-scheduler/{weekId}")
    Call<WeekSchedulerResponse> getAllWorkSchedulers(
            @Path("weekId") String weekId,
            @Query("access_token") String accessToken);

    @POST("users/update-work-scheduler")
    Call<UpdateWorkSchedulerResponse> updateWorkSchedulers(
            @Query("access_token") String accessToken,
            @Body List<UpdateUserSchedulerData> updateUserSchedulerData);

    @GET("JobPositions")
    Call<List<JobPosition>> getJobPostions(@Query("access_token") String accessToken);

    @PATCH("WorkSchedulers")
    Call<SubmitWorkSchedulerResponse> submitWorkScheduler(
            @Query("access_token") String accessToken,
            @Body SubmitWorkSchedulerRequest submitWorkSchedulerRequest);
}
