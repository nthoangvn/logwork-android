package com.worktool.serverapi.logwork.user.scheduler;

/**
 * Created by THAIHOANG on 10/31/2017.
 */

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 {
 "userId": "0010",
 "fullName": "Tester 03",
 "userWeekScheduler": {
 "weekId": "W42-2017",
 "mon": [
 "CA1"
 ],
 "tue": [
 "CA2"
 ],
 "wed": [
 "CA1",
 "CA2"
 ],
 "thu": [
 "CA2"
 ],
 "fri": [
 "CA2"
 ],
 "sat": [
 "CA2"
 ],
 "sun": [
 "CA1",
 "CA2"
 ]
 }
 }
 */
public class UserSchedulerData {
    @SerializedName("userId")
    private String mUserId;
    @SerializedName("fullName")
    private String mFullName;
    @SerializedName("weekId")
    private String mWeekId;
    @SerializedName("userWeekScheduler")
    private SchedulerData mUserWeekScheduler;

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getWeekId() {
        return mWeekId;
    }

    public void setWeekId(String weekId) {
        mWeekId = weekId;
    }

    public SchedulerData getUserWeekScheduler() {
        return mUserWeekScheduler;
    }

    public void setUserWeekScheduler(SchedulerData userWeekScheduler) {
        mUserWeekScheduler = userWeekScheduler;
    }
}
