package com.worktool.serverapi.logwork.user.session;

/**
 * Created by THAIHOANG on 10/31/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 [
 {
 "name": "CA1",
 "startTime": "06:30",
 "endTime": "14:30",
 "totalAllowWorkHours": 24,
 "isActive": true,
 "createdAt": "2017-10-07T08:49:14.730Z",
 "updatedAt": "2017-10-07T08:49:14.730Z"
 },
 {
 "name": "CA2",
 "startTime": "14:30",
 "endTime": "22:30",
 "totalAllowWorkHours": 32,
 "isActive": true,
 "createdAt": "2017-10-07T08:49:14.730Z",
 "updatedAt": "2017-10-07T08:49:14.730Z"
 }
 ]
 */
public class WorkSessionInfo implements Parcelable {
    @SerializedName("name")
    private String mName;
    @SerializedName("startTime")
    private String mStartTime;
    @SerializedName("endTime")
    private String mEndTime;
    @SerializedName("totalAllowWorkHours")
    private int mTotalAllowWorkHours;
    @SerializedName("isActive")
    private boolean mIsActive;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("updatedAt")
    private String mUpdatedAt;

    protected WorkSessionInfo(Parcel in) {
        mName = in.readString();
        mStartTime = in.readString();
        mEndTime = in.readString();
        mTotalAllowWorkHours = in.readInt();
        mIsActive = in.readByte() != 0;
        mCreatedAt = in.readString();
        mUpdatedAt = in.readString();
    }

    public static final Creator<WorkSessionInfo> CREATOR = new Creator<WorkSessionInfo>() {
        @Override
        public WorkSessionInfo createFromParcel(Parcel in) {
            return new WorkSessionInfo(in);
        }

        @Override
        public WorkSessionInfo[] newArray(int size) {
            return new WorkSessionInfo[size];
        }
    };

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getStartTime() {
        return mStartTime;
    }

    public void setStartTime(String startTime) {
        mStartTime = startTime;
    }

    public String getEndTime() {
        return mEndTime;
    }

    public void setEndTime(String endTime) {
        mEndTime = endTime;
    }

    public int getTotalAllowWorkHours() {
        return mTotalAllowWorkHours;
    }

    public void setTotalAllowWorkHours(int totalAllowWorkHours) {
        mTotalAllowWorkHours = totalAllowWorkHours;
    }

    public boolean isActive() {
        return mIsActive;
    }

    public void setActive(boolean active) {
        mIsActive = active;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mStartTime);
        dest.writeString(mEndTime);
        dest.writeInt(mTotalAllowWorkHours);
        dest.writeByte((byte) (mIsActive ? 1 : 0));
        dest.writeString(mCreatedAt);
        dest.writeString(mUpdatedAt);
    }
}
