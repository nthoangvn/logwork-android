package com.worktool.serverapi.logwork.user.scheduler;

/**
 * Created by THAIHOANG on 10/8/2017.
 */

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 "weekId": "W42-2017",
 "mon": [
 "CA1"
 ],
 "tue": [
 "CA2"
 ],
 "wed": [
 "CA1",
 "CA2"
 ],
 "thu": [
 "CA2"
 ],
 "fri": [
 "CA2"
 ],
 "sat": [
 "CA2"
 ],
 "sun": [
 "CA1",
 "CA2"
 ]
 */
public class SchedulerData {
    @SerializedName("weekId")
    private String mWeekId;
    @SerializedName("mon")
    private List<String> mMonShifts;
    @SerializedName("tue")
    private List<String> mTueShifts;
    @SerializedName("wed")
    private List<String> mWedShifts;
    @SerializedName("thu")
    private List<String> mThuShifts;
    @SerializedName("fri")
    private List<String> mFriShifts;
    @SerializedName("sat")
    private List<String> mSatShifts;
    @SerializedName("sun")
    private List<String> mSunShifts;

    public String getWeekId() {
        return mWeekId;
    }

    public void setWeekId(String weekId) {
        mWeekId = weekId;
    }

    public List<String> getMonShifts() {
        return mMonShifts;
    }

    public void setMonShifts(List<String> mMonShifts) {
        this.mMonShifts = mMonShifts;
    }

    public void addMonShifts(String shiftName) {
        if (mMonShifts == null) {
            mMonShifts = new ArrayList<>();
        }
        mMonShifts.add(shiftName);
    }

    public List<String> getTueShifts() {
        return mTueShifts;
    }

    public void setTueShifts(List<String> mTueShifts) {
        this.mTueShifts = mTueShifts;
    }

    public void addTueShifts(String shiftName) {
        if (mTueShifts == null) {
            mTueShifts = new ArrayList<>();
        }
        mTueShifts.add(shiftName);
    }

    public List<String> getWedShifts() {
        return mWedShifts;
    }

    public void setWedShifts(List<String> mWedShifts) {
        this.mWedShifts = mWedShifts;
    }

    public void addWedShifts(String shiftName) {
        if (mWedShifts == null) {
            mWedShifts = new ArrayList<>();
        }
        mWedShifts.add(shiftName);
    }

    public List<String> getThuShifts() {
        return mThuShifts;
    }

    public void setThuShifts(List<String> mThuShifts) {
        this.mThuShifts = mThuShifts;
    }

    public void addThuShifts(String shiftName) {
        if (mThuShifts == null) {
            mThuShifts = new ArrayList<>();
        }
        mThuShifts.add(shiftName);
    }

    public List<String> getFriShifts() {
        return mFriShifts;
    }

    public void setFriShifts(List<String> mFriShifts) {
        this.mFriShifts = mFriShifts;
    }

    public void addFriShifts(String shiftName) {
        if (mFriShifts == null) {
            mFriShifts = new ArrayList<>();
        }
        mFriShifts.add(shiftName);
    }

    public List<String> getSatShifts() {
        return mSatShifts;
    }

    public void setSatShifts(List<String> mSatShifts) {
        this.mSatShifts = mSatShifts;
    }

    public void addSatShifts(String shiftName) {
        if (mSatShifts == null) {
            mSatShifts = new ArrayList<>();
        }
        mSatShifts.add(shiftName);
    }

    public List<String> getSunShifts() {
        return mSunShifts;
    }

    public void setSunShifts(List<String> mSunShifts) {
        this.mSunShifts = mSunShifts;
    }

    public void addSunShifts(String shiftName) {
        if (mSunShifts == null) {
            mSunShifts = new ArrayList<>();
        }
        mSunShifts.add(shiftName);
    }
}
