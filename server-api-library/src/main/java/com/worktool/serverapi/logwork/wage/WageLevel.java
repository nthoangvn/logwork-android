package com.worktool.serverapi.logwork.wage;

/**
 * Created by THAIHOANG on 10/30/2017.
 */

import com.google.gson.annotations.SerializedName;

/**
 {
 "name": "Bậc 1",
 "amount": 22000,
 "id": "59d6906257b412108c79d09e"
 }
 */
public class WageLevel {
    @SerializedName("name")
    private String mName;
    @SerializedName("amount")
    private long mAmount;
    @SerializedName("id")
    private String mId;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public long getAmount() {
        return mAmount;
    }

    public void setAmount(long amount) {
        mAmount = amount;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }
}
