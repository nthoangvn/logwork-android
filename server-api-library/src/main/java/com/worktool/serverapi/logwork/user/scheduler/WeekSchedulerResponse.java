package com.worktool.serverapi.logwork.user.scheduler;

/**
 * Created by THAIHOANG on 10/31/2017.
 */

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 {
 "weekId": "W42-2017",
 "readOnly": false,
 "createdAt": "2017-10-07T20:28:13.923Z",
 "updatedAt": "2017-10-07T20:28:13.923Z",
 "data": [
 {
 "userId": "0010",
 "fullName": "Tester 03",
 "userWeekScheduler": {
 "weekId": "W42-2017",
 "mon": [
 "CA1"
 ],
 "tue": [
 "CA2"
 ],
 "wed": [
 "CA1",
 "CA2"
 ],
 "thu": [
 "CA2"
 ],
 "fri": [
 "CA2"
 ],
 "sat": [
 "CA2"
 ],
 "sun": [
 "CA1",
 "CA2"
 ]
 }
 }
 ]
 }
 */
public class WeekSchedulerResponse {
    @SerializedName("weekId")
    private String mWeekId;
    @SerializedName("readOnly")
    private boolean mReadOnly;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("updatedAt")
    private String mUpdatedAt;
    @SerializedName("data")
    private List<UserSchedulerData> mData;

    public String getWeekId() {
        return mWeekId;
    }

    public void setWeekId(String weekId) {
        mWeekId = weekId;
    }

    public boolean isReadOnly() {
        return mReadOnly;
    }

    public void setReadOnly(boolean readOnly) {
        mReadOnly = readOnly;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public List<UserSchedulerData> getData() {
        return mData;
    }

    public void setData(List<UserSchedulerData> data) {
        mData = data;
    }
}
