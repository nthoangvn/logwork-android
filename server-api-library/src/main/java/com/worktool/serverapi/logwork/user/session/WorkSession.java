package com.worktool.serverapi.logwork.user.session;

import com.google.gson.annotations.SerializedName;

/**
 * Created by THAIHOANG on 10/15/2017.
 */

/**
 {
 "sessionId": "string",
 "empId": "string",
 "checkInTime": "2017-11-16T23:04:25.360Z",
 "checkOutTime": "2017-11-16T23:04:25.360Z",
 "confirmedCheckInTime": "2017-11-16T23:04:25.360Z",
 "confirmedCheckOutTime": "2017-11-16T23:04:25.360Z",
 "countedWorkTime": 0
 }
 */
public class WorkSession {
    @SerializedName("sessionId")
    private String mSessionId;
    @SerializedName("empId")
    private String mEmpId;
    @SerializedName("checkInTime")
    private String mCheckInTime;
    @SerializedName("checkOutTime")
    private String mCheckOutTime;
    @SerializedName("confirmedCheckInTime")
    private String mConfirmedCheckInTime;
    @SerializedName("confirmedCheckOutTime")
    private String mConfirmedCheckOutTime;
    @SerializedName("totalWorkTime")
    private double mTotalWorkTime; // Deprecated
    @SerializedName("countedWorkTime")
    private long mCountedWorkTime; // In milliseconds
    @SerializedName("sessionName")
    private String mSessionName;

    public String getSessionId() {
        return mSessionId;
    }

    public void setSessionId(String sessionId) {
        mSessionId = sessionId;
    }

    public String getEmpId() {
        return mEmpId;
    }

    public void setEmpId(String empId) {
        mEmpId = empId;
    }

    public String getCheckInTime() {
        return mCheckInTime;
    }

    public void setCheckInTime(String checkInTime) {
        mCheckInTime = checkInTime;
    }

    public String getCheckOutTime() {
        return mCheckOutTime;
    }

    public void setCheckOutTime(String checkOutTime) {
        mCheckOutTime = checkOutTime;
    }

    public double getTotalWorkTime() {
        return mTotalWorkTime;
    }

    public void setTotalWorkTime(double totalWorkTime) {
        mTotalWorkTime = totalWorkTime;
    }

    public long getCountedWorkTime() {
        return mCountedWorkTime;
    }

    public void setCountedWorkTime(long countedWorkTime) {
        mCountedWorkTime = countedWorkTime;
    }

    public String getSessionName() {
        return mSessionName;
    }

    public void setSessionName(String sessionName) {
        mSessionName = sessionName;
    }

    public String getConfirmedCheckInTime() {
        return mConfirmedCheckInTime;
    }

    public void setConfirmedCheckInTime(String confirmedCheckInTime) {
        mConfirmedCheckInTime = confirmedCheckInTime;
    }

    public String getConfirmedCheckOutTime() {
        return mConfirmedCheckOutTime;
    }

    public void setConfirmedCheckOutTime(String confirmedCheckOutTime) {
        mConfirmedCheckOutTime = confirmedCheckOutTime;
    }

    public void copy(WorkSession srcWorkSess) {
        mSessionId = srcWorkSess.getSessionId();
        mEmpId = srcWorkSess.getEmpId();
        mCheckInTime = srcWorkSess.getCheckInTime();
        mCheckOutTime = srcWorkSess.getCheckOutTime();
        mTotalWorkTime = srcWorkSess.getTotalWorkTime();
        mCountedWorkTime = srcWorkSess.getCountedWorkTime();
        mSessionName = srcWorkSess.getSessionName();
        mConfirmedCheckInTime = srcWorkSess.getConfirmedCheckInTime();
        mConfirmedCheckOutTime = srcWorkSess.getConfirmedCheckOutTime();
    }
}
