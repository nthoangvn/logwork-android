package com.worktool.serverapi.logwork;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.worktool.serverapi.BuildConfig;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by THAIHOANG on 10/6/2017.
 */

public class LogWorkApi {
    private static final String TAG = LogWorkApi.class.getSimpleName();
    private static LogWorkApi mApiInstance;
    private Context mContext;
    public static final String SERVER_DOMAIN = "https://logwork-api.sonnguyen.org";
    private static final String BASE_API_URL = SERVER_DOMAIN + "/api/";

    private LogWorkService mApiService;

    public static void init(Context context) {
        mApiInstance = new LogWorkApi(context);
    }

    private LogWorkApi(Context context) {
        mContext = context;
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .create();

        // Build OkHttpClient
        OkHttpClient okHttpClient = buildUnsafeHttpClient();

        Log.d(TAG, "Init retrofit, base URL: " + BASE_API_URL);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mApiService = retrofit.create(LogWorkService.class);
    }

    private OkHttpClient buildHttpClient() {
        Log.d(TAG, "Build OkHttpClient");
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(30000, TimeUnit.MILLISECONDS)
                .connectTimeout(30000, TimeUnit.MILLISECONDS);
        if (BuildConfig.DEBUG) {
            addLogging(builder);
        } else {
            addLogging(builder);
        }
//        enableCache(builder);
        return builder.build();
    }

    private OkHttpClient buildUnsafeHttpClient() {
        Log.d(TAG, "Build unsafe OkHttpClient");
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(30000, TimeUnit.MILLISECONDS)
                .connectTimeout(30000, TimeUnit.MILLISECONDS);
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (BuildConfig.DEBUG) {
            addLogging(builder);
        } else {
            addLogging(builder);
        }
//        enableCache(builder);

        return builder.build();
    }

    private void addLogging(OkHttpClient.Builder builder) {
        LogWorkInterceptor logWorkInterceptor = new LogWorkInterceptor(LogWorkInterceptor.Logger.DEFAULT);
        logWorkInterceptor.setLevel(LogWorkInterceptor.Level.BODY);
        builder.addInterceptor(logWorkInterceptor);
    }

    private void enableCache(OkHttpClient.Builder builder) {
        if (mContext == null) return;
        Interceptor cacheInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response originalResponse = chain.proceed(chain.request());
                int maxAge = 180;
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, max-age=" + maxAge)
                        .build();
            }
        };
        builder.addNetworkInterceptor(cacheInterceptor);

        int cacheSize = 10 * 1024 * 1024; // 10 MB
        Cache cache = new Cache(mContext.getCacheDir(), cacheSize);
        builder.cache(cache);
    }

    public static LogWorkApi getInstance() {
        return mApiInstance;
    }

    public LogWorkService getService()
    {
        return mApiService;
    }
}
