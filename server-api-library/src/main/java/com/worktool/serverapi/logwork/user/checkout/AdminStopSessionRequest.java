package com.worktool.serverapi.logwork.user.checkout;

/**
 * Created by THAIHOANG on 11/4/2017.
 */

import com.google.gson.annotations.SerializedName;

/**
 {
 "empId": "string",
 "fullName": "string",
 "empType": "string",
 "profilePicture": "/uploads/default.png",
 "empRole": "string",
 "wageLevel": "string",
 "benefits": {},
 "avatarUrl": "string",
 "phoneNumber": "string",
 "address": "string",
 "deviceId": "string",
 "startWorkAt": "2017-11-04T03:19:12.324Z",
 "endProbationAt": "2017-11-04T03:19:12.324Z",
 "birthDay": "2017-11-04T03:19:12.324Z",
 "createdAt": "2017-11-04T03:19:12.324Z",
 "updatedAt": "2017-11-04T03:19:12.324Z",
 "username": "string",
 "email": "string",
 "emailVerified": true
 }
 */
public class AdminStopSessionRequest {
    @SerializedName("sessionId")
    private String mSessionId;
    @SerializedName("empId")
    private String mEmpId;
    @SerializedName("deviceId")
    private String mDeviceId;

    public String getSessionId() {
        return mSessionId;
    }

    public void setSessionId(String sessionId) {
        mSessionId = sessionId;
    }

    public String getEmpId() {
        return mEmpId;
    }

    public void setEmpId(String empId) {
        mEmpId = empId;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }
}
