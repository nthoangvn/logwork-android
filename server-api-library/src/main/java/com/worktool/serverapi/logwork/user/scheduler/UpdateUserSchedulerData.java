package com.worktool.serverapi.logwork.user.scheduler;

/**
 * Created by THAIHOANG on 11/8/2017.
 */

import com.google.gson.annotations.SerializedName;

/**
 {
 "userId": "0014",
 "weekId": "W45-2017",
 "fullName": "Nguyen Thai Hoang",
 "workScheduler": {
 "mon": [
 "CA1"
 ],
 "tue": [
 "CA1"
 ],
 "wed": [
 "CA2"
 ],
 "thu": [
 "CA1",
 "CA2"
 ],
 "fri": [
 "CA1"
 ],
 "sat": [
 "CA1",
 "CA2"
 ],
 "sun": [
 "CA1"
 ]
 }
 }
 */
public class UpdateUserSchedulerData {
    @SerializedName("userId")
    private String mUserId;
    @SerializedName("weekId")
    private String mWeekId;
    @SerializedName("fullName")
    private String mFullName;
    @SerializedName("workScheduler")
    private SchedulerData mWorkScheduler;

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getWeekId() {
        return mWeekId;
    }

    public void setWeekId(String weekId) {
        mWeekId = weekId;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public SchedulerData getWorkScheduler() {
        return mWorkScheduler;
    }

    public void setWorkScheduler(SchedulerData workScheduler) {
        mWorkScheduler = workScheduler;
    }
}
