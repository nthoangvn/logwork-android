package com.worktool.serverapi.logwork.benefit;

/**
 * Created by THAIHOANG on 10/27/2017.
 */

import com.google.gson.annotations.SerializedName;

/**
 {
 "name": "Xăng xe",
 "value": 100000,
 "id": "59d118d66ecb6a337ce6987c"
 }
 */
public class Benefit {
    @SerializedName("name")
    private String mName;
    @SerializedName("value")
    private long mValue;
    @SerializedName("id")
    private String mId;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public long getValue() {
        return mValue;
    }

    public void setValue(long value) {
        mValue = value;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }
}
