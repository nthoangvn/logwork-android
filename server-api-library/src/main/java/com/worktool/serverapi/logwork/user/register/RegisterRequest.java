package com.worktool.serverapi.logwork.user.register;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

public class RegisterRequest {
    @SerializedName("empId")
    private String empId;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("empType")
    private String empType;
    @SerializedName("empRole")
    private String empRole;
    @SerializedName("wageLevel")
    private String wageLevel; // Optional
    @SerializedName("benefits")
    private List<String> benefits; // Optional
    @SerializedName("avatarUrl")
    private String avatarUrl; // Optional
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("address")
    private String address;
    @SerializedName("startWorkAt")
    private String startWorkAt; // Optional
    @SerializedName("endProbationAt")
    private String endProbationAt; // Optional
    @SerializedName("birthDay")
    private String birthDay; // Optional
    @SerializedName("createdAt")
    private String createdAt; // Optional
    @SerializedName("updatedAt")
    private String updatedAt; // Optional
    @SerializedName("username")
    private String username; // Optional
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("emailVerified")
    private String emailVerified; // Optional

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmpType() {
        return empType;
    }

    public void setEmpType(String empType) {
        this.empType = empType;
    }

    public String getEmpRole() {
        return empRole;
    }

    public void setEmpRole(String empRole) {
        this.empRole = empRole;
    }

    public String getWageLevel() {
        return wageLevel;
    }

    public void setWageLevel(String wageLevel) {
        this.wageLevel = wageLevel;
    }

    public List<String> getBenefits() {
        return benefits;
    }

    public void setBenefits(List<String> benefits) {
        this.benefits = benefits;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStartWorkAt() {
        return startWorkAt;
    }

    public void setStartWorkAt(String startWorkAt) {
        this.startWorkAt = startWorkAt;
    }

    public String getEndProbationAt() {
        return endProbationAt;
    }

    public void setEndProbationAt(String endProbationAt) {
        this.endProbationAt = endProbationAt;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(String emailVerified) {
        this.emailVerified = emailVerified;
    }
}
