package com.worktool.serverapi.logwork.user.checkin;

import com.google.gson.annotations.SerializedName;

/**
 * Created by THAIHOANG on 10/8/2017.
 */

/**
 {
 "empId": "string",
 "fullName": "string",
 "empType": "string",
 "profilePicture": "/uploads/default.png",
 "empRole": "string",
 "wageLevel": "string",
 "benefits": {},
 "avatarUrl": "string",
 "phoneNumber": "string",
 "address": "string",
 "deviceId": "string",
 "startWorkAt": "2017-11-04T03:19:12.324Z",
 "endProbationAt": "2017-11-04T03:19:12.324Z",
 "birthDay": "2017-11-04T03:19:12.324Z",
 "createdAt": "2017-11-04T03:19:12.324Z",
 "updatedAt": "2017-11-04T03:19:12.324Z",
 "username": "string",
 "email": "string",
 "emailVerified": true
 }
 */
public class StartWorkSessRequest {
    @SerializedName("empId")
    private String mEmpId;
    @SerializedName("deviceId")
    private String mDeviceId;
    @SerializedName("lat")
    private double mLatitude;
    @SerializedName("lng")
    private double mLongitude;

    public String getEmpId() {
        return mEmpId;
    }

    public void setEmpId(String empId) {
        mEmpId = empId;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }
}
