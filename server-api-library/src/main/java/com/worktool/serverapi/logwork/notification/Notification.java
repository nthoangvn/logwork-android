package com.worktool.serverapi.logwork.notification;

/**
 * Created by THAIHOANG on 10/24/2017.
 */

import com.google.gson.annotations.SerializedName;

import java.util.Locale;

/**
 {
 "from": "admin",
 "content": "Thông báo chung số 2",
 "notificationType": "commonNotification",
 "pinned": false,
 "createdAt": "2017-10-20T19:17:43.143Z",
 "id": "59ea4bd77d215f3904de44d1"
 }
 */
public class Notification {
    @SerializedName("from")
    private String mFrom;
    @SerializedName("content")
    private String mContent;
    @SerializedName("notificationType")
    private String mNotificationType;
    @SerializedName("pinned")
    private boolean mPinned;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("id")
    private String mId;

    public String getFrom() {
        return mFrom;
    }

    public void setFrom(String from) {
        mFrom = from;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public boolean isPinned() {
        return mPinned;
    }

    public void setPinned(boolean pinned) {
        mPinned = pinned;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getNotificationType() {
        return mNotificationType;
    }

    public void setNotificationType(String notificationType) {
        mNotificationType = notificationType;
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "From %s, content %s, notificationType %s",
                mFrom, mContent, mNotificationType);
    }
}
