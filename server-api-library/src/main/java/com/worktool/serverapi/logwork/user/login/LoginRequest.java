package com.worktool.serverapi.logwork.user.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

public class LoginRequest {
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
