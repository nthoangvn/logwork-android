package com.worktool.logwork.notifications;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.worktool.logwork.AppApplication;
import com.worktool.logwork.BaseFragment;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.admin.profile.UserManagementFragment;
import com.worktool.logwork.schedule.WorkingShift;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.logwork.util.ProfileUtils;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.notification.Notification;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends BaseFragment {

    public static final String EXTRA_USER_PROFILE = "NotificationFragment_extra_user_profile";

    private UserProfile mUserProfile;
    private RecyclerView mNotificationList;
    private NotificationAdapter mNotificationAdapter;
    private SharedPreferences mAppSettings;

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserProfile = getArguments().getParcelable(EXTRA_USER_PROFILE);
        setHasOptionsMenu(true);
        mAppSettings = getActivity().getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mUserProfile != null && ProfileUtils.hasManagerPermission(mUserProfile.getRoles())) {
            inflater.inflate(R.menu.menu_notification, menu);
        }
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                showAddNotificationDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showAddNotificationDialog() {
        AddNotificationDialogFragment addNotifDialog = new AddNotificationDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(AddNotificationDialogFragment.EXTRA_USER_PROFILE, mUserProfile);
        args.putString(AddNotificationDialogFragment.EXTRA_NOTIFICATION_TYPE, PublicConstants.NOTIF_TYPE_COMMON);
        addNotifDialog.setArguments(args);
        addNotifDialog.setOnAddNotificationListener(mAddNotifListener);
        addNotifDialog.show(getFragmentManager(), "add_notif_dialog");
    }

    private AddNotificationDialogFragment.OnAddNotificationListener mAddNotifListener =
            new AddNotificationDialogFragment.OnAddNotificationListener() {
                @Override
                public void onAddClick(Notification newNotification) {
                    Log.d(PublicConstants.LOG_TAG, "On add notif? " + newNotification);
                    onAddNewNotification(newNotification);
                }

                @Override
                public void onCancelClick() {
                    Log.d(PublicConstants.LOG_TAG, "On add notif cancel click");
                }
            };

    private void onAddNewNotification(Notification newNotification) {
        showLoading(getString(R.string.add_notification_waiting));
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        Call<Notification> createNotifCall = LogWorkApi.getInstance().getService()
                .createNotification(userToken, newNotification);
        createNotifCall.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {
                hideLoading();
                Log.d(PublicConstants.LOG_TAG, "Add notifications success? " + response.isSuccessful());
                if (response.isSuccessful()) {
                    showToast(AppApplication.appContext.getString(R.string.add_notification_success), Toast.LENGTH_SHORT);
                    loadNotification();
                } else {
                    showToast(AppApplication.appContext.getString(R.string.add_notification_fail), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<Notification> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                showToast(AppApplication.appContext.getString(R.string.add_notification_fail), Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNotificationList = view.findViewById(R.id.notification_list);
        mNotificationAdapter = new NotificationAdapter(getActivity().getApplicationContext());
//        mNotificationList.addItemDecoration(
//                new HorizontalDividerItemDecoration.Builder(getActivity())
//                        .colorResId(R.color.light_blue)
//                        .size(3)
//                        .build());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mNotificationList.setLayoutManager(layoutManager);
        mNotificationList.setAdapter(mNotificationAdapter);

        loadNotification();
    }

    private void loadNotification() {
        showLoading(getString(R.string.loading_notifications));
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        Call<List<Notification>> getNotifCall = LogWorkApi.getInstance().getService()
                .getNotifications(userToken);
        getNotifCall.enqueue(new Callback<List<Notification>>() {
            @Override
            public void onResponse(Call<List<Notification>> call, Response<List<Notification>> response) {
                Log.d(PublicConstants.LOG_TAG, "Load notifications success? " + response.isSuccessful());
                hideLoading();
                if (response.isSuccessful()) {
                    filterNotifications(response.body());
                } else {
                    showToast(AppApplication.appContext.getString(R.string.load_notification_fail), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<List<Notification>> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                showToast(AppApplication.appContext.getString(R.string.load_notification_fail), Toast.LENGTH_SHORT);
            }
        });
    }

    private void filterNotifications(List<Notification> notifications) {
        List<Notification> filterNotifications = new ArrayList<>();
        if (notifications != null && notifications.size() > 0) {
            for (Notification notification : notifications) {
                if (PublicConstants.NOTIF_TYPE_PROMOTION.equalsIgnoreCase(notification.getNotificationType()) ||
                        PublicConstants.NOTIF_TYPE_COMMON.equalsIgnoreCase(notification.getNotificationType()) ||
                        PublicConstants.NOTIF_TYPE_COMMON_OLD.equalsIgnoreCase(notification.getNotificationType())) {
                    filterNotifications.add(notification);
                }
            }
        }

        Collections.sort(filterNotifications, mNotificationComparator);
        updateNotificationList(filterNotifications);
    }

    private final Comparator<Notification> mNotificationComparator = new Comparator<Notification>() {
        @Override
        public int compare(Notification o1, Notification o2) {
            int result;
            DateTime o1Date = DateTimeUtils.stringToDateTime(o1.getCreatedAt(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            DateTime o2Date = DateTimeUtils.stringToDateTime(o2.getCreatedAt(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            if (o1Date.isBefore(o2Date)) {
                result = 1;
            } else if (o1Date.isAfter(o2Date)) {
                result = -1;
            } else {
                result = 0;
            }
            return result;
        }
    };

    private void updateNotificationList(List<Notification> filterNotifications) {
        mNotificationAdapter.setNotifications(filterNotifications);
    }
}
