package com.worktool.logwork.notifications;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.beardedhen.androidbootstrap.BootstrapDropDown;
import com.worktool.logwork.AppApplication;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.view.bootstrap.MainBootstrapBrand;
import com.worktool.serverapi.logwork.notification.Notification;
import com.worktool.serverapi.logwork.user.UserProfile;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddNotificationDialogFragment extends DialogFragment {

    public static final String EXTRA_USER_PROFILE = "AddNotificationDialogFragment_extra_user_profile";
    public static final String EXTRA_NOTIFICATION_TYPE = "AddNotificationDialogFragment_extra_notification_type";

    public interface OnAddNotificationListener {
        public void onAddClick(Notification newNotification);
        public void onCancelClick();
    }

    private UserProfile mUserProfile;
    private Notification mNewNotification;
    private OnAddNotificationListener mOnAddNotificationListener;
    private BootstrapDropDown mDropDownNotifType;
    private EditText mTextNotifContent;
    private String[] mNotifTypeData;
    private String mCurrNotifType;
    private int mSelectedNotifPos;
    private View mBtnConfirm;
    private View mBtnCancel;

    public AddNotificationDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserProfile = getArguments().getParcelable(EXTRA_USER_PROFILE);
        mCurrNotifType = getArguments().getString(EXTRA_NOTIFICATION_TYPE);
        Log.d(PublicConstants.LOG_TAG, "On add notif dialog create, mCurrNotifType: " + mCurrNotifType);
        setStyle(STYLE_NO_TITLE, 0);
        setCancelable(false);
    }

    public void setOnAddNotificationListener(OnAddNotificationListener onAddNotificationListener) {
        mOnAddNotificationListener = onAddNotificationListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_add_notification_dialog, null);
//        view = inflater.inflate(R.layout.fragment_work_sess_modified_dialog, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDropDownNotifType = view.findViewById(R.id.spinner_notif_type);
        mTextNotifContent = view.findViewById(R.id.text_notif_content);
        mBtnConfirm = view.findViewById(R.id.btn_ok);
        mBtnCancel = view.findViewById(R.id.btn_cancel);

        mNotifTypeData = AppApplication.appContext.getResources().getStringArray(R.array.notification_types);
        mDropDownNotifType.setBootstrapBrand(new MainBootstrapBrand());
        mSelectedNotifPos = 0;
        for (int i=0; i<PublicConstants.NOTIF_TYPES.length; i++) {
            if (PublicConstants.NOTIF_TYPES[i].equalsIgnoreCase(mCurrNotifType)) {
                mSelectedNotifPos = i;
                break;
            }
        }
        mDropDownNotifType.setText(mNotifTypeData[mSelectedNotifPos]);
        mDropDownNotifType.setDropdownData(mNotifTypeData);
        Log.d(PublicConstants.LOG_TAG, "On add notif dialog show, mSelectedNotifPos: " + mSelectedNotifPos);
        setupDropDownNotifType();

        setupPositiveButton();
        setupNegativeButton();
    }

    private void setupDropDownNotifType() {
        mDropDownNotifType.setOnDropDownItemClickListener(new BootstrapDropDown.OnDropDownItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View v, int id) {
                mSelectedNotifPos = id;
                String notifTypeStr = mNotifTypeData[id];
                Log.d(PublicConstants.LOG_TAG, "On notif type selected, id: " + id + ", type: " + notifTypeStr);
                mDropDownNotifType.setText(notifTypeStr);
            }
        });
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    private void setupNegativeButton() {
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnAddNotificationListener != null) {
                    mOnAddNotificationListener.onCancelClick();
                }
            }
        });
    }

    private void setupPositiveButton() {
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnAddNotificationListener != null) {
                    Notification newNotif = new Notification();
                    newNotif.setContent(mTextNotifContent.getText().toString().trim());
                    newNotif.setFrom(mUserProfile.getUsername());
                    newNotif.setNotificationType(PublicConstants.NOTIF_TYPES[mSelectedNotifPos]);
                    mOnAddNotificationListener.onAddClick(newNotif);
                }
            }
        });
    }
}
