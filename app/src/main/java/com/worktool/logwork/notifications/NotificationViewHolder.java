package com.worktool.logwork.notifications;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.worktool.logwork.R;

/**
 * Created by THAIHOANG on 10/24/2017.
 */

public class NotificationViewHolder extends RecyclerView.ViewHolder {

    public View rootView;
    public BootstrapLabel textNotifType;
    public TextView textNotifDate;
    public TextView textNotifContent;
    public View horizontalLine;

    public NotificationViewHolder(View itemView) {
        super(itemView);
        rootView = itemView;
        textNotifType = itemView.findViewById(R.id.text_notification_type);
        textNotifDate = itemView.findViewById(R.id.text_notification_date);
        textNotifContent = itemView.findViewById(R.id.text_notification_content);
        horizontalLine = itemView.findViewById(R.id.title_divider);
    }
}
