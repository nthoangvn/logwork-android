package com.worktool.logwork.notifications;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.notification.Notification;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THAIHOANG on 10/24/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationViewHolder> {

    private Context mContext;
    private List<Notification> mNotifications;

    public NotificationAdapter(Context context) {
        mContext = context;
        mNotifications = new ArrayList<>();
    }

    public void setNotifications(List<Notification> notifications) {
        mNotifications.clear();
        if (notifications != null && notifications.size() > 0) {
            mNotifications = new ArrayList<>(notifications);
        }
        notifyDataSetChanged();
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_notification_item, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        final Notification notification = mNotifications.get(position);
        holder.textNotifType.setText(getNotificationName(notification.getNotificationType()));
        DefaultBootstrapBrand bootstrapBrand = getNotificationBrand(notification.getNotificationType());
        holder.textNotifType.setBootstrapBrand(bootstrapBrand);
        holder.horizontalLine.setBackgroundResource(getTitleDividerColor(notification.getNotificationType()));

        DateTime notifDate = DateTimeUtils.stringToDateTime(notification.getCreatedAt(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
        holder.textNotifDate.setText(DateTimeUtils.dateTimeToString(notifDate, PublicConstants.NOTIFICATION_DATE_FMT));

        holder.textNotifContent.setText(notification.getContent());
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    private String getNotificationName(String notificationType) {
        String notifName;
        if (PublicConstants.NOTIF_TYPE_PROMOTION.equalsIgnoreCase(notificationType)) {
            notifName = mContext.getString(R.string.notification_type_promotion);
        } else if (PublicConstants.NOTIF_TYPE_RULE.equalsIgnoreCase(notificationType)) {
            notifName = mContext.getString(R.string.notification_type_rule);
        } else {
            notifName = mContext.getString(R.string.notification_type_common);
        }
        return notifName;
    }

    private DefaultBootstrapBrand getNotificationBrand(String notificationType) {
        DefaultBootstrapBrand brand;
        if (PublicConstants.NOTIF_TYPE_PROMOTION.equalsIgnoreCase(notificationType)) {
            brand = DefaultBootstrapBrand.DANGER;
        } else if (PublicConstants.NOTIF_TYPE_RULE.equalsIgnoreCase(notificationType)) {
            brand = DefaultBootstrapBrand.PRIMARY;
        } else {
            brand = DefaultBootstrapBrand.PRIMARY;
        }
        return brand;
    }

    private int getTitleDividerColor(String notificationType) {
        int color;
        if (PublicConstants.NOTIF_TYPE_PROMOTION.equalsIgnoreCase(notificationType)) {
            color = R.color.bootstrap_brand_danger;
        } else if (PublicConstants.NOTIF_TYPE_RULE.equalsIgnoreCase(notificationType)) {
            color = R.color.bootstrap_brand_primary;
        } else {
            color = R.color.bootstrap_brand_primary;
        }
        return color;
    }
}
