package com.worktool.logwork.admin.session;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.wajahatkarim3.easymoneywidgets.EasyMoneyTextView;
import com.worktool.logwork.AppApplication;
import com.worktool.logwork.LoadingSupportActivity;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.logwork.util.ProfileUtils;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.benefit.Benefit;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.worktool.serverapi.logwork.user.session.WorkSession;
import com.worktool.serverapi.logwork.user.session.WorkSessionInfo;
import com.worktool.serverapi.logwork.wage.WageLevel;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WorkSessionActivity extends LoadingSupportActivity implements UserWorkSessionAdapter.OnUserWorkSessionClick {

    public static final String EXTRA_USER_PROFILE = "WorkSessionActivity_extra_user_profile";

    private UserProfile mUserProfile;
    private UserWorkSessionAdapter mWorkSessionAdapter;
    private RecyclerView mWorkSessList;
    private SharedPreferences mAppSettings;
    //    private ProgressDialog mProgressDialog;
    private TextView mWorkTimeTotalText, mWorkTimeTotalModifiedText;
    private EasyMoneyTextView mSalaryTotalText, mWageAmountText, mBenefitText;

    private WageLevel mWageLevel;
    private Benefit mBenefit;
    private List<WorkSession> mFilteredWorkSessions = new ArrayList<>();
    private List<WorkSessionInfo> mWorkSessionInfos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_working_date);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        String thisMonth = DateTimeUtils.dateTimeToString(DateTime.now(), PublicConstants.WORK_SESSION_LIST_DATE_FMT);
        String title = getString(R.string.work_session_title) + " - " + thisMonth;
        setTitle(title);

        mUserProfile = getIntent().getParcelableExtra(EXTRA_USER_PROFILE);
        mAppSettings = getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);

        mWorkSessList = findViewById(R.id.working_session_list);
        mWorkSessList.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(this)
                        .colorResId(R.color.main_text_color)
                        .size(3)
                        .build());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mWorkSessList.setLayoutManager(layoutManager);

        mWorkSessionAdapter = new UserWorkSessionAdapter(AppApplication.appContext);
        mWorkSessionAdapter.setUserWorkSessionClick(this);
        mWorkSessList.setAdapter(mWorkSessionAdapter);

        setupUserInfoLayout();
        loadWorkSessionInfo();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupUserInfoLayout() {
        TextView textFullName = findViewById(R.id.text_full_name_value);
        textFullName.setText(mUserProfile.getFullName());

        mWorkTimeTotalText = findViewById(R.id.text_total_working_time_value);
        mWorkTimeTotalModifiedText = findViewById(R.id.text_total_working_time_modified_value);
        mWageAmountText = findViewById(R.id.text_wage_level_value);
        mBenefitText = findViewById(R.id.text_benefit_value);
        mSalaryTotalText = findViewById(R.id.text_salary_total_value);
    }

    private void updateWorkSessionList(List<WorkSession> workSessionsRes) {
        mFilteredWorkSessions.clear();
        if (workSessionsRes != null && workSessionsRes.size() > 0) {
            for (WorkSession workSession : workSessionsRes) {
                // Filter check in/out date
                if (!TextUtils.isEmpty(workSession.getCheckInTime()) &&
                        !TextUtils.isEmpty(workSession.getCheckOutTime())) {
                    DateTime checkInDate = DateTimeUtils.stringToDateTime(workSession.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                    DateTime checkOutDate = DateTimeUtils.stringToDateTime(workSession.getCheckOutTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                    if (checkOutDate.isAfter(checkInDate)) {
                        // Filter by this month
                        String workSessMonth = DateTimeUtils.dateTimeToString(checkInDate, PublicConstants.MONTH_ID_FMT);
                        String thisMonth = DateTimeUtils.dateTimeToString(DateTime.now(), PublicConstants.MONTH_ID_FMT);
                        if (thisMonth.equalsIgnoreCase(workSessMonth))
                        {
                            // Check whether work session name is valid
                            for (WorkSessionInfo workSessionInfo : mWorkSessionInfos) {
                                if (workSessionInfo.getName().equalsIgnoreCase(workSession.getSessionName())) {
                                    mFilteredWorkSessions.add(workSession);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        Collections.sort(mFilteredWorkSessions, mWorkSessionComparator);

//        List<WorkSession> workSessionsThisMonth = WorkSessionUtils.buildWorkSessionOfMonth(
//                mUserProfile.getUserId(), DateTimeUtils.getFirstDateOfTheMonth(), DateTimeUtils.getLastDateOfTheMonth());
//        WorkSessionUtils.updateWorkSessions(workSessionsThisMonth, filteredWorkSessions);

        mWorkSessionAdapter.setWorkSessions(mFilteredWorkSessions);

        loadWageInfo();
    }

    private void loadWageInfo() {
        showLoading(getString(R.string.loading_wage_info));
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        Log.d(PublicConstants.LOG_TAG, "Get wage info done, wage id? " + mUserProfile.getWageLevel());
        Call<WageLevel> wageLevelCall = LogWorkApi.getInstance().getService()
                .getWageLevel(mUserProfile.getWageLevel(), userToken);
        wageLevelCall.enqueue(new Callback<WageLevel>() {
            @Override
            public void onResponse(Call<WageLevel> call, Response<WageLevel> response) {
                Log.d(PublicConstants.LOG_TAG, "Get wage info done, success? " + response.isSuccessful());
                hideLoading();
                if (response.isSuccessful()) {
                    onLoadWageInfoCompleted(response.body());
                } else {
                    Toast.makeText(WorkSessionActivity.this, R.string.get_wage_info_fail, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<WageLevel> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                Toast.makeText(WorkSessionActivity.this, R.string.get_wage_info_fail, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void onLoadWageInfoCompleted(WageLevel wageLevel) {
        mWageLevel = wageLevel;
        if (mWageAmountText != null) {
            mWageAmountText.setText(String.valueOf(mWageLevel.getAmount()));
            mWageAmountText.setShowCurrency(true);
        }
        loadBenefitInfo();
    }

    private void loadBenefitInfo() {
        if (mUserProfile.getBenefits() != null && mUserProfile.getBenefits().size() > 0) {
            Log.d(PublicConstants.LOG_TAG, "Loading benefit info");
            showLoading(getString(R.string.loading_benefit_info));
            String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
            String firstBenefitId = mUserProfile.getBenefits().get(0);
            Call<Benefit> benefitCall = LogWorkApi.getInstance().getService()
                    .getBenefit(firstBenefitId, userToken);
            benefitCall.enqueue(new Callback<Benefit>() {
                @Override
                public void onResponse(Call<Benefit> call, Response<Benefit> response) {
                    hideLoading();
                    Log.d(PublicConstants.LOG_TAG, "Loading benefit info done, success? " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        mBenefit = response.body();
                        invalidateSalaryInfo();
                    } else {
                        Toast.makeText(WorkSessionActivity.this, R.string.load_benefit_info_fail, Toast.LENGTH_SHORT).show();
                        invalidateSalaryInfo();
                    }
                }

                @Override
                public void onFailure(Call<Benefit> call, Throwable t) {
                    t.printStackTrace();
                    hideLoading();
                    Toast.makeText(WorkSessionActivity.this, R.string.load_benefit_info_fail, Toast.LENGTH_SHORT).show();
                    invalidateSalaryInfo();
                }
            });
        } else {
            Log.d(PublicConstants.LOG_TAG, "Load benefit fail, no benefit info");
            invalidateSalaryInfo();
        }
    }

    private void invalidateSalaryInfo() {
        if (mBenefitText != null) {
            mBenefitText.setText(String.valueOf(mBenefit!=null?mBenefit.getValue():0));
            mBenefitText.setShowCurrency(true);
        }

        long workSessTimeTotal = 0;
        for (WorkSession workSession : mFilteredWorkSessions) {
            DateTime checkInDate;
            if (!TextUtils.isEmpty(workSession.getConfirmedCheckInTime())) {
                checkInDate = DateTimeUtils.stringToDateTime(workSession.getConfirmedCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            } else {
                checkInDate = DateTimeUtils.stringToDateTime(workSession.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            }

            DateTime checkOutDate;
            if (!TextUtils.isEmpty(workSession.getConfirmedCheckOutTime())) {
                checkOutDate = DateTimeUtils.stringToDateTime(workSession.getConfirmedCheckOutTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            } else {
                checkOutDate = DateTimeUtils.stringToDateTime(workSession.getCheckOutTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            }
            long workSessTimeMinutes = (checkOutDate.getMillis() - checkInDate.getMillis()) / PublicConstants.ONE_MINUTE_MS;
            workSessTimeTotal += workSessTimeMinutes;
        }

        if (mWorkTimeTotalText != null) {
            mWorkTimeTotalText.setText(getWorkSessTimeTotal(workSessTimeTotal));
        }

        if (mWorkTimeTotalModifiedText != null) {
            mWorkTimeTotalModifiedText.setText(getWorkSessTimeTotal(workSessTimeTotal));
        }

        if (mSalaryTotalText != null) {
            long salaryTotal = 0;
            // Calculate basic salary
            long basicSalary = (long) (workSessTimeTotal / 60.0f * mWageLevel.getAmount());
            salaryTotal = basicSalary + (mBenefit!=null?mBenefit.getValue():0);
            Log.d(PublicConstants.LOG_TAG, "Work session time total: " + workSessTimeTotal + ", salary total: " + salaryTotal);
            mSalaryTotalText.setText(String.valueOf(salaryTotal));
            mSalaryTotalText.setShowCurrency(true);
        }
    }

    private String getWorkSessTimeTotal(long workSessTimeTotalMins) {
        String workSessTimeTotalStr = null;
        int workHours = (int) (workSessTimeTotalMins / 60);
        int workMins = (int) (workSessTimeTotalMins % 60);
        workSessTimeTotalStr = getString(R.string.admin_work_sess_time_hour_minute, workHours, workMins);
        return workSessTimeTotalStr;
    }

    private final Comparator<WorkSession> mWorkSessionComparator = new Comparator<WorkSession>() {
        @Override
        public int compare(WorkSession o1, WorkSession o2) {
            int result;
            DateTime o1CheckInDate = DateTimeUtils.stringToDateTime(o1.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            DateTime o2CheckInDate = DateTimeUtils.stringToDateTime(o2.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            if (o1CheckInDate.isBefore(o2CheckInDate)) {
                result = -1;
            } else if (o1CheckInDate.isAfter(o2CheckInDate)) {
                result = 1;
            } else {
                result = 0;
            }
            return result;
        }
    };

    private void loadWorkSession() {
        mFilteredWorkSessions.clear();
        if (mUserProfile != null) {
            final String userId = mUserProfile.getEmpId();
            Log.d(PublicConstants.LOG_TAG, "Load work session for user: " + userId);
            String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
            if (!TextUtils.isEmpty(userToken)) {
                showLoading(AppApplication.appContext.getString(R.string.loading_work_session_monthly));
                Call<List<WorkSession>> getWorkSessCall = LogWorkApi.getInstance().getService()
                        .getWorkSession(userId, userToken);
                getWorkSessCall.enqueue(new Callback<List<WorkSession>>() {
                    @Override
                    public void onResponse(Call<List<WorkSession>> call, Response<List<WorkSession>> response) {
                        Log.d(PublicConstants.LOG_TAG, "Load work session for user " + userId
                                + " done, success? " + response.isSuccessful());
                        hideLoading();
                        if (response.isSuccessful()) {
                            List<WorkSession> workSessions = response.body();
                            updateWorkSessionList(workSessions);
                        } else {
                            Toast.makeText(WorkSessionActivity.this, R.string.error_invalid_work_sessions, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<WorkSession>> call, Throwable t) {
                        t.printStackTrace();
                        hideLoading();
                        Toast.makeText(WorkSessionActivity.this, R.string.get_work_session_fail, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            Log.d(PublicConstants.LOG_TAG, "Load work session fail, user profile null");
            Toast.makeText(WorkSessionActivity.this, R.string.get_work_session_fail, Toast.LENGTH_SHORT).show();
        }
    }

    private void loadWorkSessionInfo() {
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        showLoading(getString(R.string.loading_work_session_info));
        Call<List<WorkSessionInfo>> listWorkSessCall = LogWorkApi.getInstance().getService()
                .getWorkSessionInfos(userToken);
        listWorkSessCall.enqueue(new Callback<List<WorkSessionInfo>>() {
            @Override
            public void onResponse(Call<List<WorkSessionInfo>> call, Response<List<WorkSessionInfo>> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    mWorkSessionInfos = response.body();
                    loadWorkSession();
                } else {
                    Toast.makeText(WorkSessionActivity.this, R.string.load_work_session_info_fail, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<WorkSessionInfo>> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                Toast.makeText(WorkSessionActivity.this, R.string.load_work_session_info_fail, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onUserWorkSessionEditClick(WorkSession workSession) {
        Log.d(PublicConstants.LOG_TAG, "On user work session edit click");
        if (ProfileUtils.hasAdminPermission(mUserProfile.getRoles())) {
            WorkSessModifiedDialogFragment dialogFragment = new WorkSessModifiedDialogFragment();
            dialogFragment.setOnSessModifiedListener(mWorkSessTimeChangeListener);
            dialogFragment.setWorkSession(workSession);
            dialogFragment.show(getSupportFragmentManager(), "change_work_sess_time_dialog");
        } else {
            showNoPermissionDialog();
        }
    }

    private void showNoPermissionDialog() {
        String title = getString(R.string.confirm_work_scheduler_no_permission);
        String content = getString(R.string.change_work_session_time_no_permission_msg);
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(content)
                .show();
    }

    private WorkSessModifiedDialogFragment.OnWorkSessTimeChangeListener mWorkSessTimeChangeListener =
            new WorkSessModifiedDialogFragment.OnWorkSessTimeChangeListener() {
        @Override
        public void onWorkSessTimeChanged(WorkSession workSession) {
            Log.d(PublicConstants.LOG_TAG, "On work sess time changed, confirmCheckIn: " +
                    workSession.getConfirmedCheckInTime() + ", confirmCheckOut: " + workSession.getConfirmedCheckOutTime());
            onUpdateWorkSessionTime(workSession);
        }

        @Override
        public void onWorkSessTimeCanceled() {
            Log.d(PublicConstants.LOG_TAG, "On work sess time canceled");
        }
    };

    private void onUpdateWorkSessionTime(WorkSession workSession) {
        showLoading(getString(R.string.modify_work_session_waiting));
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        Call<WorkSession> updateWorkSessCall = LogWorkApi.getInstance().getService()
                .updateWorkSession(workSession.getSessionId(), userToken, workSession);
        updateWorkSessCall.enqueue(new Callback<WorkSession>() {
            @Override
            public void onResponse(Call<WorkSession> call, Response<WorkSession> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    Toast.makeText(WorkSessionActivity.this, R.string.modify_work_session_success, Toast.LENGTH_SHORT).show();
                    loadWorkSession();
                } else {
                    Toast.makeText(WorkSessionActivity.this, R.string.modify_work_session_fail, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<WorkSession> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                Toast.makeText(WorkSessionActivity.this, R.string.modify_work_session_fail, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
