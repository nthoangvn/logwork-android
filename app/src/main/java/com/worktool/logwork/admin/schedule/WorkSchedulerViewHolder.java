package com.worktool.logwork.admin.schedule;

import android.view.View;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.worktool.logwork.R;

import org.zakariya.stickyheaders.SectioningAdapter;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

public class WorkSchedulerViewHolder extends SectioningAdapter.HeaderViewHolder {

    public View rootView;
    public BootstrapLabel textWorkingDate;
    public View titleDivider;

    public WorkSchedulerViewHolder(View itemView) {
        super(itemView);
        rootView = itemView;
        textWorkingDate = itemView.findViewById(R.id.text_working_date);
        titleDivider = itemView.findViewById(R.id.title_divider);
    }
}
