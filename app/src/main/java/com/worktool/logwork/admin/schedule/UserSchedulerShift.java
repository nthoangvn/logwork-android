package com.worktool.logwork.admin.schedule;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Locale;

/**
 * Created by THAIHOANG on 10/31/2017.
 */

public class UserSchedulerShift implements Parcelable {
    private String mUserId;
    private String mFullName;
    private String mShiftDate;
    private String mShiftName;

    public UserSchedulerShift() {
    }

    protected UserSchedulerShift(Parcel in) {
        mUserId = in.readString();
        mFullName = in.readString();
        mShiftDate = in.readString();
        mShiftName = in.readString();
    }

    public static final Creator<UserSchedulerShift> CREATOR = new Creator<UserSchedulerShift>() {
        @Override
        public UserSchedulerShift createFromParcel(Parcel in) {
            return new UserSchedulerShift(in);
        }

        @Override
        public UserSchedulerShift[] newArray(int size) {
            return new UserSchedulerShift[size];
        }
    };

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getShiftDate() {
        return mShiftDate;
    }

    public void setShiftDate(String shiftDate) {
        mShiftDate = shiftDate;
    }

    public String getShiftName() {
        return mShiftName;
    }

    public void setShiftName(String shiftName) {
        mShiftName = shiftName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUserId);
        dest.writeString(mFullName);
        dest.writeString(mShiftDate);
        dest.writeString(mShiftName);
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "userId %s, fullName %s, shiftName %s, shiftDate %s",
                mUserId, mFullName, mShiftName, mShiftDate);
    }
}
