package com.worktool.logwork.admin.session;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.worktool.logwork.R;

/**
 * Created by THAIHOANG on 10/29/2017.
 */

public class UserWorkSessionViewHolder extends RecyclerView.ViewHolder {

    public TextView textSessDate;
    public CheckBox textCheckInTime;
    public CheckBox textCheckOutTime;
    public ImageView imgEdit;

    public UserWorkSessionViewHolder(View itemView) {
        super(itemView);
        textSessDate = itemView.findViewById(R.id.text_sess_date);
        textCheckInTime = itemView.findViewById(R.id.text_checkin_time);
        textCheckOutTime = itemView.findViewById(R.id.text_checkout_time);
        imgEdit = itemView.findViewById(R.id.img_edit);
    }
}
