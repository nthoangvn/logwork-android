package com.worktool.logwork.admin.profile;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.worktool.logwork.R;

/**
 * Created by THAIHOANG on 10/29/2017.
 */

public class UserViewHolder extends RecyclerView.ViewHolder {

    public View rootView;
    public ImageView imgAvatar;
    public TextView textFullName;
    public TextView textPhoneNumber;
    public TextView textBranch;
    public BootstrapLabel buttonWorkDate;

    public UserViewHolder(View itemView) {
        super(itemView);
        rootView = itemView;
        imgAvatar = itemView.findViewById(R.id.img_avatar);
        textFullName = itemView.findViewById(R.id.text_full_name);
        textPhoneNumber = itemView.findViewById(R.id.text_phone_number);
        textBranch = itemView.findViewById(R.id.text_branch);
        buttonWorkDate = itemView.findViewById(R.id.btn_work_date);
    }
}
