package com.worktool.logwork.admin.schedule;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.worktool.logwork.R;

/**
 * Created by THAIHOANG on 11/7/2017.
 */

public class AddSchedulerShiftViewHolder extends RecyclerView.ViewHolder {

    public View rootView;
    public ImageView imgAvatar;
    public TextView textFullName;
    public TextView textPhoneNumber;
    public CheckBox btnSelected;

    public AddSchedulerShiftViewHolder(View itemView) {
        super(itemView);
        rootView = itemView;
        imgAvatar = itemView.findViewById(R.id.img_avatar);
        textFullName = itemView.findViewById(R.id.text_full_name);
        textPhoneNumber = itemView.findViewById(R.id.text_phone_number);
        btnSelected = itemView.findViewById(R.id.btn_selected);
    }
}
