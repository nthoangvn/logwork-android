package com.worktool.logwork.admin.schedule;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.worktool.logwork.LoadingSupportActivity;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.schedule.WorkingScheduleFactory;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.worktool.serverapi.logwork.user.scheduler.UpdateUserSchedulerData;
import com.worktool.serverapi.logwork.user.scheduler.UpdateWorkSchedulerResponse;
import com.worktool.serverapi.logwork.user.session.WorkSessionInfo;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddSchedulerShiftActivity extends LoadingSupportActivity implements View.OnClickListener {

    public static final String EXTRA_USER_SCHEDULER_LIST = "AddSchedulerShiftActivity_extra_user_scheduler_list";
    public static final String EXTRA_CURRENT_WORK_SCHEDULER_DATE = "AddSchedulerShiftActivity_extra_work_scheduler_date";
    public static final String EXTRA_WORK_SESSION_INFO = "AddSchedulerShiftActivity_extra_work_session_info";

    private List<UserSchedulerShift> mUserSchedulerShifts;
    private WorkSchedulerDate mCurrWorkSchdlrDate;
    private WorkSessionInfo mWorkSessionInfo;
    private List<UserProfile> mUserProfiles = new ArrayList<>();
    private RecyclerView mEmployeeList;
    private AddSchedulerShiftAdapter mAdapter;
    private SharedPreferences mAppSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_scheduler_shift);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.edit_work_session);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mAppSettings = getApplicationContext().getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);
        mUserSchedulerShifts = getIntent().getParcelableArrayListExtra(EXTRA_USER_SCHEDULER_LIST);
        mCurrWorkSchdlrDate = getIntent().getParcelableExtra(EXTRA_CURRENT_WORK_SCHEDULER_DATE);
        mWorkSessionInfo = getIntent().getParcelableExtra(EXTRA_WORK_SESSION_INFO);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        mEmployeeList = findViewById(R.id.employee_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mEmployeeList.setLayoutManager(layoutManager);
        mEmployeeList.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(this)
                        .colorResId(R.color.main_text_color)
                        .size(3)
                        .build());

        mAdapter = new AddSchedulerShiftAdapter(getApplicationContext());
        mEmployeeList.setAdapter(mAdapter);

        setupSchedulerButtons();
        loadUserList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupSchedulerButtons() {
        View btnAddScheduler = findViewById(R.id.btn_add);
        btnAddScheduler.setOnClickListener(this);
        View btnCancelScheduler = findViewById(R.id.btn_cancel);
        btnCancelScheduler.setOnClickListener(this);
    }

    private void loadUserList() {
        showLoading(getString(R.string.loading_user_list));
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        Call<List<UserProfile>> userProfilesCall = LogWorkApi.getInstance().getService()
                .getUserList(userToken);
        userProfilesCall.enqueue(new Callback<List<UserProfile>>() {
            @Override
            public void onResponse(Call<List<UserProfile>> call, Response<List<UserProfile>> response) {
                hideLoading();
                Log.d(PublicConstants.LOG_TAG, "Load user list completed, success? " + response.isSuccessful());
                if (response.isSuccessful()) {
                    showUserList(response.body());
                } else {
                    Toast.makeText(AddSchedulerShiftActivity.this, R.string.load_user_list_fail, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<UserProfile>> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                Toast.makeText(AddSchedulerShiftActivity.this, R.string.load_user_list_fail, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showUserList(List<UserProfile> userProfiles) {
        if (mAdapter != null) {
            mUserProfiles.clear();
            Map<String, Boolean> currSelectedEmps = new HashMap<>();
            List<UserSchedulerShift> userSchedulerShifts = mCurrWorkSchdlrDate.getShifts();
            if (userSchedulerShifts != null && userSchedulerShifts.size() > 0) {
                for (UserSchedulerShift userSchedulerShift : userSchedulerShifts) {
                    if (mWorkSessionInfo.getName().equalsIgnoreCase(userSchedulerShift.getShiftName())) {
                        currSelectedEmps.put(userSchedulerShift.getUserId(), true);
                    }
                }
            }
            mAdapter.setSelectedProfiles(currSelectedEmps);

            if (userProfiles != null && userProfiles.size() > 0) {
                mUserProfiles = new ArrayList<>(userProfiles);
            }
            mAdapter.setUserProfiles(mUserProfiles);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                onSubmitScheduler();
                break;
            case R.id.btn_cancel:
                onCancelScheduler();
                break;
        }
    }

    private void onCancelScheduler() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void onSubmitScheduler() {
        if (mAdapter != null) {
            showLoading(getString(R.string.update_work_scheduler_waiting));
            // Build selected employees
            Map<String, Boolean> selectedArr = mAdapter.getSelectedEmployees();
            List<UserProfile> selectedProfiles = new ArrayList<>();
            for (UserProfile userProfile : mUserProfiles) {
                Boolean isSelected = selectedArr.get(userProfile.getEmpId());
                if (isSelected != null && isSelected) {
                    selectedProfiles.add(userProfile);
                }
            }

            // Update work scheduler date
            List<UserSchedulerShift> schedulerShifts = new ArrayList<>();
            for (UserProfile selectedProfile : selectedProfiles) {
                // Build user scheduler shift
                UserSchedulerShift userSchedulerShift = new UserSchedulerShift();
                userSchedulerShift.setUserId(selectedProfile.getEmpId());
                userSchedulerShift.setFullName(selectedProfile.getFullName());
                userSchedulerShift.setShiftDate(mCurrWorkSchdlrDate.getDate());
                userSchedulerShift.setShiftName(mWorkSessionInfo.getName());
                schedulerShifts.add(userSchedulerShift);
            }

            // Sync user scheduler data
            Iterator<UserSchedulerShift> allShiftsIter = mUserSchedulerShifts.iterator();
            while (allShiftsIter.hasNext()) {
                UserSchedulerShift userSchedulerShift = allShiftsIter.next();
                if (mWorkSessionInfo.getName().equalsIgnoreCase(userSchedulerShift.getShiftName())) {
                    String currWorkSchdlrDateStr = DateTimeUtils.convertUtcStringToDefault(mCurrWorkSchdlrDate.getDate(),
                            PublicConstants.USER_PROFILE_DATE_FMT, PublicConstants.GENERAL_DATE_FMT);
                    String shiftDate = DateTimeUtils.convertUtcStringToDefault(userSchedulerShift.getShiftDate(),
                            PublicConstants.USER_PROFILE_DATE_FMT, PublicConstants.GENERAL_DATE_FMT);
                    if (currWorkSchdlrDateStr.equalsIgnoreCase(shiftDate)) {
                        // Same day, same shift -> check user id
                        boolean foundShift = false;
                        Iterator<UserSchedulerShift> currDateShiftIter = schedulerShifts.iterator();
                        while (currDateShiftIter.hasNext()) {
                            UserSchedulerShift currDateShift = currDateShiftIter.next();
                            if (currDateShift.getUserId().equalsIgnoreCase(userSchedulerShift.getUserId())) {
                                currDateShiftIter.remove();
                                foundShift = true;
                                break;
                            }
                        }

                        if (!foundShift) {
                            // Remove unselected shift
                            Log.d(PublicConstants.LOG_TAG, "Remove user scheduler shift: " + userSchedulerShift);
                            allShiftsIter.remove();
                        }
                    }
                }
            }

            if (schedulerShifts.size() > 0) {
                mUserSchedulerShifts.addAll(schedulerShifts);
            }

            updateWorkSchedulers(mUserSchedulerShifts);


        } else {
            Toast.makeText(this, R.string.update_work_scheduler_fail, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void updateWorkSchedulers(List<UserSchedulerShift> userSchedulerShifts) {
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        List<UpdateUserSchedulerData> updateUserSchedulerData = WorkingScheduleFactory.createUpdateWorkSchedulerDatas(userSchedulerShifts);
        Call<UpdateWorkSchedulerResponse> updateWorkSchedulerCall = LogWorkApi.getInstance().getService()
                .updateWorkSchedulers(userToken, updateUserSchedulerData);
        updateWorkSchedulerCall.enqueue(new Callback<UpdateWorkSchedulerResponse>() {
            @Override
            public void onResponse(Call<UpdateWorkSchedulerResponse> call, Response<UpdateWorkSchedulerResponse> response) {
                hideLoading();
                Log.d(PublicConstants.LOG_TAG, "Update work scheduler done, success? " + response.isSuccessful());
                if (response.isSuccessful()) {
                    Toast.makeText(AddSchedulerShiftActivity.this, R.string.update_work_scheduler_success, Toast.LENGTH_SHORT).show();
                    onUpdateWorkSchedulerSuccess();
                } else {
                    Toast.makeText(AddSchedulerShiftActivity.this, R.string.update_work_scheduler_fail, Toast.LENGTH_SHORT).show();
                    onUpdateWorkSchedulerFail();
                }
            }

            @Override
            public void onFailure(Call<UpdateWorkSchedulerResponse> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                Toast.makeText(AddSchedulerShiftActivity.this, R.string.update_work_scheduler_fail, Toast.LENGTH_SHORT).show();
                onUpdateWorkSchedulerFail();
            }
        });
    }

    private void onUpdateWorkSchedulerSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    private void onUpdateWorkSchedulerFail() {
        finish();
    }
}
