package com.worktool.logwork.admin.schedule;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.worktool.logwork.R;
import com.worktool.logwork.admin.profile.UserViewHolder;
import com.worktool.logwork.image.glide.GlideApp;
import com.worktool.logwork.schedule.WorkingShift;
import com.worktool.logwork.util.UrlUtils;
import com.worktool.serverapi.logwork.user.UserProfile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by THAIHOANG on 11/7/2017.
 */

public class AddSchedulerShiftAdapter extends RecyclerView.Adapter<AddSchedulerShiftViewHolder> {

    private Context mContext;
    private List<UserProfile> mUserProfiles;
    private Map<String, Boolean> mSelectedProfiles;

    public AddSchedulerShiftAdapter(Context context) {
        mContext = context;
        mUserProfiles = new ArrayList<>();
        mSelectedProfiles = new HashMap<>();
    }

    public void setSelectedProfiles(Map<String, Boolean> selectedProfiles) {
        mSelectedProfiles.clear();
        mSelectedProfiles = new HashMap<>(selectedProfiles);
    }

    public void setUserProfiles(List<UserProfile> userProfiles) {
        mUserProfiles.clear();
        if (userProfiles != null && userProfiles.size() > 0) {
            mUserProfiles = new ArrayList<>(userProfiles);
        }
        notifyDataSetChanged();
    }

    @Override
    public AddSchedulerShiftViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_scheduler_shift_item, parent, false);
        return new AddSchedulerShiftViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AddSchedulerShiftViewHolder holder, final int position) {
        final UserProfile userProfile = mUserProfiles.get(position);

        String avatarUrl = UrlUtils.getAvatarDomain() + userProfile.getProfilePicture();
        GlideApp.with(mContext)
                .load(avatarUrl)
                .error(R.drawable.ic_avatar_default)
                .fitCenter()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(holder.imgAvatar);

        holder.textFullName.setText(userProfile.getFullName() + " - " + userProfile.getEmpId());
        holder.textPhoneNumber.setText(userProfile.getPhoneNumber());

        Boolean isSelected = mSelectedProfiles.get(userProfile.getEmpId());
        holder.btnSelected.setOnCheckedChangeListener(null);
        holder.btnSelected.setChecked(isSelected != null && isSelected);
        holder.btnSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mSelectedProfiles.put(userProfile.getEmpId(), isChecked);
            }
        });

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.btnSelected.setChecked(!holder.btnSelected.isChecked());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUserProfiles.size();
    }

    public Map<String, Boolean> getSelectedEmployees() {
        return mSelectedProfiles;
    }
}
