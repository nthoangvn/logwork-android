package com.worktool.logwork.admin.schedule;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.worktool.logwork.AppApplication;
import com.worktool.logwork.BaseFragment;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.schedule.WorkingScheduleFactory;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.logwork.util.ProfileUtils;
import com.worktool.logwork.view.bootstrap.MainBootstrapBrand;
import com.worktool.logwork.view.bootstrap.SecondaryBootstrapBrand;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.worktool.serverapi.logwork.user.scheduler.WeekSchedulerResponse;
import com.worktool.serverapi.logwork.user.session.WorkSessionInfo;
import com.worktool.serverapi.logwork.workscheduler.SubmitWorkSchedulerRequest;
import com.worktool.serverapi.logwork.workscheduler.SubmitWorkSchedulerResponse;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.worktool.logwork.util.DateTimeUtils.dateTimeToString;


/**
 * A simple {@link Fragment} subclass.
 */
public class WorkSchedulerManagementFragment extends BaseFragment implements
        View.OnClickListener, WorkSchedulerManagementAdapter.WorkSchedulerManagementClickListener {

    public static final String EXTRA_USER_PROFILE = "WorkSchedulerManagementFragment_extra_user_profile";
    private static final int REQ_UPDATE_WORK_SCHEDULER = 1;

    private UserProfile mUserProfile;
    private RecyclerView mWorkSchedulerList;
    private WorkSchedulerManagementAdapter mWorkSchedulerAdapter;
    private SharedPreferences mAppSettings;
    private boolean mCurrWeekSchdlrClosed = false;
    private BootstrapButton mFirstShiftBtn, mSecondShiftBtn, mExtraShiftBtn;
    private TextView textWeek, textFirstDate, textLastDate;
    private SwitchCompat mSwScheduleConfirm;
    private String mCurrSelectedWeekId;
    private List<WorkSessionInfo> mWorkSessionInfos;
    private List<UserSchedulerShift> mUserSchedulerShifts = new ArrayList<>();
    private int mCurrWorkSessInfoIdx = 0;

    public WorkSchedulerManagementFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserProfile = getArguments().getParcelable(EXTRA_USER_PROFILE);
        mAppSettings = getActivity().getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);
        DateTime today = DateTime.now();
        mCurrSelectedWeekId = dateTimeToString(today, PublicConstants.WEEK_ID_FMT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_working_shift_management, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mFirstShiftBtn = view.findViewById(R.id.btn_morning_shift);
        mFirstShiftBtn.setBootstrapBrand(new MainBootstrapBrand());
        mFirstShiftBtn.setOnClickListener(this);
        mSecondShiftBtn = view.findViewById(R.id.btn_afternoon_shift);
        mSecondShiftBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
        mSecondShiftBtn.setOnClickListener(this);
        mExtraShiftBtn = view.findViewById(R.id.btn_extra_shift);
        mExtraShiftBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
        mExtraShiftBtn.setOnClickListener(this);
        mExtraShiftBtn.setVisibility(View.GONE);

        mWorkSchedulerList = view.findViewById(R.id.working_shift_list);

        mWorkSchedulerAdapter = new WorkSchedulerManagementAdapter(getActivity().getApplicationContext());
        mWorkSchedulerAdapter.setListener(this);
        StickyHeaderLayoutManager layoutManager = new StickyHeaderLayoutManager();
        mWorkSchedulerList.setLayoutManager(layoutManager);
        mWorkSchedulerList.setAdapter(mWorkSchedulerAdapter);

        textWeek = view.findViewById(R.id.text_week_value);
        textWeek.setText(mCurrSelectedWeekId);

        textFirstDate = view.findViewById(R.id.text_first_day_value);
        DateTime firstDate = DateTimeUtils.getStartDayOfThisWeek();
        textFirstDate.setText(DateTimeUtils.dateTimeToString(firstDate, PublicConstants.GENERAL_DATE_FMT));

        textLastDate = view.findViewById(R.id.text_last_date_value);
        DateTime lastDate = firstDate.plusDays(6);
        textLastDate.setText(DateTimeUtils.dateTimeToString(lastDate, PublicConstants.GENERAL_DATE_FMT));

        mSwScheduleConfirm = view.findViewById(R.id.sw_schedule_confirm);
        mSwScheduleConfirm.setEnabled(false);
        mSwScheduleConfirm.setOnCheckedChangeListener(mSwCheckedChangeListener);

        Log.d(PublicConstants.LOG_TAG, "Work scheduler management, load work session info");
        loadWorkSessionInfo();
//        loadAllWorkSchedulers(mCurrSelectedWeekId);
    }

    private CompoundButton.OnCheckedChangeListener mSwCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            List<String> userRoles = mUserProfile.getRoles();
            boolean hasAdminPermission = ProfileUtils.hasAdminPermission(userRoles);
            if (hasAdminPermission) {
                showConfirmDialog(isChecked);
            } else {
                // Revert old state
                buttonView.setChecked(!isChecked);
                showNoPermissionDialog();
            }
        }
    };

    private void showNoPermissionDialog() {
        String title = AppApplication.appContext.getString(R.string.confirm_work_scheduler_no_permission);
        String content = AppApplication.appContext.getString(R.string.confirm_work_scheduler_no_permission_msg);
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(content)
                .show();
    }

    private void setSwSchedulerConfirmState(boolean isChecked) {
        mSwScheduleConfirm.setOnCheckedChangeListener(null);
        mSwScheduleConfirm.setChecked(isChecked);
        mSwScheduleConfirm.setOnCheckedChangeListener(mSwCheckedChangeListener);
    }

    private void showConfirmDialog(final boolean workSchedulerClosed) {
        String title;
        if (workSchedulerClosed) {
            title = AppApplication.appContext.getString(R.string.close_work_scheduler);
        } else {
            title = AppApplication.appContext.getString(R.string.open_work_scheduler);
        }
        String content;
        if (workSchedulerClosed) {
            content = AppApplication.appContext.getString(R.string.close_work_scheduler_confirm);
        } else {
            content = AppApplication.appContext.getString(R.string.open_work_scheduler_confirm);
        }

        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(content)
                .setCancelText(getString(R.string.cancel))
                .setConfirmText(getString(R.string.yes))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        submitWorkScheduler(workSchedulerClosed);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
    }

    private void submitWorkScheduler(final boolean workSchdlrClosed) {
        Log.d(PublicConstants.LOG_TAG, "Submit work scheduler, workSchedulerClosed? " + workSchdlrClosed);
        String loadingMsg;
        if (workSchdlrClosed) {
            loadingMsg = AppApplication.appContext.getString(R.string.close_work_scheduler_waiting);
        } else {
            loadingMsg = AppApplication.appContext.getString(R.string.open_work_scheduler_waiting);
        }
        showLoading(loadingMsg);

        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        final SubmitWorkSchedulerRequest request = new SubmitWorkSchedulerRequest();
        request.setWeekId(mCurrSelectedWeekId);
        request.setReadOnly(workSchdlrClosed);
        Call<SubmitWorkSchedulerResponse> submitWorkSchedulerCall = LogWorkApi.getInstance().getService()
                .submitWorkScheduler(userToken, request);
        submitWorkSchedulerCall.enqueue(new Callback<SubmitWorkSchedulerResponse>() {
            @Override
            public void onResponse(Call<SubmitWorkSchedulerResponse> call, Response<SubmitWorkSchedulerResponse> response) {
                hideLoading();
                Log.d(PublicConstants.LOG_TAG, "Submit work scheduler DONE, success? " + response.isSuccessful());
                if (response.isSuccessful()) {
                    showSuccessDialog(workSchdlrClosed);
                } else {
                    setSwSchedulerConfirmState(!workSchdlrClosed);
                    showErrorDialog(workSchdlrClosed);
                }
            }

            @Override
            public void onFailure(Call<SubmitWorkSchedulerResponse> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                setSwSchedulerConfirmState(!workSchdlrClosed);
                showErrorDialog(workSchdlrClosed);
            }
        });
    }

    private void showSuccessDialog(boolean workSchdlrClosed) {
        String title = AppApplication.appContext.getString(R.string.success);
        String content;
        if (workSchdlrClosed) {
            content = AppApplication.appContext.getString(R.string.close_work_scheduler_success);
        } else {
            content = AppApplication.appContext.getString(R.string.open_work_scheduler_success);
        }
        new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(content)
                .show();
    }

    private void showErrorDialog(boolean workSchdlrClosed) {
        String title = AppApplication.appContext.getString(R.string.fail);
        String content;
        if (workSchdlrClosed) {
            content = AppApplication.appContext.getString(R.string.close_work_scheduler_fail);
        } else {
            content = AppApplication.appContext.getString(R.string.open_work_scheduler_fail);
        }
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(content)
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_UPDATE_WORK_SCHEDULER:
                Log.d(PublicConstants.LOG_TAG, "On update work scheduler result? " + resultCode);
                if (resultCode == Activity.RESULT_OK) {
                    loadAllWorkSchedulers(mCurrSelectedWeekId);
                }
                break;
        }
    }

    private void loadAllWorkSchedulers(String currSelectedWeekId) {
        Log.d(PublicConstants.LOG_TAG, "Load all work scheduler, weekId? " + currSelectedWeekId);
        showLoading(getString(R.string.loading_work_scheduler));
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        Call<WeekSchedulerResponse> weekSchedulerCall = LogWorkApi.getInstance().getService()
                .getAllWorkSchedulers(currSelectedWeekId, userToken);
        weekSchedulerCall.enqueue(new Callback<WeekSchedulerResponse>() {
            @Override
            public void onResponse(Call<WeekSchedulerResponse> call, Response<WeekSchedulerResponse> response) {
                hideLoading();
                Log.d(PublicConstants.LOG_TAG, "Load all work scheduler done, success? " + response.isSuccessful());
                if (response.isSuccessful()) {
                    updateWorkSchdlrList(response.body());
                } else {
                    showToast(AppApplication.appContext.getString(R.string.load_work_scheduler_fail), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<WeekSchedulerResponse> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                showToast(AppApplication.appContext.getString(R.string.load_work_scheduler_fail), Toast.LENGTH_SHORT);
            }
        });
    }

    private void updateWorkSchdlrList(WeekSchedulerResponse weekSchedulerRes) {
//        mSwScheduleConfirm.setEnabled(false);
        mSwScheduleConfirm.setOnCheckedChangeListener(null);
        mSwScheduleConfirm.setChecked(weekSchedulerRes.isReadOnly());
        mSwScheduleConfirm.setOnCheckedChangeListener(mSwCheckedChangeListener);
        mSwScheduleConfirm.setEnabled(true);

        mUserSchedulerShifts.clear();
        mUserSchedulerShifts = WorkingScheduleFactory.createWorkSchedulerShifts(weekSchedulerRes);
        Collections.sort(mUserSchedulerShifts, mWorkSchedulerShiftComparator);
        invalidateWorkSchedulerList();
    }

    private void invalidateWorkSchedulerList() {
        WorkSessionInfo workSessionInfo = mWorkSessionInfos.get(mCurrWorkSessInfoIdx);
        List<UserSchedulerShift> filterUserSchedulerShifts = WorkingScheduleFactory
                .filterWorkSchedulerShifts(mUserSchedulerShifts, workSessionInfo);
        mWorkSchedulerAdapter.setWeekId(mCurrSelectedWeekId);
        mWorkSchedulerAdapter.setWorkSessionInfo(workSessionInfo);
        mWorkSchedulerAdapter.setWorkingShifts(filterUserSchedulerShifts);
    }

    private final Comparator<UserSchedulerShift> mWorkSchedulerShiftComparator = new Comparator<UserSchedulerShift>() {
        @Override
        public int compare(UserSchedulerShift o1, UserSchedulerShift o2) {
            int result;
            DateTime o1CheckInDate = DateTimeUtils.stringToDateTime(o1.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            DateTime o2CheckInDate = DateTimeUtils.stringToDateTime(o2.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            if (o1CheckInDate.isBefore(o2CheckInDate)) {
                result = -1;
            } else if (o1CheckInDate.isAfter(o2CheckInDate)) {
                result = 1;
            } else {
                result = 0;
            }
            return result;
        }
    };

    private void loadWorkSessionInfo() {
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        showLoading(getString(R.string.loading_work_session_info));
        Call<List<WorkSessionInfo>> listWorkSessCall = LogWorkApi.getInstance().getService()
                .getWorkSessionInfos(userToken);
        listWorkSessCall.enqueue(new Callback<List<WorkSessionInfo>>() {
            @Override
            public void onResponse(Call<List<WorkSessionInfo>> call, Response<List<WorkSessionInfo>> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    mWorkSessionInfos = response.body();
                    loadAllWorkSchedulers(mCurrSelectedWeekId);
                } else {
                    showToast(AppApplication.appContext.getString(R.string.load_work_session_info_fail), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<List<WorkSessionInfo>> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                showToast(AppApplication.appContext.getString(R.string.load_work_session_info_fail), Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_morning_shift:
                mFirstShiftBtn.setBootstrapBrand(new MainBootstrapBrand());
                mSecondShiftBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
                mExtraShiftBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
                mCurrWorkSessInfoIdx = 0;
                invalidateWorkSchedulerList();
                break;
            case R.id.btn_afternoon_shift:
                mFirstShiftBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
                mSecondShiftBtn.setBootstrapBrand(new MainBootstrapBrand());
                mExtraShiftBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
                mCurrWorkSessInfoIdx = 1;
                invalidateWorkSchedulerList();
                break;
            case R.id.btn_extra_shift:
                mFirstShiftBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
                mSecondShiftBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
                mExtraShiftBtn.setBootstrapBrand(new MainBootstrapBrand());
                mCurrWorkSessInfoIdx = 2;
                invalidateWorkSchedulerList();
                break;
        }
    }

    @Override
    public void onAddEmployeeClick(WorkSchedulerDate workSchedulerDate) {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), AddSchedulerShiftActivity.class);
            WorkSessionInfo workSessionInfo = mWorkSessionInfos.get(mCurrWorkSessInfoIdx);
            intent.putExtra(AddSchedulerShiftActivity.EXTRA_WORK_SESSION_INFO, workSessionInfo);
            intent.putExtra(AddSchedulerShiftActivity.EXTRA_CURRENT_WORK_SCHEDULER_DATE, workSchedulerDate);
            intent.putParcelableArrayListExtra(AddSchedulerShiftActivity.EXTRA_USER_SCHEDULER_LIST,
                    (ArrayList<UserSchedulerShift>) mUserSchedulerShifts);
            startActivityForResult(intent, REQ_UPDATE_WORK_SCHEDULER);
        }
    }

    @Override
    public void onRemoveEmployeeClick(WorkSchedulerDate workSchedulerDate, UserSchedulerShift userSchedulerShift) {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), AddSchedulerShiftActivity.class);
            WorkSessionInfo workSessionInfo = mWorkSessionInfos.get(mCurrWorkSessInfoIdx);
            intent.putExtra(AddSchedulerShiftActivity.EXTRA_WORK_SESSION_INFO, workSessionInfo);
            intent.putExtra(AddSchedulerShiftActivity.EXTRA_CURRENT_WORK_SCHEDULER_DATE, workSchedulerDate);
            intent.putParcelableArrayListExtra(AddSchedulerShiftActivity.EXTRA_USER_SCHEDULER_LIST,
                    (ArrayList<UserSchedulerShift>) mUserSchedulerShifts);
            startActivityForResult(intent, REQ_UPDATE_WORK_SCHEDULER);
        }
    }
}
