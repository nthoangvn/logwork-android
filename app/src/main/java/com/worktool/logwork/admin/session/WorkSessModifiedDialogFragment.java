package com.worktool.logwork.admin.session;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.github.pinball83.maskededittext.MaskedEditText;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.worktool.logwork.MainActivity;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.user.session.WorkSession;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


/**
 * A simple {@link Fragment} subclass.
 */
public class WorkSessModifiedDialogFragment extends DialogFragment {

    private static final String WORK_SESS_TIME_INPUT_FMT = "HHmm";

    public interface OnWorkSessTimeChangeListener {
        public void onWorkSessTimeChanged(WorkSession workSession);
        public void onWorkSessTimeCanceled();
    }

    private WorkSession mWorkSession;
    private OnWorkSessTimeChangeListener mOnSessModifiedListener;
    private MaskedEditText mTextCheckInTime;
    private MaskedEditText mTextCheckOutTime;
    private View mBtnConfirm;
    private View mBtnCancel;

    public WorkSessModifiedDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        setCancelable(false);
    }

    public void setWorkSession(WorkSession workSession) {
        mWorkSession = new WorkSession();
        mWorkSession.copy(workSession);
    }

    public void setOnSessModifiedListener(OnWorkSessTimeChangeListener onSessModifiedListener) {
        mOnSessModifiedListener = onSessModifiedListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_work_sess_modified_dialog, null);
//        view = inflater.inflate(R.layout.fragment_work_sess_modified_dialog, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTextCheckInTime = view.findViewById(R.id.text_checkin_time);
        mTextCheckOutTime = view.findViewById(R.id.text_checkout_time);
        mBtnConfirm = view.findViewById(R.id.btn_ok);
        mBtnCancel = view.findViewById(R.id.btn_cancel);

        String checkInDate;
        if (!TextUtils.isEmpty(mWorkSession.getConfirmedCheckInTime())) {
            checkInDate = mWorkSession.getConfirmedCheckInTime();
        } else {
            checkInDate = mWorkSession.getCheckInTime();
        }
        String checkInTime = DateTimeUtils.convertUtcStringToDefault(
                checkInDate, PublicConstants.USER_PROFILE_DATE_FMT, WORK_SESS_TIME_INPUT_FMT);
        mTextCheckInTime.setMaskedText(checkInTime);

        String checkOutDate;
        if (!TextUtils.isEmpty(mWorkSession.getConfirmedCheckInTime())) {
            checkOutDate = mWorkSession.getConfirmedCheckOutTime();
        } else {
            checkOutDate = mWorkSession.getCheckOutTime();
        }
        String checkOutTime = DateTimeUtils.convertUtcStringToDefault(
                checkOutDate, PublicConstants.USER_PROFILE_DATE_FMT, WORK_SESS_TIME_INPUT_FMT);
        mTextCheckOutTime.setMaskedText(checkOutTime);

        setupPositiveButton();
        setupNegativeButton();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    private void setupNegativeButton() {
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnSessModifiedListener != null) {
                    mOnSessModifiedListener.onWorkSessTimeCanceled();
                }
            }
        });
    }

    private void setupPositiveButton() {
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnSessModifiedListener != null) {
                    DateTime checkInDate;
                    if (!TextUtils.isEmpty(mWorkSession.getConfirmedCheckInTime())) {
                        checkInDate = DateTimeUtils.stringToDateTime(
                                mWorkSession.getConfirmedCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                    } else {
                        checkInDate = DateTimeUtils.stringToDateTime(
                                mWorkSession.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                    }
                    String newCheckInTime = mTextCheckInTime.getText().toString().trim();
                    String newCheckOutTime = mTextCheckOutTime.getText().toString().trim();
                    Log.d(PublicConstants.LOG_TAG, "New check in time? " + newCheckInTime + ", new check out time? " + newCheckOutTime);

                    int checkInHour = getHourValue(newCheckInTime);
                    int checkInMin = getMinValue(newCheckInTime);
                    int checkOutHour = getHourValue(newCheckOutTime);
                    int checkOutMin = getMinValue(newCheckOutTime);
                    if (isTimeValid(checkInHour, checkInMin) && isTimeValid(checkOutHour, checkOutMin)) {
                        DateTime confirmCheckInDate = checkInDate.withHourOfDay(checkInHour).withMinuteOfHour(checkInMin);
                        DateTime confirmCheckOutDate = checkInDate.withHourOfDay(checkOutHour).withMinuteOfHour(checkOutMin);
                        String confirmCheckInStr = DateTimeUtils.dateTimeToString(confirmCheckInDate, PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                        String confirmCheckOutStr = DateTimeUtils.dateTimeToString(confirmCheckOutDate, PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                        mWorkSession.setConfirmedCheckInTime(confirmCheckInStr);
                        mWorkSession.setConfirmedCheckOutTime(confirmCheckOutStr);
                        mOnSessModifiedListener.onWorkSessTimeChanged(mWorkSession);
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.work_session_time_is_invalid), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private int getHourValue(String hourAndMinStr) {
        int hour = -1;
        try {
            String[] hourAndMin = hourAndMinStr.split(":");
            if (hourAndMin.length == 2) {
                try {
                    hour = Integer.parseInt(hourAndMin[0]);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hour;
    }

    private int getMinValue(String hourAndMinStr) {
        int min = -1;
        try {
            String[] hourAndMin = hourAndMinStr.split(":");
            if (hourAndMin.length == 2) {
                try {
                    min = Integer.parseInt(hourAndMin[1]);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return min;
    }

    private boolean isTimeValid(int hour, int min) {
        boolean isValid = false;
        if (hour > -1 && hour < 24 && min > -1 && min < 60) {
            isValid = true;
        }
        return isValid;
    }
}
