package com.worktool.logwork.admin.schedule;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.schedule.WorkingDateViewHolder;
import com.worktool.logwork.schedule.WorkingScheduleFactory;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.user.session.WorkSessionInfo;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THAIHOANG on 10/31/2017.
 */

public class WorkSchedulerManagementAdapter extends SectioningAdapter {

    public interface WorkSchedulerManagementClickListener {
        public void onAddEmployeeClick(WorkSchedulerDate workSchedulerDate);
        public void onRemoveEmployeeClick(WorkSchedulerDate workSchedulerDate, UserSchedulerShift userSchedulerShift);
    }

    private Context mContext;
    private String mWeekId;
    private List<UserSchedulerShift> mWorkSchdlrShifts;
    private List<WorkSchedulerDate> mWorkSchdlrDates;
    private WorkSessionInfo mWorkSessionInfo;
    private WorkSchedulerManagementClickListener mListener;

    public WorkSchedulerManagementAdapter(Context context) {
        mContext = context;
        mWorkSchdlrShifts = new ArrayList<>();
        mWorkSchdlrDates = new ArrayList<>();
        mWeekId = DateTimeUtils.dateTimeToString(DateTime.now(), PublicConstants.WEEK_ID_FMT);
    }

    public void setListener(WorkSchedulerManagementClickListener listener) {
        mListener = listener;
    }

    public void setWeekId(String weekId) {
        mWeekId = weekId;
    }

    public void setWorkSessionInfo(WorkSessionInfo workSessionInfo) {
        mWorkSessionInfo = workSessionInfo;
    }

    public void setWorkingShifts(List<UserSchedulerShift> workingShifts) {
        mWorkSchdlrShifts.clear();
        if (workingShifts != null && workingShifts.size() > 0) {
            mWorkSchdlrShifts = new ArrayList<>(workingShifts);
        }

        mWorkSchdlrDates.clear();
        mWorkSchdlrDates = WorkingScheduleFactory.groupWorkSchedulerShiftsByDayShift(mWeekId, mWorkSchdlrShifts, mWorkSessionInfo);
//        Log.d(PublicConstants.LOG_TAG, "Build work scheduler group size? " + mWorkSchdlrDates.size());
        notifyAllSectionsDataSetChanged();
    }

    @Override
    public int getNumberOfSections() {
        return mWorkSchdlrDates.size();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        return mWorkSchdlrDates.get(sectionIndex).getShifts().size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public boolean doesSectionHaveFooter(int sectionIndex) {
        return false;
    }

    @Override
    public WorkingSchedulerShiftViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.work_scheduler_shift_item, parent, false);
        return new WorkingSchedulerShiftViewHolder(v);
    }

    @Override
    public WorkingDateViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.working_date_item, parent, false);
        return new WorkingDateViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, final int sectionIndex, final int itemIndex, int itemType) {
        final WorkSchedulerDate workingDate = mWorkSchdlrDates.get(sectionIndex);
        WorkingSchedulerShiftViewHolder itemViewHolder = (WorkingSchedulerShiftViewHolder) viewHolder;
        final UserSchedulerShift workingShift = workingDate.getShifts().get(itemIndex);
        itemViewHolder.textShiftName.setText(workingShift.getFullName() + " - " + workingShift.getUserId());
        itemViewHolder.imgRemoveEmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onRemoveEmployeeClick(workingDate, workingShift);
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex, int headerType) {
        final WorkSchedulerDate workingSchdlrDate = mWorkSchdlrDates.get(sectionIndex);
        WorkingDateViewHolder headerViewHolder = (WorkingDateViewHolder) viewHolder;

        DateTime workTime = DateTimeUtils.stringToDateTime(workingSchdlrDate.getDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
        headerViewHolder.textWorkingDate.setText(DateTimeUtils.dateTimeToString(
                workTime, PublicConstants.WORKING_SCHEDULER_HEADER_FMT));
        headerViewHolder.textWorkingDate.setBackgroundResource(R.color.main_text_color);
        headerViewHolder.titleDivider.setBackgroundResource(R.color.main_text_color);
        headerViewHolder.textWorkSessTime.setText(mContext.getString(R.string.scheduler_sess_time_info,
                workingSchdlrDate.getRegisteredTime(), workingSchdlrDate.getTotalTime()));
        headerViewHolder.textWorkSessTime.setVisibility(View.VISIBLE);
        headerViewHolder.imgAddEmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onAddEmployeeClick(workingSchdlrDate);
                }
            }
        });
    }
}
