package com.worktool.logwork.admin.profile;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapDropDown;
import com.worktool.logwork.AppApplication;
import com.worktool.logwork.BaseFragment;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.admin.session.WorkSessionActivity;
import com.worktool.logwork.view.bootstrap.CustomBootstrapBrand;
import com.worktool.logwork.view.bootstrap.MainBootstrapBrand;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.jobposition.JobPosition;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserManagementFragment extends BaseFragment implements UserManagementAdapter.OnUserClickListener {

    public static final String EXTRA_USER_PROFILE = "UserManagementFragment_extra_user_profile";

    private UserProfile mUserProfile;
    private RecyclerView mUserList;
    private UserManagementAdapter mUserAdapter;
    private SharedPreferences mAppSettings;
    private List<UserProfile> mUserProfiles;
    private List<JobPosition> mJobPositions;
    private String[] mJobPositionData;
    private String[] mUserBranchData;
    private BootstrapDropDown mSpinnerUserBranch, mSpinnerUserPosition;

    public UserManagementFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserProfile = getArguments().getParcelable(EXTRA_USER_PROFILE);
        mAppSettings = getActivity().getApplicationContext().getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);
        mJobPositions = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUserList = view.findViewById(R.id.user_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mUserList.setLayoutManager(layoutManager);
        mUserList.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getActivity())
                        .colorResId(R.color.main_text_color)
                        .size(3)
                        .build());

        mUserAdapter = new UserManagementAdapter(getActivity().getApplicationContext());
        mUserAdapter.setUserClickListener(this);
        mUserList.setAdapter(mUserAdapter);

        mSpinnerUserBranch = view.findViewById(R.id.spinner_user_branch);
        mSpinnerUserPosition = view.findViewById(R.id.spinner_user_position);
        setupUserFilters();

        loadUserPositions();
//        loadUserList();
    }

    private void setupUserFilters() {
        String branchFilter;
        if (!TextUtils.isEmpty(mUserProfile.getBranch())) {
            branchFilter = mUserProfile.getBranch();
        } else {
            branchFilter = AppApplication.appContext.getString(R.string.user_profile_filter_all);
        }
        String userBranchTitle = AppApplication.appContext.getString(R.string.user_branch_filter, branchFilter);
        mSpinnerUserBranch.setText(userBranchTitle);
        mSpinnerUserBranch.setBootstrapBrand(new MainBootstrapBrand());
        mUserBranchData = new String[] {branchFilter};
        mSpinnerUserBranch.setDropdownData(mUserBranchData);

        String filterAll = AppApplication.appContext.getString(R.string.user_profile_filter_all);
        String userPosTitle = AppApplication.appContext.getString(R.string.user_position_filter, filterAll);
        mSpinnerUserPosition.setText(userPosTitle);
        mSpinnerUserPosition.setBootstrapBrand(new MainBootstrapBrand());
        mJobPositionData = new String[] {filterAll};
        mSpinnerUserPosition.setDropdownData(mJobPositionData);
    }

    private void loadUserPositions() {
        showLoading(getString(R.string.loading_user_list));
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        Call<List<JobPosition>> jobPositionsCall = LogWorkApi.getInstance().getService()
                .getJobPostions(userToken);
        jobPositionsCall.enqueue(new Callback<List<JobPosition>>() {
            @Override
            public void onResponse(Call<List<JobPosition>> call, Response<List<JobPosition>> response) {
                Log.d(PublicConstants.LOG_TAG, "Load user positions completed, success? " + response.isSuccessful());
                if (response.isSuccessful()) {
                    mJobPositions = response.body();
                    loadUserList();
                } else {
                    hideLoading();
                    showToast(AppApplication.appContext.getString(R.string.load_user_list_fail), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<List<JobPosition>> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                showToast(AppApplication.appContext.getString(R.string.load_user_list_fail), Toast.LENGTH_SHORT);
            }
        });
    }

    private void loadUserList() {
//        showLoading(getString(R.string.loading_user_list));
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        Call<List<UserProfile>> userProfilesCall = LogWorkApi.getInstance().getService()
                .getUserList(userToken);
        userProfilesCall.enqueue(new Callback<List<UserProfile>>() {
            @Override
            public void onResponse(Call<List<UserProfile>> call, Response<List<UserProfile>> response) {
                hideLoading();
                Log.d(PublicConstants.LOG_TAG, "Load user list completed, success? " + response.isSuccessful());
                if (response.isSuccessful()) {
                    mUserProfiles = response.body();
                    showUserList(AppApplication.appContext.getString(R.string.user_profile_filter_all));
                    updateUserFilters();
                } else {
                    showToast(AppApplication.appContext.getString(R.string.load_user_list_fail), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<List<UserProfile>> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                showToast(AppApplication.appContext.getString(R.string.load_user_list_fail), Toast.LENGTH_SHORT);
            }
        });
    }

    private void showUserList(String currPosFilter) {
        List<UserProfile> filterProfiles = null;
        String allPosFilter = AppApplication.appContext.getString(R.string.user_profile_filter_all);
        if (currPosFilter == null || currPosFilter.equalsIgnoreCase(allPosFilter)) {
            filterProfiles = mUserProfiles;
        } else {
            filterProfiles = new ArrayList<>();
            for (UserProfile userProfile : mUserProfiles) {
                if (currPosFilter.equalsIgnoreCase(userProfile.getEmpRole())) {
                    filterProfiles.add(userProfile);
                }
            }
        }

        if (mUserAdapter != null) {
            mUserAdapter.setUserProfiles(filterProfiles);
        }
    }

    private String mCurrPos;
    private void updateUserFilters() {
        mJobPositionData = new String[mJobPositions.size() + 1];
        mJobPositionData[0] = getString(R.string.user_profile_filter_all);
        for (int i=0; i<mJobPositions.size(); i++) {
            JobPosition jobPosition = mJobPositions.get(i);
            mJobPositionData[i+1] = jobPosition.getTitle().trim();
        }
        mSpinnerUserPosition.setDropdownData(mJobPositionData);
        mSpinnerUserPosition.setOnDropDownItemClickListener(new BootstrapDropDown.OnDropDownItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View v, int id) {
                Log.d(PublicConstants.LOG_TAG, "On dropdown item click listener, id: " + id);
                mCurrPos = mJobPositionData[id];
                String spinnerTitle = AppApplication.appContext.getString(R.string.user_position_filter, mCurrPos);
                mSpinnerUserPosition.setText(spinnerTitle);
                showUserList(mCurrPos);
            }
        });
    }

    @Override
    public void onWorkDateClick(UserProfile userProfile) {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), WorkSessionActivity.class);
            intent.putExtra(WorkSessionActivity.EXTRA_USER_PROFILE, userProfile);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public void onUserProfileClick(UserProfile userProfile) {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), EmployeeDetailActivity.class);
            intent.putExtra(EmployeeDetailActivity.EXTRA_EMPLOYEE_PROFILE, userProfile);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }
}
