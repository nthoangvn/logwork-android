package com.worktool.logwork.admin.schedule;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THAIHOANG on 11/8/2017.
 */

public class UserSchedulerWeek {
    private String mUserId;
    private String mWeekId;
    private String mFullName;
    private List<UserSchedulerShift> mWorkScheduler = new ArrayList<>();

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getWeekId() {
        return mWeekId;
    }

    public void setWeekId(String weekId) {
        mWeekId = weekId;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public List<UserSchedulerShift> getWorkScheduler() {
        return mWorkScheduler;
    }

    public void setWorkScheduler(List<UserSchedulerShift> workScheduler) {
        mWorkScheduler = workScheduler;
    }
}
