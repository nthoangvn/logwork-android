package com.worktool.logwork.admin.session;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.user.session.WorkSession;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THAIHOANG on 10/29/2017.
 */

public class UserWorkSessionAdapter extends RecyclerView.Adapter<UserWorkSessionViewHolder> {

    public interface OnUserWorkSessionClick {
        public void onUserWorkSessionEditClick(WorkSession workSession);
    }

    private OnUserWorkSessionClick mUserWorkSessionClick;
    private List<WorkSession> mWorkSessions;
    private Context mContext;

    public UserWorkSessionAdapter(Context context) {
        mContext = context;
        mWorkSessions = new ArrayList<>();
    }

    public void setUserWorkSessionClick(OnUserWorkSessionClick userWorkSessionClick) {
        mUserWorkSessionClick = userWorkSessionClick;
    }

    public void setWorkSessions(List<WorkSession> workSessions) {
        mWorkSessions.clear();
        if (workSessions != null && workSessions.size() > 0) {
            mWorkSessions = new ArrayList<>(workSessions);
        }
        notifyDataSetChanged();
    }

    @Override
    public UserWorkSessionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_work_session_list_item, parent, false);
        return new UserWorkSessionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UserWorkSessionViewHolder holder, int position) {
        final WorkSession workSession = mWorkSessions.get(position);
        DateTime checkInDate;
        if (!TextUtils.isEmpty(workSession.getConfirmedCheckInTime())) {
            checkInDate = DateTimeUtils.stringToDateTime(workSession.getConfirmedCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
        } else {
            checkInDate = DateTimeUtils.stringToDateTime(workSession.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
        }
        String workDate = DateTimeUtils.dateTimeToString(checkInDate, PublicConstants.DAY_OF_MONTH_FMT);
        holder.textSessDate.setText(workDate + " - " + workSession.getSessionName());
        holder.textSessDate.setTextColor(mContext.getResources().getColor(R.color.main_text_color));

        holder.textCheckInTime.setOnCheckedChangeListener(null);
        if (!TextUtils.isEmpty(workSession.getConfirmedCheckInTime())) {
            holder.textCheckInTime.setChecked(true);
        } else {
            holder.textCheckInTime.setChecked(false);
        }
        holder.textCheckInTime.setText(DateTimeUtils.dateTimeToString(checkInDate, PublicConstants.WORK_SESSION_TIME_FMT));
        holder.textCheckInTime.setOnCheckedChangeListener(mAutoReverseCheckedChangeListener);

        DateTime checkOutDate;
        if (!TextUtils.isEmpty(workSession.getConfirmedCheckOutTime())) {
            checkOutDate = DateTimeUtils.stringToDateTime(workSession.getConfirmedCheckOutTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
        } else {
            checkOutDate = DateTimeUtils.stringToDateTime(workSession.getCheckOutTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
        }

        holder.textCheckOutTime.setOnCheckedChangeListener(null);
        if (!TextUtils.isEmpty(workSession.getConfirmedCheckOutTime())) {
            holder.textCheckOutTime.setChecked(true);
        } else {
            holder.textCheckOutTime.setChecked(false);
        }
        holder.textCheckOutTime.setText(DateTimeUtils.dateTimeToString(checkOutDate, PublicConstants.WORK_SESSION_TIME_FMT));
        holder.textCheckOutTime.setOnCheckedChangeListener(mAutoReverseCheckedChangeListener);



        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUserWorkSessionClick != null) {
                    mUserWorkSessionClick.onUserWorkSessionEditClick(workSession);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mWorkSessions.size();
    }

    private CompoundButton.OnCheckedChangeListener mAutoReverseCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.d(PublicConstants.LOG_TAG, "On work session time check changed, isCheck? " + isChecked);
            buttonView.setChecked(!isChecked);
        }
    };
}
