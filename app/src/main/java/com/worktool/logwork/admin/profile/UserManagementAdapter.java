package com.worktool.logwork.admin.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.worktool.logwork.R;
import com.worktool.logwork.image.glide.GlideApp;
import com.worktool.logwork.util.UrlUtils;
import com.worktool.serverapi.logwork.user.UserProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THAIHOANG on 10/29/2017.
 */

public class UserManagementAdapter extends RecyclerView.Adapter<UserViewHolder> {

    public interface OnUserClickListener {
        public void onWorkDateClick(UserProfile userProfile);
        public void onUserProfileClick(UserProfile userProfile);
    }

    private Context mContext;
    private List<UserProfile> mUserProfiles;
    private OnUserClickListener mUserClickListener;

    public UserManagementAdapter(Context context) {
        mContext = context;
        mUserProfiles = new ArrayList<>();
    }

    public void setUserProfiles(List<UserProfile> userProfiles) {
        mUserProfiles.clear();
        if (userProfiles != null && userProfiles.size() > 0) {
            mUserProfiles = new ArrayList<>(userProfiles);
        }
        notifyDataSetChanged();
    }

    public void setUserClickListener(OnUserClickListener userClickListener) {
        mUserClickListener = userClickListener;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        final UserProfile userProfile = mUserProfiles.get(position);

        String avatarUrl = UrlUtils.getAvatarDomain() + userProfile.getProfilePicture();
        GlideApp.with(mContext)
                .load(avatarUrl)
                .error(R.drawable.ic_avatar_default)
                .fitCenter()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(holder.imgAvatar);

        holder.textFullName.setText(userProfile.getFullName() + " - " + userProfile.getEmpId());
        holder.textPhoneNumber.setText(userProfile.getPhoneNumber());
        if (!TextUtils.isEmpty(userProfile.getBranch())) {
            holder.textBranch.setVisibility(View.VISIBLE);
            holder.textBranch.setText(userProfile.getBranch());
        } else {
            holder.textBranch.setVisibility(View.GONE);
        }

        holder.buttonWorkDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUserClickListener != null) {
                    mUserClickListener.onWorkDateClick(userProfile);
                }
            }
        });

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUserClickListener != null) {
                    mUserClickListener.onUserProfileClick(userProfile);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUserProfiles.size();
    }
}
