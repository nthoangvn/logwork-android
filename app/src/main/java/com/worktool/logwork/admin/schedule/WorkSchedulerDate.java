package com.worktool.logwork.admin.schedule;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THAIHOANG on 10/31/2017.
 */

public class WorkSchedulerDate implements Parcelable {
    private String mDate;
    private int mTotalTime;
    private int mRegisteredTime;
    private List<UserSchedulerShift> mShifts;

    public WorkSchedulerDate() {
        mShifts = new ArrayList<>();
    }

    protected WorkSchedulerDate(Parcel in) {
        mDate = in.readString();
        mTotalTime = in.readInt();
        mRegisteredTime = in.readInt();
        mShifts = in.createTypedArrayList(UserSchedulerShift.CREATOR);
    }

    public static final Creator<WorkSchedulerDate> CREATOR = new Creator<WorkSchedulerDate>() {
        @Override
        public WorkSchedulerDate createFromParcel(Parcel in) {
            return new WorkSchedulerDate(in);
        }

        @Override
        public WorkSchedulerDate[] newArray(int size) {
            return new WorkSchedulerDate[size];
        }
    };

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public List<UserSchedulerShift> getShifts() {
        return mShifts;
    }

    public void setShifts(List<UserSchedulerShift> mShifts) {
        this.mShifts = mShifts;
    }

    public int getTotalTime() {
        return mTotalTime;
    }

    public void setTotalTime(int totalTime) {
        mTotalTime = totalTime;
    }

    public int getRegisteredTime() {
        return mRegisteredTime;
    }

    public void setRegisteredTime(int registeredTime) {
        mRegisteredTime = registeredTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDate);
        dest.writeInt(mTotalTime);
        dest.writeInt(mRegisteredTime);
        dest.writeTypedList(mShifts);
    }
}
