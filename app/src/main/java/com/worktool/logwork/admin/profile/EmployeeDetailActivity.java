package com.worktool.logwork.admin.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.worktool.logwork.LoadingSupportActivity;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.image.glide.GlideApp;
import com.worktool.logwork.user.login.LoginActivity;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.logwork.util.UrlUtils;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.benefit.Benefit;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.worktool.serverapi.logwork.user.checkin.AdminStartWorkSessRequest;
import com.worktool.serverapi.logwork.user.checkin.UserStartWorkSessResponse;
import com.worktool.serverapi.logwork.user.checkout.AdminStopSessionRequest;
import com.worktool.serverapi.logwork.user.checkout.StopWorkSessResponse;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeDetailActivity extends LoadingSupportActivity {

    public static final String EXTRA_EMPLOYEE_PROFILE = "EmployeeDetailActivity_extra_employee_profile";
    private UserProfile mUserProfile;
    private SharedPreferences mAppSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mAppSettings = getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);
        mUserProfile = getIntent().getParcelableExtra(EXTRA_EMPLOYEE_PROFILE);
        setupEmployeeProfileLayout();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_employee_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_start_work_sess:
                // do whatever
                confirmStartWorkSession();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupEmployeeProfileLayout() {
        ViewGroup rootView = findViewById(R.id.layout_profile_header);
//        Blurry.with(this)
//                .radius(10)
//                .sampling(8)
//                .color(Color.argb(66, 255, 255, 0))
//                .async()
//                .onto(rootView);
//        Blurry.with(this).radius(25).sampling(2).onto((ViewGroup) rootView);
        ImageView imgProfile = findViewById(R.id.img_profile);
        String avatarUrl = UrlUtils.getAvatarDomain() + mUserProfile.getProfilePicture();
        GlideApp.with(this)
                .load(avatarUrl)
                .error(R.drawable.ic_avatar_default)
                .fitCenter()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imgProfile);

        // Full name
        TextView fullNameText = findViewById(R.id.text_full_name_value);
        fullNameText.setText(mUserProfile.getFullName());
        TextView fullNameTitleText = findViewById(R.id.text_full_name);
        fullNameTitleText.setText(mUserProfile.getFullName() + " - " + mUserProfile.getEmpId());

        // Email
        TextView emailText = findViewById(R.id.text_email_value);
        emailText.setText(mUserProfile.getEmail());
        TextView emailTitleText = findViewById(R.id.text_email);
        emailTitleText.setText(mUserProfile.getEmpRole());

        // User name
        TextView userNameText = findViewById(R.id.text_user_name_value);
        userNameText.setText(mUserProfile.getUsername());

        // Emp ID
        TextView empIdText = findViewById(R.id.text_emp_id_value);
        empIdText.setText(mUserProfile.getEmpId());

        // Emp position
        TextView empRoleText = findViewById(R.id.text_emp_role_value);
        empRoleText.setText(mUserProfile.getEmpRole());

        // Phone number
        TextView phoneNumberText = findViewById(R.id.text_phone_number_value);
        phoneNumberText.setText(mUserProfile.getPhoneNumber());

        // Address
        TextView addressText = findViewById(R.id.text_address_value);
        addressText.setText(mUserProfile.getAddress());

        // Birthday
        TextView birthDayText = findViewById(R.id.text_birthday_value);
        if (!TextUtils.isEmpty(mUserProfile.getBirthDay())) {
            DateTime userBirthDay = DateTimeUtils.stringToDateTime(mUserProfile.getBirthDay(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            birthDayText.setText(DateTimeUtils.dateTimeToString(userBirthDay, PublicConstants.BIRTHDAY_DATE_FMT));
        } else {
            birthDayText.setText("");
        }

        // End probation date
        TextView textEndProbation = findViewById(R.id.text_end_probation_value);
        if (!TextUtils.isEmpty(mUserProfile.getEndProbationAt())) {
            DateTime endProbDate = DateTimeUtils.stringToDateTime(mUserProfile.getEndProbationAt(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            textEndProbation.setText(DateTimeUtils.dateTimeToString(endProbDate, PublicConstants.GENERAL_DATE_FMT));
        } else {
            textEndProbation.setText("");
        }

        if (mUserProfile.getBenefits() != null && mUserProfile.getBenefits().size() > 0) {
            String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
            Call<List<Benefit>> benefitCall = LogWorkApi.getInstance().getService()
                    .getAllBenefits(userToken);
            benefitCall.enqueue(new Callback<List<Benefit>>() {
                @Override
                public void onResponse(Call<List<Benefit>> call, Response<List<Benefit>> response) {
                    Log.d(PublicConstants.LOG_TAG, "Query all benefits done, success? " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        updateBenefits(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<Benefit>> call, Throwable t) {
                    t.printStackTrace();
                    Log.d(PublicConstants.LOG_TAG, "Query all benefits fail");
                }
            });
        }
    }

    private void updateBenefits(List<Benefit> allBenefits) {
        if (allBenefits != null && allBenefits.size() > 0) {
            final TextView textBenefit = findViewById(R.id.text_benefit_value);
            if (textBenefit != null) {
                StringBuilder builder = new StringBuilder();
                for (String benefitId : mUserProfile.getBenefits()) {
                    for (Benefit benefit : allBenefits) {
                        if (benefitId.equals(benefit.getId())) {
                            builder.append(benefit.getName());
                            builder.append(" - ");
                            builder.append(benefit.getValue());
                            builder.append("\n");
                            break;
                        }
                    }
                }

                String userBenefits = builder.toString();
                textBenefit.setText(userBenefits);
            }
        }
    }

    private void confirmStartWorkSession() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.start_work_session))
                .setContentText(getString(R.string.start_work_session_for_user_confirm, mUserProfile.getFullName()))
                .setCancelText(getString(R.string.cancel))
                .setConfirmText(getString(R.string.yes))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        adminStartWorkSession();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
    }

    private void adminStartWorkSession() {
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        if (!TextUtils.isEmpty(userToken)) {
            showLoading(getString(R.string.start_work_session_waiting));
            AdminStartWorkSessRequest startWorkSessReq = new AdminStartWorkSessRequest();
            startWorkSessReq.setUserId(mUserProfile.getEmpId());
            Call<UserStartWorkSessResponse> startWorkSessCall = LogWorkApi.getInstance().getService()
                    .adminStartWorkSession(userToken, startWorkSessReq);
            startWorkSessCall.enqueue(new Callback<UserStartWorkSessResponse>() {
                @Override
                public void onResponse(Call<UserStartWorkSessResponse> call, Response<UserStartWorkSessResponse> response) {
                    Log.d(PublicConstants.LOG_TAG, "Admin start work session completed, success? " + response.isSuccessful());
                    hideLoading();
                    if (response.isSuccessful()) {
                        Toast.makeText(EmployeeDetailActivity.this, R.string.start_work_session_success, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EmployeeDetailActivity.this, R.string.start_work_session_fail, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserStartWorkSessResponse> call, Throwable t) {
                    t.printStackTrace();
                    Log.d(PublicConstants.LOG_TAG, "Admin start work session fail");
                    hideLoading();
                    Toast.makeText(EmployeeDetailActivity.this, R.string.start_work_session_fail, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            goToLoginScreen();
        }
    }

    private void adminStopWorkSession() {
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        if (!TextUtils.isEmpty(userToken)) {
            showLoading(getString(R.string.stop_work_session_waiting));
            AdminStopSessionRequest stopWorkSessReq = new AdminStopSessionRequest();
            stopWorkSessReq.setSessionId(null);
            Call<StopWorkSessResponse> stopWorkSessCall = LogWorkApi.getInstance().getService()
                    .adminStopWorkSession(userToken, stopWorkSessReq);
            stopWorkSessCall.enqueue(new Callback<StopWorkSessResponse>() {
                @Override
                public void onResponse(Call<StopWorkSessResponse> call, Response<StopWorkSessResponse> response) {
                    Log.d(PublicConstants.LOG_TAG, "Admin stop work session completed, success? " + response.isSuccessful());
                    hideLoading();
                    if (response.isSuccessful()) {
                        Toast.makeText(EmployeeDetailActivity.this, R.string.stop_work_session_success, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EmployeeDetailActivity.this, R.string.stop_work_session_fail, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<StopWorkSessResponse> call, Throwable t) {
                    t.printStackTrace();
                    Log.d(PublicConstants.LOG_TAG, "Admin stop work session fail");
                    hideLoading();
                    Toast.makeText(EmployeeDetailActivity.this, R.string.stop_work_session_fail, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            goToLoginScreen();
        }
    }

    private void goToLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
