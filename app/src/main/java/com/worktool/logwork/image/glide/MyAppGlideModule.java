package com.worktool.logwork.image.glide;

/**
 * Created by THAIHOANG on 11/9/2017.
 */

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}
