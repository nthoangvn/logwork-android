package com.worktool.logwork.util;

import android.text.TextUtils;
import android.util.Log;

import com.worktool.logwork.PublicConstants;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;

/**
 * Created by hoang on 11/20/15.
 */
public class DateTimeUtils {

    /**
     * Parse a UTC time string, return a date correspond to input timestamp.
     *
     * @param utcTime    UTC date time string.
     * @param dateFormat Date format that app want to parse.
     * @return UTC timestamp.
     */
    public static Date parseUtcTimestamp(String utcTime, String dateFormat) {
        Date date = null;
        if (!TextUtils.isEmpty(utcTime) && !TextUtils.isEmpty(dateFormat)) {
            SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.US);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            if (format != null) {
                try {
                    date = format.parse(utcTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                Log.i(PublicConstants.LOG_TAG, "parseUtcTimestamp wrong date format? " + dateFormat);
            }
        } else {
            Log.i(PublicConstants.LOG_TAG, "parseUtcTimestamp invalid input: utcTime: " + utcTime + ", dateFormat: " + dateFormat);
        }
        return date;
    }

    public static String date2String(Date utcDate, String dateFormat) {
        String dateStr = null;
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.US);
        format.setTimeZone(TimeZone.getDefault());

        dateStr = format.format(utcDate);

        return dateStr;
    }

    public static String unixTimestamp2string(long unixTimeStamp, String dateFormat) {
        String output = null;
        if (unixTimeStamp > 0 && !TextUtils.isEmpty(dateFormat)) {
            Date thatDay = new Date(unixTimeStamp);
            SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.US);
            format.setTimeZone(TimeZone.getDefault());
            output = format.format(thatDay);
        } else {
            Log.i(PublicConstants.LOG_TAG, "parseUtcTimestamp invalid input: utcTime: " + output + ", dateFormat: " + dateFormat);
        }
        return output;
    }

    public static DateTime getStartDayOfThisWeek() {
        DateTime startDayOfWeek = null;
        DateTime now = DateTime.now();
        startDayOfWeek = now.withDayOfWeek(DateTimeConstants.MONDAY)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0);
        return startDayOfWeek;
    }

    public static DateTime getStartDayOfWeek(DateTime dateTime) {
        DateTime startDayOfWeek = null;
        startDayOfWeek = dateTime.withDayOfWeek(DateTimeConstants.MONDAY)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0);
        return startDayOfWeek;
    }

    /**
     * Convert date time to string in default timezone
     * @param dateTime Date time object in UTC
     * @param pattern Date time format
     * @return The string represent date time in default timezone.
     */
    public static String dateTimeToString(DateTime dateTime, String pattern) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern(pattern)
                .withLocale(new Locale("vi"))
                .withZone(DateTimeZone.getDefault());
        return dtf.print(dateTime);
    }

    public static String dateTimeToString(DateTime dateTime, String pattern, DateTimeZone dateTimeZone) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern(pattern)
                .withLocale(new Locale("vi"))
                .withZone(dateTimeZone);
        return dtf.print(dateTime);
    }

    public static DateTime stringToDateTime(String dateTimeStr, String pattern) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern(pattern).withZone(DateTimeZone.getDefault());
        return dtf.parseDateTime(dateTimeStr).withZone(DateTimeZone.getDefault());
    }

    public static DateTime stringToDateTime(String dateTimeStr, String pattern, DateTimeZone dateTimeZone) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern(pattern).withZone(dateTimeZone);
        return dtf.parseDateTime(dateTimeStr).withZone(DateTimeZone.getDefault());
    }

    public static String convertUtcStringFormat(String utcDateTimeStr, String oldUtcPattern, String newUtcPattern) {
        String utcDateStr;
        DateTime defaultDateTime = stringToDateTime(utcDateTimeStr, oldUtcPattern, DateTimeZone.UTC);
        utcDateStr = dateTimeToString(defaultDateTime, newUtcPattern, DateTimeZone.UTC);
        return utcDateStr;
    }

    public static String convertUtcStringToDefault(String utcDateTimeStr, String utcPattern, String defaultPattern) {
        String defaultDateStr;
        DateTime defaultDateTime = stringToDateTime(utcDateTimeStr, utcPattern, DateTimeZone.UTC);
        defaultDateStr = dateTimeToString(defaultDateTime, defaultPattern);
        return defaultDateStr;
    }

    public static DateTime getFirstDateOfTheMonth() {
        DateTime dateTime = DateTime.now();
        return dateTime.dayOfMonth().withMinimumValue();
    }

    public static DateTime getLastDateOfTheMonth() {
        DateTime dateTime = DateTime.now();
        return dateTime.dayOfMonth().withMaximumValue();
    }
}
