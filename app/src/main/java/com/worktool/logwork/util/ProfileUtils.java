package com.worktool.logwork.util;

import android.util.Log;

import com.worktool.logwork.PublicConstants;

import java.util.List;

/**
 * Created by THAIHOANG on 11/15/2017.
 */

public class ProfileUtils {

    public static boolean hasManagerPermission(List<String> userRoles) {
        boolean isAdminOrManager = false;
        if (userRoles != null && userRoles.size() > 0) {
            for (String role : userRoles) {
                if (PublicConstants.USER_ROLE_ADMIN.equalsIgnoreCase(role) ||
                        PublicConstants.USER_ROLE_MANAGER.equalsIgnoreCase(role)) {
                    isAdminOrManager = true;
                    break;
                }
            }
        }
        return isAdminOrManager;
    }

    public static boolean hasAdminPermission(List<String> userRoles) {
        boolean isAdmin = false;
        if (userRoles != null && userRoles.size() > 0) {
            for (String role : userRoles) {
                if (PublicConstants.USER_ROLE_ADMIN.equalsIgnoreCase(role)) {
                    isAdmin = true;
                    break;
                }
            }
        }
        return isAdmin;
    }
}
