package com.worktool.logwork.util;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.worktool.logwork.PublicConstants;

/**
 * Created by THAIHOANG on 11/2/2017.
 */

public class DeviceUtils {

    public static String getSecureId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getImeiNumber(Context context) {
        String imei = null;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            imei = telephonyManager.getDeviceId();
            if (TextUtils.isEmpty(imei)) {
                // IMEI null will cause server error
                imei = "";
            }
        } else {
            imei = "";
        }
        Log.d(PublicConstants.LOG_TAG, "Your device IMEI number --> " + imei);
        return imei;
    }

    public static String getDeviceName(Context context) {
        String deviceName = null;
        deviceName = getUserDefinedDeviceName(context);
        if (TextUtils.isEmpty(deviceName)) {
            deviceName = getFactoryDeviceName();
        }
        return deviceName;
    }

    public static String getUserDefinedDeviceName(Context context) {
        String deviceName = null;
        deviceName = Settings.System.getString(context.getContentResolver(), "device_name");
        return deviceName;
    }

    public static String getFactoryDeviceName() {
        String deviceName = null;
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            deviceName = capitalize(model);
        } else {
            deviceName = capitalize(manufacturer) + "_" + capitalize(model);
        }
        return deviceName;
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static String getOsBuildVersion() {
        return Build.VERSION.RELEASE;
    }
}
