package com.worktool.logwork.util;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.inputmethod.InputMethodManager;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by THAIHOANG on 10/10/2017.
 */

public class LayoutUtils {

    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            try {
                InputMethodManager inputMethodManager = (InputMethodManager)
                        activity.getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                if (activity.getCurrentFocus() != null) {
                    inputMethodManager.hideSoftInputFromWindow(
                            activity.getCurrentFocus().getWindowToken(), 0);
                }
            } catch (Exception e) {

            }
        }
    }

}
