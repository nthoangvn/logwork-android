package com.worktool.logwork.session;

import android.util.Log;

import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.user.session.WorkSession;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THAIHOANG on 10/25/2017.
 */

public class WorkSessionUtils {

    public static List<WorkSession> buildWorkSessionOfMonth(String userId, DateTime firstDateOfMonth, DateTime lastDateOfMonth) {
        List<WorkSession> workSessions = new ArrayList<>();
        String firstDate = DateTimeUtils.dateTimeToString(firstDateOfMonth, PublicConstants.WORKING_SCHEDULER_DAY_FMT, DateTimeZone.UTC);
        String lastDate = DateTimeUtils.dateTimeToString(lastDateOfMonth, PublicConstants.WORKING_SCHEDULER_DAY_FMT, DateTimeZone.UTC);
        int daysInMonth = lastDateOfMonth.getDayOfMonth() - firstDateOfMonth.getDayOfMonth() + 1;
//        Log.d(PublicConstants.LOG_TAG, "Days in month: " + daysInMonth + ", first date: " + firstDate + ", last date: " + lastDate);
        for (int i=0; i<daysInMonth; i++) {
            WorkSession workSession = new WorkSession();
            DateTime currDate = firstDateOfMonth.plusDays(i);
            workSession.setEmpId(userId);
            String workSessTime = DateTimeUtils.dateTimeToString(currDate, PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            workSession.setCheckInTime(workSessTime);
            workSession.setCheckOutTime(workSessTime);
            workSessions.add(workSession);
        }
        return workSessions;
    }

    public static void updateWorkSessions(List<WorkSession> targetWorkSessions, List<WorkSession> sourceWorkSessions) {
        if (targetWorkSessions != null && targetWorkSessions.size() > 0 &&
                sourceWorkSessions != null && sourceWorkSessions.size() > 0) {
            for (WorkSession targetWorkSess : targetWorkSessions) {
                DateTime targetCheckIn = DateTimeUtils.stringToDateTime(targetWorkSess.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                for (WorkSession srcWorkSess : sourceWorkSessions) {
                    DateTime srcCheckIn = DateTimeUtils.stringToDateTime(srcWorkSess.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                    int daysDiff = targetCheckIn.getDayOfMonth() - srcCheckIn.getDayOfMonth();
//                    Log.d(PublicConstants.LOG_TAG, "Days diff: " + daysDiff + ", targetWorkSess: " + DateTimeUtils.dateTimeToString(targetCheckIn, PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC) +
//                            ", srcWorkSess: " + DateTimeUtils.dateTimeToString(srcCheckIn, PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC));
                    if (daysDiff == 0) {
                        // Update target workSession
                        targetWorkSess.copy(srcWorkSess);
//                        break;
                    }
                }
            }
        }
    }
}
