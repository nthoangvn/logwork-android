package com.worktool.logwork.session;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.user.session.WorkSession;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;

import static com.worktool.logwork.PublicConstants.WORKING_HOURS_PER_DAY_MAX;

/**
 * Created by THAIHOANG on 10/15/2017.
 */

public class WorkSessionAdapter extends RecyclerView.Adapter<WorkSessionViewHolder> {

    private Context mContext;
    private List<WorkSession> mWorkSessions;

    public WorkSessionAdapter(Context context) {
        mContext = context;
        mWorkSessions = new ArrayList<>();
    }

    public void setWorkSessions(List<WorkSession> workSessions) {
        mWorkSessions.clear();
        if (workSessions != null && workSessions.size() > 0) {
            mWorkSessions = new ArrayList<>(workSessions);
        }
        notifyDataSetChanged();
    }

    @Override
    public WorkSessionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.work_session_item, parent, false);
        return new WorkSessionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WorkSessionViewHolder holder, int position) {
        final WorkSession workSession = mWorkSessions.get(position);

        DateTime workSessionDate = DateTimeUtils.stringToDateTime(workSession.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
        String workSessionDateText = String.valueOf(workSessionDate.getDayOfMonth());
        holder.workSessDate.setText(workSessionDateText);

        DateTime currDate = DateTime.now();
        int daysDiff = currDate.getDayOfMonth() - workSessionDate.getDayOfMonth();
//        String workSessDateStr = DateTimeUtils.dateTimeToString(workSessionDate, PublicConstants.WORKING_SCHEDULER_DAY_FMT);
//        String currDateStr = DateTimeUtils.dateTimeToString(currDate, PublicConstants.WORKING_SCHEDULER_DAY_FMT);
//        Log.d(PublicConstants.LOG_TAG, "Work session list: " + "work sess date: " + workSessDateStr + ", daysOfMonth: " + workSessionDate.getDayOfMonth() +
//                ", curr date: " + currDateStr + ", dayOfMonth: " + currDate.getDayOfMonth() + ", days diff: " + daysDiff);
        if (daysDiff < 0) {
            holder.rootView.setBackgroundResource(R.color.transparent);
            holder.workSessUnregister.setVisibility(View.VISIBLE);
            holder.workSessUnregister.setText(mContext.getString(R.string.work_session_unregister).toUpperCase());
            holder.workSessShift.setVisibility(View.GONE);
            holder.workSessTotalTime.setVisibility(View.GONE);
        } else {
            if (daysDiff == 0) {
                holder.rootView.setBackgroundResource(R.color.light_blue);
            } else {
                holder.rootView.setBackgroundResource(R.color.transparent);
            }
            holder.workSessUnregister.setVisibility(View.GONE);

            long workSessTimeMinutes = 0;
            if (workSession.getCountedWorkTime() > 0) {
                workSessTimeMinutes = workSession.getCountedWorkTime() / PublicConstants.ONE_MINUTE_MS;
            } else {
                if (!TextUtils.isEmpty(workSession.getCheckInTime()) && !TextUtils.isEmpty(workSession.getCheckOutTime())) {
                    DateTime checkInDate = DateTimeUtils.stringToDateTime(workSession.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                    DateTime checkOutDate = DateTimeUtils.stringToDateTime(workSession.getCheckOutTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                    long msDiff = checkOutDate.getMillis() - checkInDate.getMillis();
                    workSessTimeMinutes = msDiff / PublicConstants.ONE_MINUTE_MS;
                }
            }

            int workHours = (int) (workSessTimeMinutes / 60);
            int workMins = (int) (workSessTimeMinutes % 60);

            String workSessionTimeStr;
            if (workHours >= WORKING_HOURS_PER_DAY_MAX) {
                workHours = WORKING_HOURS_PER_DAY_MAX;
                workSessionTimeStr = mContext.getString(R.string.work_session_time_hour, workHours);
            } else if (workHours > 0) {
                if (workMins > 0) {
                    workSessionTimeStr = mContext.getString(R.string.work_session_time_hour_minute, workHours, workMins);
                } else {
                    workSessionTimeStr = mContext.getString(R.string.work_session_time_hour, workHours);
                }
            } else {
                workSessionTimeStr = mContext.getString(R.string.work_session_time_minute, workMins);
            }


            holder.workSessShift.setVisibility(View.VISIBLE);
            if (workSessTimeMinutes > 0) {
                holder.workSessShift.setText(workSession.getSessionName());
                holder.workSessTotalTime.setVisibility(View.VISIBLE);
                holder.workSessTotalTime.setText(workSessionTimeStr);
            } else {
                holder.workSessShift.setText(mContext.getString(R.string.work_session_off).toUpperCase());
                holder.workSessTotalTime.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mWorkSessions.size();
    }
}
