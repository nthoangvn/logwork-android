package com.worktool.logwork.session;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.worktool.logwork.R;

/**
 * Created by THAIHOANG on 10/15/2017.
 */

public class WorkSessionViewHolder extends RecyclerView.ViewHolder {

    public View rootView;
    public TextView workSessDate;
    public TextView workSessShift;
    public TextView workSessTotalTime;
    public TextView workSessUnregister;

    public WorkSessionViewHolder(View itemView) {
        super(itemView);
        rootView = itemView.findViewById(R.id.layout_session_item);
        workSessDate = itemView.findViewById(R.id.text_sess_date);
        workSessShift = itemView.findViewById(R.id.text_session_shift);
        workSessTotalTime = itemView.findViewById(R.id.text_session_total_time);
        workSessUnregister = itemView.findViewById(R.id.text_session_unregister);
    }
}
