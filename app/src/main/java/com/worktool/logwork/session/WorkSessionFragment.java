package com.worktool.logwork.session;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.worktool.logwork.AppApplication;
import com.worktool.logwork.BaseFragment;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.admin.session.WorkSessionActivity;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.worktool.serverapi.logwork.user.session.WorkSession;
import com.worktool.serverapi.logwork.user.session.WorkSessionInfo;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.worktool.logwork.session.WorkSessionUtils.buildWorkSessionOfMonth;
import static com.worktool.logwork.util.DateTimeUtils.dateTimeToString;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkSessionFragment extends BaseFragment implements View.OnClickListener {

    public static final String EXTRA_USER_PROFILE = "WorkSessionFragment_extra_user_profile";

    private RecyclerView mMonthlyWorkSessionList;
    private WorkSessionAdapter mWorkSessionAdapter;
    private UserProfile mUserProfile;
    private SharedPreferences mAppSettings;
    private List<WorkSessionInfo> mWorkSessionInfos;

    public WorkSessionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserProfile = getArguments().getParcelable(EXTRA_USER_PROFILE);
        mAppSettings = getActivity().getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_work_session, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        TextView textWorkSessTitle = view.findViewById(R.id.text_work_session_title);
//        String workSessMonth = dateTimeToString(DateTime.now(), PublicConstants.WORK_SESSION_TITLE_DATE_FMT);
//        textWorkSessTitle.setText(getString(R.string.working_days_in_month, workSessMonth));

        mMonthlyWorkSessionList = view.findViewById(R.id.monthly_work_session_list);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        mMonthlyWorkSessionList.setLayoutManager(layoutManager);
        mWorkSessionAdapter = new WorkSessionAdapter(getActivity().getApplicationContext());
        mMonthlyWorkSessionList.setAdapter(mWorkSessionAdapter);

        loadWorkSessionInfo();
    }

    private void updateWorkSessionList(List<WorkSession> workSessionsRes) {
        List<WorkSession> filteredWorkSessions = new ArrayList<>();
        if (workSessionsRes != null && workSessionsRes.size() > 0) {
            for (WorkSession workSession : workSessionsRes) {
                // Filter check in/out date
                if (!TextUtils.isEmpty(workSession.getCheckInTime()) &&
                        !TextUtils.isEmpty(workSession.getCheckOutTime())) {
                    DateTime checkInDate = DateTimeUtils.stringToDateTime(workSession.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                    DateTime checkOutDate = DateTimeUtils.stringToDateTime(workSession.getCheckOutTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                    if (checkOutDate.isAfter(checkInDate)) {
                        // Filter by this month
                        String workSessMonth = DateTimeUtils.dateTimeToString(checkInDate, PublicConstants.MONTH_ID_FMT);
                        String thisMonth = DateTimeUtils.dateTimeToString(DateTime.now(), PublicConstants.MONTH_ID_FMT);
                        if (thisMonth.equalsIgnoreCase(workSessMonth)) {
                            // Filter work session name
                            for (WorkSessionInfo workSessionInfo : mWorkSessionInfos) {
                                if (workSessionInfo.getName().equalsIgnoreCase(workSession.getSessionName())) {
                                    filteredWorkSessions.add(workSession);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        Collections.sort(filteredWorkSessions, mWorkSessionComparator);

        List<WorkSession> workSessionsThisMonth = WorkSessionUtils.buildWorkSessionOfMonth(
                mUserProfile.getEmpId(), DateTimeUtils.getFirstDateOfTheMonth(), DateTimeUtils.getLastDateOfTheMonth());
        WorkSessionUtils.updateWorkSessions(workSessionsThisMonth, filteredWorkSessions);

        mWorkSessionAdapter.setWorkSessions(workSessionsThisMonth);
    }

    private final Comparator<WorkSession> mWorkSessionComparator = new Comparator<WorkSession>() {
        @Override
        public int compare(WorkSession o1, WorkSession o2) {
            int result;
            DateTime o1CheckInDate = DateTimeUtils.stringToDateTime(o1.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            DateTime o2CheckInDate = DateTimeUtils.stringToDateTime(o2.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            if (o1CheckInDate.isBefore(o2CheckInDate)) {
                result = -1;
            } else if (o1CheckInDate.isAfter(o2CheckInDate)) {
                result = 1;
            } else {
                result = 0;
            }
            return result;
        }
    };

    private void loadWorkSession() {
        Log.d(PublicConstants.LOG_TAG, "Load work session");
        if (mUserProfile != null) {
            String userId = mUserProfile.getEmpId();
            String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
            if (!TextUtils.isEmpty(userToken)) {
                showLoading(AppApplication.appContext.getString(R.string.loading_work_session_monthly));
                Call<List<WorkSession>> getWorkSessCall = LogWorkApi.getInstance().getService()
                        .getWorkSession(userId, userToken);
                getWorkSessCall.enqueue(new Callback<List<WorkSession>>() {
                    @Override
                    public void onResponse(Call<List<WorkSession>> call, Response<List<WorkSession>> response) {
                        Log.d(PublicConstants.LOG_TAG, "Load work session done, success? " + response.isSuccessful());
                        hideLoading();
                        if (response.isSuccessful()) {
                            List<WorkSession> workSessions = response.body();
                            updateWorkSessionList(workSessions);
                        } else {
                            showToast(AppApplication.appContext.getString(R.string.error_invalid_work_sessions), Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<WorkSession>> call, Throwable t) {
                        hideLoading();
                        showToast(AppApplication.appContext.getString(R.string.get_work_session_fail), Toast.LENGTH_SHORT);
                    }
                });
            }
        }
    }

    private void loadWorkSessionInfo() {
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        showLoading(getString(R.string.loading_work_session_info));
        Call<List<WorkSessionInfo>> listWorkSessCall = LogWorkApi.getInstance().getService()
                .getWorkSessionInfos(userToken);
        listWorkSessCall.enqueue(new Callback<List<WorkSessionInfo>>() {
            @Override
            public void onResponse(Call<List<WorkSessionInfo>> call, Response<List<WorkSessionInfo>> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    mWorkSessionInfos = response.body();
                    loadWorkSession();
                } else {
                    showToast(AppApplication.appContext.getString(R.string.load_work_session_info_fail), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<List<WorkSessionInfo>> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                showToast(AppApplication.appContext.getString(R.string.load_work_session_info_fail), Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }
}
