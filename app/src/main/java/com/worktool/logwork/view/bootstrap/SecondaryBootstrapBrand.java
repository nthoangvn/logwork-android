package com.worktool.logwork.view.bootstrap;

import com.worktool.logwork.R;

/**
 * Created by THAIHOANG on 11/13/2017.
 */

public class SecondaryBootstrapBrand extends CustomBootstrapBrand {
    public SecondaryBootstrapBrand() {
        super(R.color.colorPrimaryLight);
    }
}
