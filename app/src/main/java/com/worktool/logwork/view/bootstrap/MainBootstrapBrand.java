package com.worktool.logwork.view.bootstrap;

import com.worktool.logwork.R;

/**
 * Created by THAIHOANG on 11/13/2017.
 */

public class MainBootstrapBrand extends CustomBootstrapBrand {

    public MainBootstrapBrand() {
        super(R.color.main_text_color);
    }
}
