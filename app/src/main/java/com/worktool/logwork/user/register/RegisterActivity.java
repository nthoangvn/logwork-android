package com.worktool.logwork.user.register;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.worktool.serverapi.logwork.user.register.RegisterRequest;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import retrofit2.Call;

import static android.Manifest.permission.READ_CONTACTS;
import static android.media.CamcorderProfile.get;

/**
 * A login screen that offers login via email/password.
 */
public class RegisterActivity extends AppCompatActivity implements LoaderCallbacks<Cursor>, DatePickerDialog.OnDateSetListener, View.OnClickListener {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the register task task to ensure we can cancel it if requested.
     */
    private UserRegisterTask mAuthTask = null;
    private SharedPreferences mAppSettings;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mConfirmPasswordView;
    private EditText mBirthdayView;
    private EditText mUserNameView;
    private EditText mFullNameView;
    private EditText mPhoneNumberView;
    private EditText mAddressView;
    private EditText mEmployeeIdView;
    private EditText mEmployeeTypeView;
    private EditText mEmployeeRoleView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mAppSettings = getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.register || id == EditorInfo.IME_NULL) {
                    attemptRegister();
                    return true;
                }
                return false;
            }
        });

        mConfirmPasswordView = findViewById(R.id.confirm_password);

        mLoginFormView = findViewById(R.id.scroll_register_form);
        mProgressView = findViewById(R.id.register_progress);

        mUserNameView = findViewById(R.id.text_user_name);
        mFullNameView = findViewById(R.id.text_full_name);
        mPhoneNumberView = findViewById(R.id.text_phone_number);
        mAddressView = findViewById(R.id.text_address);
        mEmployeeIdView = findViewById(R.id.text_employee_id);
        mEmployeeTypeView = findViewById(R.id.text_employee_type);
        mEmployeeRoleView = findViewById(R.id.text_employee_role);

        mBirthdayView = findViewById(R.id.text_birthday);
        mBirthdayView.setOnClickListener(this);

        BootstrapButton registerButton = findViewById(R.id.register_button);
        registerButton.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDatePickerDialog() {
//        final Calendar calendar = Calendar.getInstance();
//        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
//                this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
//                calendar.get(Calendar.DAY_OF_MONTH), true);
//        datePickerDialog.setOnDateSetListener(this);
//        datePickerDialog.setYearRange(1950, 2028);
//        datePickerDialog.setVibrate(true);
//        datePickerDialog.setCloseOnSingleTapDay(false);
//        datePickerDialog.show(getSupportFragmentManager(), "DatePickerDialog");
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DATE));
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Log.d(PublicConstants.LOG_TAG, "On date set, year " + year + ", month " + month + ", day " + dayOfMonth);
        DateTime newDate = DateTime.now().withYear(year).withMonthOfYear(month + 1).withDayOfMonth(dayOfMonth)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);
        String dateStr = DateTimeUtils.dateTimeToString(newDate, PublicConstants.BIRTHDAY_DATE_FMT);
        Log.d(PublicConstants.LOG_TAG, "On birthday selected: " + dateStr);
//        Toast.makeText(this, "New date:" + dateStr, Toast.LENGTH_LONG).show();
        mBirthdayView.setText(dateStr);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptRegister() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mConfirmPasswordView.setError(null);
        mUserNameView.setError(null);
        mEmployeeIdView.setError(null);
        mFullNameView.setError(null);
        mBirthdayView.setError(null);
        mAddressView.setError(null);
        mPhoneNumberView.setError(null);
        mEmployeeTypeView.setError(null);
        mEmployeeRoleView.setError(null);

        // Store values at the time of the login attempt.
        String userName = mUserNameView.getText().toString();
        String fullName = mFullNameView.getText().toString();
        String birthDayStr = mBirthdayView.getText().toString();
        DateTime empBirthDay = null;
        if (!TextUtils.isEmpty(birthDayStr)) {
            empBirthDay = DateTimeUtils.stringToDateTime(birthDayStr, PublicConstants.BIRTHDAY_DATE_FMT, DateTimeZone.UTC);
        }

        String empId = mEmployeeIdView.getText().toString();
        String empType = mEmployeeTypeView.getText().toString();
        String empRole = mEmployeeRoleView.getText().toString();
        String phoneNumber = mPhoneNumberView.getText().toString();
        String address = mAddressView.getText().toString();
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String confirmPassword = mConfirmPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid address.
        if (TextUtils.isEmpty(birthDayStr)) {
            mBirthdayView.setError(getString(R.string.error_field_required));
            focusView = mBirthdayView;
            cancel = true;
        }

        // Check for a valid address.
        if (TextUtils.isEmpty(address)) {
            mAddressView.setError(getString(R.string.error_field_required));
            focusView = mAddressView;
            cancel = true;
        }

        // Check for a valid phone number.
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberView.setError(getString(R.string.error_field_required));
            focusView = mPhoneNumberView;
            cancel = true;
        }

        // Check for a valid employee role.
//        if (TextUtils.isEmpty(empRole)) {
//            mEmployeeRoleView.setError(getString(R.string.error_field_required));
//            focusView = mEmployeeRoleView;
//            cancel = true;
//        }

        // Check for a valid employee type.
//        if (TextUtils.isEmpty(empType)) {
//            mEmployeeTypeView.setError(getString(R.string.error_field_required));
//            focusView = mEmployeeTypeView;
//            cancel = true;
//        }

        // Check for a valid employee id.
//        if (TextUtils.isEmpty(empId)) {
//            mEmployeeIdView.setError(getString(R.string.error_field_required));
//            focusView = mEmployeeIdView;
//            cancel = true;
//        }

        // Check for a valid full name.
        if (TextUtils.isEmpty(fullName)) {
            mFullNameView.setError(getString(R.string.error_field_required));
            focusView = mFullNameView;
            cancel = true;
        }

        // Check for a valid confirm password, if the user entered one.
        if (!TextUtils.isEmpty(confirmPassword) && !confirmPassword.equals(password)) {
            mConfirmPasswordView.setError(getString(R.string.error_invalid_confirm_password));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        // Check for a valid user name.
        if (TextUtils.isEmpty(userName)) {
            mUserNameView.setError(getString(R.string.error_field_required));
            focusView = mUserNameView;
            cancel = true;
        } else if (!isUserNameValid(userName)) {
            mUserNameView.setError(getString(R.string.error_invalid_user_name));
            focusView = mUserNameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserRegisterTask();
            mAuthTask.setUserName(userName);
            mAuthTask.setEmail(email);
            mAuthTask.setPassword(password);
            mAuthTask.setFullName(fullName);
            mAuthTask.setBirthday(empBirthDay);
            mAuthTask.setPhoneNumber(phoneNumber);
            mAuthTask.setAddress(address);
            mAuthTask.setEmployeeId(empId);
            mAuthTask.setEmployeeType(empType);
            mAuthTask.setEmployeeRole(empRole);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isUserNameValid(String userName) {
        //TODO: Replace this with your own logic
        return true;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= PublicConstants.PASSWORD_LENGTH_MIN;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(RegisterActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_button:
                attemptRegister();
                break;
            case R.id.text_birthday:
                Log.d(PublicConstants.LOG_TAG, "On birthday text clicked");
                showDatePickerDialog();
                break;
        }
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    private void onUserRegisterSuccess() {
        Toast.makeText(this, R.string.register_user_success, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    private void onUserRegisterFail() {
        Toast.makeText(this, R.string.register_user_failed, Toast.LENGTH_SHORT).show();
        mUserNameView.requestFocus();
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {

        private String mEmail;
        private String mPassword;
        private String mUserName;
        private String mFullName;
        private String mPhoneNumber;
        private String mAddress;
        private DateTime mBirthday;
        private String mEmployeeId;
        private String mEmployeeType;
        private String mEmployeeRole;

        UserRegisterTask() {
        }

        public void setEmail(String mEmail) {
            this.mEmail = mEmail;
        }

        public void setPassword(String mPassword) {
            this.mPassword = mPassword;
        }

        public void setUserName(String mUserName) {
            this.mUserName = mUserName;
        }

        public void setFullName(String mFullName) {
            this.mFullName = mFullName;
        }

        public void setPhoneNumber(String mPhoneNumber) {
            this.mPhoneNumber = mPhoneNumber;
        }

        public void setAddress(String mAddress) {
            this.mAddress = mAddress;
        }

        public void setBirthday(DateTime mBirthday) {
            this.mBirthday = mBirthday;
        }

        public void setEmployeeId(String employeeId) {
            mEmployeeId = employeeId;
        }

        public void setEmployeeType(String mEmployeeType) {
            this.mEmployeeType = mEmployeeType;
        }

        public void setEmployeeRole(String mEmployeeRole) {
            this.mEmployeeRole = mEmployeeRole;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            boolean success = false;

            RegisterRequest registerReq = new RegisterRequest();
            registerReq.setUsername(mUserName);
            registerReq.setPassword(mPassword);
            registerReq.setFullName(mFullName);
            registerReq.setPhoneNumber(mPhoneNumber);
            registerReq.setEmail(mEmail);
            registerReq.setAddress(mAddress);
//            registerReq.setUserId(mEmployeeId);
//            registerReq.setEmpType(mEmployeeType);
//            registerReq.setEmpRole(mEmployeeRole);
            registerReq.setBirthDay(DateTimeUtils.dateTimeToString(mBirthday, PublicConstants.USER_PROFILE_DATE_FMT));

            Call<UserProfile> registerCall = LogWorkApi.getInstance().getService().userRegister(registerReq);
            try {
                UserProfile userProfile = registerCall.execute().body();
                if (userProfile != null) {
                    Log.d(PublicConstants.LOG_TAG, "Register done, user token: " + userProfile);
                    if (!TextUtils.isEmpty(userProfile.getEmail())) {
                        SharedPreferences.Editor editor = mAppSettings.edit();
                        editor.putString(PublicConstants.PREFS_USER_EMAIL, mEmail);
                        editor.apply();

                        success = true;
                    }
                } else {
                    Log.d(PublicConstants.LOG_TAG, "Register fail, response null");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return success;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                onUserRegisterSuccess();
            } else {
                onUserRegisterFail();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

