package com.worktool.logwork.user.activity;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.worktool.logwork.AppApplication;
import com.worktool.logwork.BaseFragment;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.user.activity.RecentActivity;
import com.worktool.serverapi.logwork.user.session.WorkSession;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecentActivityFragment extends BaseFragment {

    private RecyclerView mRecentActivityList;
    private RecentActivityAdapter mRecentActivityAdapter;
    private SharedPreferences mAppSettings;

    public RecentActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppSettings = getActivity().getApplicationContext().getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recent_activity, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecentActivityList = view.findViewById(R.id.recent_activity_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecentActivityList.setLayoutManager(layoutManager);
        mRecentActivityList.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getActivity())
                        .colorResId(R.color.main_text_color)
                        .size(3)
                        .build());

        mRecentActivityAdapter = new RecentActivityAdapter(getActivity().getApplicationContext());
        mRecentActivityList.setAdapter(mRecentActivityAdapter);

        loadRecentActivities();
    }

    private void loadRecentActivities() {
        showLoading(getString(R.string.loading_recent_activity_waiting));
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        Call<List<RecentActivity>> userProfilesCall = LogWorkApi.getInstance().getService()
                .getRecentActivities(userToken);
        userProfilesCall.enqueue(new Callback<List<RecentActivity>>() {
            @Override
            public void onResponse(Call<List<RecentActivity>> call, Response<List<RecentActivity>> response) {
                hideLoading();
                Log.d(PublicConstants.LOG_TAG, "Load recent activities completed, success? " + response.isSuccessful());
                if (response.isSuccessful()) {
                    showRecentActivities(response.body());
                } else {
                    showToast(AppApplication.appContext.getString(R.string.load_recent_activity_fail), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<List<RecentActivity>> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                showToast(AppApplication.appContext.getString(R.string.load_recent_activity_fail), Toast.LENGTH_SHORT);
            }
        });
    }

    private void showRecentActivities(List<RecentActivity> recentActivities) {
        if (mRecentActivityAdapter != null) {
            Collections.sort(recentActivities, mRecentActivityComparator);
            mRecentActivityAdapter.setRecentActivities(recentActivities);
        }
    }

    private final Comparator<RecentActivity> mRecentActivityComparator = new Comparator<RecentActivity>() {
        @Override
        public int compare(RecentActivity o1, RecentActivity o2) {
            int result;
            DateTime o1Date;
            if (PublicConstants.ACTION_CHECKOUT.equalsIgnoreCase(o1.getAction())) {
                o1Date = DateTimeUtils.stringToDateTime(o1.getCheckOutTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            } else {
                o1Date = DateTimeUtils.stringToDateTime(o1.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            }
            DateTime o2Date;
            if (PublicConstants.ACTION_CHECKOUT.equalsIgnoreCase(o2.getAction())) {
                o2Date = DateTimeUtils.stringToDateTime(o2.getCheckOutTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            } else {
                o2Date = DateTimeUtils.stringToDateTime(o2.getCheckInTime(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            }
            if (o1Date.isBefore(o2Date)) {
                result = 1;
            } else if (o1Date.isAfter(o2Date)) {
                result = -1;
            } else {
                result = 0;
            }
            return result;
        }
    };
}
