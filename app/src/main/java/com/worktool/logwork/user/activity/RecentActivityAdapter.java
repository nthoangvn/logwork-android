package com.worktool.logwork.user.activity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.serverapi.logwork.user.activity.RecentActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THAIHOANG on 11/6/2017.
 */

public class RecentActivityAdapter extends RecyclerView.Adapter<RecentActivityViewHolder> {

    private List<RecentActivity> mRecentActivities;
    private Context mContext;

    public RecentActivityAdapter(Context context) {
        mContext = context;
        mRecentActivities = new ArrayList<>();
    }

    public void setRecentActivities(List<RecentActivity> recentActivities) {
        mRecentActivities.clear();
        if (recentActivities != null && recentActivities.size() > 0) {
            mRecentActivities = new ArrayList<>(recentActivities);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecentActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.recent_activity_item, parent, false);
        return new RecentActivityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecentActivityViewHolder holder, int position) {
        final RecentActivity recentActivity = mRecentActivities.get(position);

        if (PublicConstants.ACTION_CHECKIN.equalsIgnoreCase(recentActivity.getAction())) {
            holder.imgAction.setImageResource(R.drawable.ic_login);
        } else {
            holder.imgAction.setImageResource(R.drawable.ic_logout);
        }

        holder.textActivity.setText(recentActivity.getText());
    }

    @Override
    public int getItemCount() {
        return mRecentActivities.size();
    }
}
