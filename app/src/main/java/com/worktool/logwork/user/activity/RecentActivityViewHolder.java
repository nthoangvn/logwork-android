package com.worktool.logwork.user.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.worktool.logwork.R;

/**
 * Created by THAIHOANG on 11/6/2017.
 */

public class RecentActivityViewHolder extends RecyclerView.ViewHolder {

    public ImageView imgAction;
    public TextView textActivity;

    public RecentActivityViewHolder(View itemView) {
        super(itemView);
        imgAction = itemView.findViewById(R.id.img_action);
        textActivity = itemView.findViewById(R.id.text_activity);
    }
}
