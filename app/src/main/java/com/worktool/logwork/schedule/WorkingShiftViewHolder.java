package com.worktool.logwork.schedule;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.worktool.logwork.R;

import org.zakariya.stickyheaders.SectioningAdapter;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

public class WorkingShiftViewHolder extends SectioningAdapter.ItemViewHolder {

    public View rootView;
    public TextView textShiftName;
    public TextView textShiftTime;
    public CheckBox cbShiftSelected;

    public WorkingShiftViewHolder(View itemView) {
        super(itemView);
        rootView = itemView;
        textShiftName = itemView.findViewById(R.id.text_shift_name);
        textShiftTime = itemView.findViewById(R.id.text_shift_time);
        cbShiftSelected = itemView.findViewById(R.id.cb_shift_selected);
    }

}
