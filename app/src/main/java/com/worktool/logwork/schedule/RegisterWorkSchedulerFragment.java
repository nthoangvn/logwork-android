package com.worktool.logwork.schedule;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.worktool.logwork.AppApplication;
import com.worktool.logwork.BaseFragment;
import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.logwork.view.bootstrap.MainBootstrapBrand;
import com.worktool.logwork.view.bootstrap.SecondaryBootstrapBrand;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.worktool.serverapi.logwork.user.scheduler.SchedulerData;
import com.worktool.serverapi.logwork.user.scheduler.SchedulerResponse;
import com.worktool.serverapi.logwork.user.session.WorkSessionInfo;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.worktool.logwork.util.DateTimeUtils.dateTimeToString;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterWorkSchedulerFragment extends BaseFragment implements View.OnClickListener {

    public static final String EXTRA_USER_PROFILE = "RegisterWorkSchedulerFragment_extra_user_profile";

    private RecyclerView mWorkingTimeList;
    private WorkingScheduleAdapter mWorkingDateAdapter;
    private BootstrapButton mThisWeekBtn, mNextWeekBtn;
    private View mLayoutNoWeekSchdlr;
    private TextView mTextNoWeekSchdlr;
    private String mCurrWeekId, mNextWeekId;
    private String mSelectedWeekId;
    private boolean mCurrWeekSchdlrClosed = false;
    private UserProfile mUserProfile;
    private SharedPreferences mAppSettings;
    private List<WorkSessionInfo> mWorkSessionInfos;
    private View mLayoutSubmit;

    public RegisterWorkSchedulerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserProfile = getArguments().getParcelable(EXTRA_USER_PROFILE);
        mAppSettings = getActivity().getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register_work_scheduler, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLayoutSubmit = view.findViewById(R.id.layout_submit_scheduler);
        View submitBtn = view.findViewById(R.id.btn_submit);
        submitBtn.setOnClickListener(this);
        View cancelBtn = view.findViewById(R.id.btn_cancel);
        cancelBtn.setOnClickListener(this);
        mThisWeekBtn = view.findViewById(R.id.btn_this_week);
        mThisWeekBtn.setBootstrapBrand(new MainBootstrapBrand());
        mThisWeekBtn.setOnClickListener(this);
        mNextWeekBtn = view.findViewById(R.id.btn_next_week);
        mNextWeekBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
        mNextWeekBtn.setOnClickListener(this);

        mCurrWeekId = dateTimeToString(DateTime.now(), PublicConstants.WEEK_ID_FMT);
        DateTime startDayOfThisWeek = DateTimeUtils.getStartDayOfThisWeek();
        DateTime startDayOfNextWeek = startDayOfThisWeek.plusDays(7);
        mNextWeekId = dateTimeToString(startDayOfNextWeek, PublicConstants.WEEK_ID_FMT);
        mSelectedWeekId = mCurrWeekId;

        mWorkingDateAdapter = new WorkingScheduleAdapter(getActivity().getApplicationContext());
        mWorkingTimeList = view.findViewById(R.id.working_date_list);

        StickyHeaderLayoutManager layoutManager = new StickyHeaderLayoutManager();
        mWorkingTimeList.setLayoutManager(layoutManager);
        mWorkingTimeList.setAdapter(mWorkingDateAdapter);

        mLayoutNoWeekSchdlr = view.findViewById(R.id.layout_no_work_scheduler);
        mLayoutNoWeekSchdlr.setVisibility(View.GONE);
        mTextNoWeekSchdlr = view.findViewById(R.id.text_no_work_scheduler);

        setupWeekSchedulerButtons();
        loadWorkSessionInfo();
//        loadWorkScheduler(mSelectedWeekId);
    }

    private void setupWeekSchedulerButtons() {
        final DateTime startDateOfThisWeek = DateTimeUtils.getStartDayOfThisWeek();
        final DateTime endDateOfThisWeek = startDateOfThisWeek.plusDays(6);
        String thisWeekTime = dateTimeToString(startDateOfThisWeek, PublicConstants.WEEK_SCHEDULER_BUTTON_DAY_FMT) + " - " +
                dateTimeToString(endDateOfThisWeek, PublicConstants.WEEK_SCHEDULER_BUTTON_DAY_FMT);
        if (mThisWeekBtn != null) {
            mThisWeekBtn.setText(getString(R.string.this_week_scheduler, thisWeekTime));
        }

        final DateTime startDateOfNextWeek = startDateOfThisWeek.plusDays(7);
        final DateTime endDateOfNextWeek = startDateOfNextWeek.plusDays(6);
        String nextWeekTime = dateTimeToString(startDateOfNextWeek, PublicConstants.WEEK_SCHEDULER_BUTTON_DAY_FMT) + " - " +
                dateTimeToString(endDateOfNextWeek, PublicConstants.WEEK_SCHEDULER_BUTTON_DAY_FMT);
        if (mNextWeekBtn != null) {
            mNextWeekBtn.setText(getString(R.string.next_week_scheduler, nextWeekTime));
        }
    }

    private void loadWorkSessionInfo() {
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        showLoading(getString(R.string.loading_work_session_info));
        Call<List<WorkSessionInfo>> listWorkSessCall = LogWorkApi.getInstance().getService()
                .getWorkSessionInfos(userToken);
        listWorkSessCall.enqueue(new Callback<List<WorkSessionInfo>>() {
            @Override
            public void onResponse(Call<List<WorkSessionInfo>> call, Response<List<WorkSessionInfo>> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    mWorkSessionInfos = response.body();
                    loadWorkScheduler(mSelectedWeekId);
                } else {
                    showToast(AppApplication.appContext.getString(R.string.load_work_session_info_fail), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<List<WorkSessionInfo>> call, Throwable t) {
                t.printStackTrace();
                hideLoading();
                showToast(AppApplication.appContext.getString(R.string.load_work_session_info_fail), Toast.LENGTH_SHORT);
            }
        });
    }

    private void setupWorkingDateAdapter(SchedulerResponse schedulerResponse, DateTime startDayOfWeek) {
        List<WorkingShift> workingShifts = null;
        if (schedulerResponse != null && schedulerResponse.getWorkScheduler() != null) {
            String currentWeekId = DateTimeUtils.dateTimeToString(DateTime.now(), PublicConstants.WEEK_ID_FMT);
            if (!TextUtils.isEmpty(mSelectedWeekId) && mSelectedWeekId.equals(currentWeekId)) {
                mCurrWeekSchdlrClosed = schedulerResponse.isReadOnly();
            }
            // Invalidate submit layout
            if (mLayoutSubmit != null) {
                if (schedulerResponse.isReadOnly()) {
                    mLayoutSubmit.setVisibility(View.GONE);
                } else {
                    mLayoutSubmit.setVisibility(View.VISIBLE);
                }
            }

            if (schedulerResponse.isReadOnly()) {
                mWorkingDateAdapter.setReadOnly(schedulerResponse.isReadOnly());
            }
            workingShifts = WorkingScheduleFactory.createDefaultWorkingShifts(startDayOfWeek, mWorkSessionInfos);
            List<WorkingShift> selectedWorkingShifts = WorkingScheduleFactory.buildWorkingShifts(schedulerResponse.getWorkScheduler(), startDayOfWeek, mWorkSessionInfos);
            WorkingScheduleFactory.updateWorkingShifts(workingShifts, selectedWorkingShifts);
        } else {
            workingShifts = WorkingScheduleFactory.createDefaultWorkingShifts(startDayOfWeek, mWorkSessionInfos);
        }
//        Collections.sort(workingShifts, mWorkingShiftComparator);
        mWorkingDateAdapter.setWorkingShifts(workingShifts);
    }

    private final Comparator<WorkingShift> mWorkingShiftComparator = new Comparator<WorkingShift>() {
        @Override
        public int compare(WorkingShift o1, WorkingShift o2) {
            int result;
            DateTime o1Date = DateTimeUtils.stringToDateTime(o1.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            DateTime o2Date = DateTimeUtils.stringToDateTime(o2.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            if (o1Date.isBefore(o2Date)) {
                result = -1;
            } else if (o1Date.isAfter(o2Date)) {
                result = 1;
            } else {
                result = 0;
            }
            return result;
        }
    };

    private void registerWorkingScheduler(List<WorkingShift> selectedWorkingShifts) {
        Log.d(PublicConstants.LOG_TAG, "Register work scheduler");
        if (mUserProfile != null) {
            showLoading(getString(R.string.register_work_scheduler_waiting));
            String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
            String userId = mUserProfile.getEmpId();

            SchedulerData schedulerData = WorkingScheduleFactory.buildSchedulerData(selectedWorkingShifts);
            schedulerData.setWeekId(mSelectedWeekId);
            Call<SchedulerResponse> schedulerResponseCall = LogWorkApi.getInstance().getService()
                    .registerWorkScheduler(userId, userToken, schedulerData);
            schedulerResponseCall.enqueue(new Callback<SchedulerResponse>() {
                @Override
                public void onResponse(Call<SchedulerResponse> call, Response<SchedulerResponse> response) {
                    Log.d(PublicConstants.LOG_TAG, "Register work scheduler completed, success? " + response.isSuccessful());
                    // Dismiss loading dialog
                    hideLoading();
                    if (response.isSuccessful()) {
                        Toast.makeText(getActivity(), R.string.register_work_scheduler_success, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.register_work_scheduler_fail, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<SchedulerResponse> call, Throwable t) {
                    // Dismiss loading dialog
                    t.printStackTrace();
                    Log.d(PublicConstants.LOG_TAG, "Register work scheduler fail");
                    hideLoading();
                    Toast.makeText(getActivity(), R.string.register_work_scheduler_fail, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void loadWorkScheduler(final String weekId) {
        if (mUserProfile != null) {
            showLoading(getString(R.string.loading_work_scheduler));
            Log.d(PublicConstants.LOG_TAG, "Loading work scheduler, weekId: " + weekId);
            String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
            String userId = mUserProfile.getEmpId();
            final DateTime startDayOfWeek = DateTimeUtils.stringToDateTime(weekId, PublicConstants.WEEK_ID_FMT);
            Call<SchedulerResponse> getWorkSchedulerCall = LogWorkApi.getInstance().getService()
                    .getWorkScheduler(userId, userToken, weekId);
            getWorkSchedulerCall.enqueue(new Callback<SchedulerResponse>() {
                @Override
                public void onResponse(Call<SchedulerResponse> call, Response<SchedulerResponse> response) {
                    Log.d(PublicConstants.LOG_TAG, "Load work scheduler completed, success? " + response.isSuccessful());
                    // Dismiss loading layout
                    hideLoading();
                    if (response.isSuccessful()) {
                        SchedulerResponse schedulerResponse = response.body();
                        Log.d(PublicConstants.LOG_TAG, "Start day of week? " + dateTimeToString(startDayOfWeek, PublicConstants.USER_PROFILE_DATE_FMT));
                        setupWorkingDateAdapter(schedulerResponse, startDayOfWeek);
                    } else {
                        showToast(AppApplication.appContext.getString(R.string.get_work_scheduler_fail), Toast.LENGTH_LONG);
                        showNoWeekSchdlrLayout();
                    }
                }

                @Override
                public void onFailure(Call<SchedulerResponse> call, Throwable t) {
                    // Dismiss loading layout or retry
                    hideLoading();
                    t.printStackTrace();
                    showToast(AppApplication.appContext.getString(R.string.get_work_scheduler_fail), Toast.LENGTH_LONG);
                    showNoWeekSchdlrLayout();
                }
            });
        }
    }

    private void showNoWeekSchdlrLayout() {
        mWorkingTimeList.setVisibility(View.GONE);
        mLayoutNoWeekSchdlr.setVisibility(View.VISIBLE);
        if (mCurrWeekId.equalsIgnoreCase(mSelectedWeekId)) {
            mTextNoWeekSchdlr.setText(R.string.no_work_scheduler_for_this_week);
        } else {
            mTextNoWeekSchdlr.setText(R.string.no_work_scheduler_for_next_week);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (mWorkingDateAdapter != null) {
                    mWorkingDateAdapter.clearCheckedWorkingShifts();
                }
                break;
            case R.id.btn_submit:
                if (mWorkingDateAdapter != null) {
                    List<WorkingShift> selectedWorkingShifts = mWorkingDateAdapter.getCheckedWorkingShifts();
                    registerWorkingScheduler(selectedWorkingShifts);
                }
                break;
            case R.id.btn_this_week:
                if (!mCurrWeekId.equalsIgnoreCase(mSelectedWeekId)) {
                    mSelectedWeekId = mCurrWeekId;
                    mLayoutNoWeekSchdlr.setVisibility(View.GONE);
                    mWorkingTimeList.setVisibility(View.VISIBLE);
                    mThisWeekBtn.setBootstrapBrand(new MainBootstrapBrand());
                    mNextWeekBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
                    Log.d(PublicConstants.LOG_TAG, "Load this week work scheduler");
                    if (mWorkSessionInfos != null) {
                        loadWorkScheduler(mSelectedWeekId);
                    } else {
                        loadWorkSessionInfo();
                    }
                }
                break;
            case R.id.btn_next_week:
                if (!mNextWeekId.equalsIgnoreCase(mSelectedWeekId)) {
                    mSelectedWeekId = mNextWeekId;
                    mThisWeekBtn.setBootstrapBrand(new SecondaryBootstrapBrand());
                    mNextWeekBtn.setBootstrapBrand(new MainBootstrapBrand());
                    if (mCurrWeekSchdlrClosed) {
                        mLayoutNoWeekSchdlr.setVisibility(View.GONE);
                        mWorkingTimeList.setVisibility(View.VISIBLE);
                        Log.d(PublicConstants.LOG_TAG, "Load next week work scheduler");
                        if (mWorkSessionInfos != null) {
                            loadWorkScheduler(mSelectedWeekId);
                        } else {
                            loadWorkSessionInfo();
                        }
                    } else {
                        mWorkingTimeList.setVisibility(View.GONE);
                        mLayoutNoWeekSchdlr.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }
    }
}
