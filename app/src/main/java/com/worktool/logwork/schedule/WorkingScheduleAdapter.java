package com.worktool.logwork.schedule;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.R;
import com.worktool.logwork.util.DateTimeUtils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

public class WorkingScheduleAdapter extends SectioningAdapter {

    private Context mContext;
    private List<WorkingShift> mWorkingShifts;
    private List<WorkingShift> mCheckedWorkingShifts;
    private List<WorkingDate> mWorkingDates;
    private boolean mReadOnly;

    public WorkingScheduleAdapter(Context context) {
        mContext = context;
        mWorkingShifts = new ArrayList<>();
        mCheckedWorkingShifts = new ArrayList<>();
        mWorkingDates = new ArrayList<>();
    }

    public void setReadOnly(boolean readOnly) {
        mReadOnly = readOnly;
    }

    public void setWorkingShifts(List<WorkingShift> workingShifts) {
        mWorkingShifts.clear();
        mCheckedWorkingShifts.clear();
        if (workingShifts != null && workingShifts.size() > 0) {
            this.mWorkingShifts = new ArrayList<>(workingShifts);
            for (WorkingShift workingShift : mWorkingShifts) {
                if (workingShift.isSelected()) {
                    mCheckedWorkingShifts.add(workingShift);
                }
            }
        }

        mWorkingDates.clear();
        mWorkingDates = WorkingScheduleFactory.groupWorkingShift(mWorkingShifts);
        notifyAllSectionsDataSetChanged();
    }

    public void clearCheckedWorkingShifts() {
        if (!mReadOnly) {
            mCheckedWorkingShifts.clear();
            for (WorkingShift workingShift : mWorkingShifts) {
                workingShift.setSelected(false);
            }
            mWorkingDates.clear();
            mWorkingDates = WorkingScheduleFactory.groupWorkingShift(mWorkingShifts);
            notifyAllSectionsDataSetChanged();
        }
    }

    public List<WorkingShift> getCheckedWorkingShifts() {
        // Should clone checked list
        return new ArrayList<>(mCheckedWorkingShifts);
    }

    @Override
    public int getNumberOfSections() {
        return mWorkingDates.size();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        return mWorkingDates.get(sectionIndex).getShifts().size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public boolean doesSectionHaveFooter(int sectionIndex) {
        return false;
    }

    @Override
    public WorkingShiftViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.working_shift_item, parent, false);
        return new WorkingShiftViewHolder(v);
    }

    @Override
    public WorkingDateViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.working_date_item, parent, false);
        return new WorkingDateViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, final int sectionIndex, final int itemIndex, int itemType) {
        WorkingDate workingDate = mWorkingDates.get(sectionIndex);
        WorkingShiftViewHolder itemViewHolder = (WorkingShiftViewHolder) viewHolder;
        final WorkingShift workingShift = workingDate.getShifts().get(itemIndex);
        itemViewHolder.textShiftName.setText(workingShift.getShiftName());
        itemViewHolder.textShiftTime.setText("(" + workingShift.getShiftStartTime() + " - " + workingShift.getShiftEndTime() + ")");

        itemViewHolder.cbShiftSelected.setEnabled(!mReadOnly);
        itemViewHolder.cbShiftSelected.setOnCheckedChangeListener(null);
        itemViewHolder.cbShiftSelected.setChecked(workingShift.isSelected());
        itemViewHolder.cbShiftSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                workingShift.setSelected(isChecked);
                if (isChecked) {
                    mCheckedWorkingShifts.add(workingShift);
                } else {
                    if (mCheckedWorkingShifts.contains(workingShift)) {
                        mCheckedWorkingShifts.remove(workingShift);
                    }
                }
                notifySectionItemChanged(sectionIndex, itemIndex);
            }
        });

        itemViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mReadOnly) {
                    workingShift.setSelected(!workingShift.isSelected());
                    if (workingShift.isSelected()) {
                        mCheckedWorkingShifts.add(workingShift);
                    } else {
                        if (mCheckedWorkingShifts.contains(workingShift)) {
                            mCheckedWorkingShifts.remove(workingShift);
                        }
                    }
                    notifySectionItemChanged(sectionIndex, itemIndex);
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex, int headerType) {
        WorkingDate workingDate = mWorkingDates.get(sectionIndex);
        WorkingDateViewHolder hvh = (WorkingDateViewHolder) viewHolder;
        DateTime workingDateTime = DateTimeUtils.stringToDateTime(workingDate.getDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
        hvh.textWorkingDate.setText(DateTimeUtils.dateTimeToString(
                workingDateTime, PublicConstants.WORKING_SCHEDULER_HEADER_FMT));
        hvh.textWorkingDate.setBackgroundResource(R.color.main_text_color);
        hvh.titleDivider.setBackgroundResource(R.color.main_text_color);
        hvh.imgAddEmp.setVisibility(View.GONE);
    }
}
