package com.worktool.logwork.schedule;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.worktool.logwork.R;

import org.zakariya.stickyheaders.SectioningAdapter;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

public class WorkingDateViewHolder extends SectioningAdapter.HeaderViewHolder {

    public View rootView;
    public BootstrapLabel textWorkingDate;
    public View titleDivider;
    public TextView textWorkSessTime;
    public ImageView imgAddEmp;

    public WorkingDateViewHolder(View itemView) {
        super(itemView);
        rootView = itemView;
        textWorkingDate = itemView.findViewById(R.id.text_working_date);
        titleDivider = itemView.findViewById(R.id.title_divider);
        textWorkSessTime = itemView.findViewById(R.id.text_work_time_total);
        imgAddEmp = itemView.findViewById(R.id.img_add_emp);
    }
}
