package com.worktool.logwork.schedule;

import android.text.TextUtils;
import android.util.Log;

import com.worktool.logwork.PublicConstants;
import com.worktool.logwork.admin.schedule.UserSchedulerWeek;
import com.worktool.logwork.admin.schedule.WorkSchedulerDate;
import com.worktool.logwork.admin.schedule.UserSchedulerShift;
import com.worktool.logwork.util.DateTimeUtils;
import com.worktool.serverapi.logwork.user.scheduler.SchedulerData;
import com.worktool.serverapi.logwork.user.scheduler.UpdateUserSchedulerData;
import com.worktool.serverapi.logwork.user.scheduler.UserSchedulerData;
import com.worktool.serverapi.logwork.user.scheduler.WeekSchedulerResponse;
import com.worktool.serverapi.logwork.user.session.WorkSessionInfo;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by THAIHOANG on 10/8/2017.
 */

public class WorkingScheduleFactory {
    public static List<WorkingShift> createDefaultWorkingShifts(DateTime startDayOfWeek, List<WorkSessionInfo> workSessionInfos) {
        List<WorkingShift> workingShifts = new ArrayList<>();

        for (int dayOffset=0; dayOffset<7; dayOffset++) {
            DateTime dayOfWeek = startDayOfWeek.plusDays(dayOffset);

            for (WorkSessionInfo workSessionInfo : workSessionInfos) {
                // Add morning shift
                WorkingShift workingShift = new WorkingShift();
                workingShift.setShiftName(workSessionInfo.getName());
                workingShift.setShiftStartTime(workSessionInfo.getStartTime());
                workingShift.setShiftEndTime(workSessionInfo.getEndTime());
                workingShift.setShiftDate(DateTimeUtils.dateTimeToString(dayOfWeek, PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC));
                workingShifts.add(workingShift);
            }
        }

        return workingShifts;
    }

    public static List<WorkingDate> groupWorkingShift(List<WorkingShift> workingShifts) {
        List<WorkingDate> workingDates = new ArrayList<>();
        // sort people into buckets by the first letter of last name
        Map<String, WorkingDate> mapping = new HashMap<>();
        for (WorkingShift workingShift : workingShifts) {
            DateTime shiftDateTime = DateTimeUtils.stringToDateTime(workingShift.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            String shiftDate = DateTimeUtils.dateTimeToString(shiftDateTime, PublicConstants.WORKING_SCHEDULER_DAY_FMT);
            WorkingDate workingDate = mapping.get(shiftDate);
            if (workingDate == null) {
                // Group not exist, create new one
                workingDate = new WorkingDate();
                workingDate.setDate(workingShift.getShiftDate());
                workingDates.add(workingDate);
            }
            workingDate.getShifts().add(workingShift);
            mapping.put(shiftDate, workingDate);
        }
        mapping.clear();
        return workingDates;
    }

    public static SchedulerData buildSchedulerData(List<WorkingShift> workingShifts) {
        SchedulerData schedulerData = new SchedulerData();
        if (workingShifts != null && workingShifts.size() > 0) {
            for (WorkingShift workingShift : workingShifts) {
                DateTime shiftDate = DateTimeUtils.stringToDateTime(workingShift.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                switch (shiftDate.getDayOfWeek()) {
                    case DateTimeConstants.MONDAY:
                        schedulerData.addMonShifts(workingShift.getShiftName());
                        break;
                    case DateTimeConstants.TUESDAY:
                        schedulerData.addTueShifts(workingShift.getShiftName());
                        break;
                    case DateTimeConstants.WEDNESDAY:
                        schedulerData.addWedShifts(workingShift.getShiftName());
                        break;
                    case DateTimeConstants.THURSDAY:
                        schedulerData.addThuShifts(workingShift.getShiftName());
                        break;
                    case DateTimeConstants.FRIDAY:
                        schedulerData.addFriShifts(workingShift.getShiftName());
                        break;
                    case DateTimeConstants.SATURDAY:
                        schedulerData.addSatShifts(workingShift.getShiftName());
                        break;
                    case DateTimeConstants.SUNDAY:
                        schedulerData.addSunShifts(workingShift.getShiftName());
                        break;
                    default:
                        break;
                }
            }
        }
        return schedulerData;
    }

    public static List<WorkingShift> buildWorkingShifts(SchedulerData schedulerData, DateTime startDayOfWeek, List<WorkSessionInfo> workSessionInfos) {
        List<WorkingShift> workingShifts = new ArrayList<>();

        // Build Monday shifts
        if (schedulerData.getMonShifts() != null && schedulerData.getMonShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getMonShifts();
            for (String shiftName : shiftNames) {
                workingShifts.add(createWorkingShift(startDayOfWeek, shiftName, workSessionInfos));
            }
        }

        // Build Tuesday shifts
        if (schedulerData.getTueShifts() != null && schedulerData.getTueShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getTueShifts();
            for (String shiftName : shiftNames) {
                workingShifts.add(createWorkingShift(startDayOfWeek.plusDays(1), shiftName, workSessionInfos));
            }
        }

        // Build Wednesday shifts
        if (schedulerData.getWedShifts() != null && schedulerData.getWedShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getWedShifts();
            for (String shiftName : shiftNames) {
                workingShifts.add(createWorkingShift(startDayOfWeek.plusDays(2), shiftName, workSessionInfos));
            }
        }

        // Build Thusday shifts
        if (schedulerData.getThuShifts() != null && schedulerData.getThuShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getThuShifts();
            for (String shiftName : shiftNames) {
                workingShifts.add(createWorkingShift(startDayOfWeek.plusDays(3), shiftName, workSessionInfos));
            }
        }

        // Build Friday shifts
        if (schedulerData.getFriShifts() != null && schedulerData.getFriShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getFriShifts();
            for (String shiftName : shiftNames) {
                workingShifts.add(createWorkingShift(startDayOfWeek.plusDays(4), shiftName, workSessionInfos));
            }
        }

        // Build Saturday shifts
        if (schedulerData.getSatShifts() != null && schedulerData.getSatShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getSatShifts();
            for (String shiftName : shiftNames) {
                workingShifts.add(createWorkingShift(startDayOfWeek.plusDays(5), shiftName, workSessionInfos));
            }
        }

        // Build Sunday shifts
        if (schedulerData.getSunShifts() != null && schedulerData.getSunShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getSunShifts();
            for (String shiftName : shiftNames) {
                workingShifts.add(createWorkingShift(startDayOfWeek.plusDays(6), shiftName, workSessionInfos));
            }
        }

        return workingShifts;
    }

    public static WorkingShift createWorkingShift(DateTime shiftDate, String shiftName, List<WorkSessionInfo> workSessionInfos) {
        WorkingShift workingShift = new WorkingShift();
        workingShift.setShiftDate(DateTimeUtils.dateTimeToString(shiftDate, PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC));
        workingShift.setShiftName(shiftName);
        if (!TextUtils.isEmpty(shiftName)) {
            for (WorkSessionInfo workSessionInfo : workSessionInfos) {
                if (shiftName.equals(workSessionInfo.getName())) {
                    workingShift.setShiftStartTime(workSessionInfo.getStartTime());
                    workingShift.setShiftEndTime(workSessionInfo.getEndTime());
                    break;
                }
            }
        }
        return workingShift;
    }

    public static void updateWorkingShifts(List<WorkingShift> defaultWorkingShifts, List<WorkingShift> selectedWorkingShifts) {
        if (defaultWorkingShifts != null && defaultWorkingShifts.size() > 0 &&
                selectedWorkingShifts != null && selectedWorkingShifts.size() > 0) {
            for (WorkingShift selectedWorkingShift : selectedWorkingShifts) {
                DateTime selectedWorkingShiftDate = DateTimeUtils.stringToDateTime(selectedWorkingShift.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                for (WorkingShift defaultWorkingShift : defaultWorkingShifts) {
                    DateTime shiftDate = DateTimeUtils.stringToDateTime(defaultWorkingShift.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                    if (shiftDate.getDayOfWeek() ==
                            selectedWorkingShiftDate.getDayOfWeek() &&
                            defaultWorkingShift.getShiftName().equalsIgnoreCase(selectedWorkingShift.getShiftName())) {
                        defaultWorkingShift.setSelected(true);
                    }
                }
            }
        }
    }

    public static List<UserSchedulerShift> createWorkSchedulerShifts(WeekSchedulerResponse weekSchedulerRes) {
        List<UserSchedulerShift> userSchedulerShifts = new ArrayList<>();
        List<UserSchedulerData> userSchedulerDatas = weekSchedulerRes.getData();
        for (UserSchedulerData userSchedulerData : userSchedulerDatas) {
            SchedulerData schedulerDatas = userSchedulerData.getUserWeekScheduler();
            if (schedulerDatas != null) {
//                Log.d(PublicConstants.LOG_TAG, "Create work scheduler, weekId? " + weekSchedulerRes.getWeekId());
                DateTime startDayOfWeek = DateTimeUtils.stringToDateTime(weekSchedulerRes.getWeekId(), PublicConstants.WEEK_ID_FMT);
                List<UserSchedulerShift> userSchdlrShifts = buildWorkSchedulerShifts(userSchedulerData,
                        schedulerDatas, startDayOfWeek);
                if (userSchdlrShifts != null && userSchdlrShifts.size() > 0) {
                    userSchedulerShifts.addAll(userSchdlrShifts);
                }
            }
        }
        return userSchedulerShifts;
    }

    public static List<UserSchedulerShift> filterWorkSchedulerShifts(List<UserSchedulerShift> userSchedulerShifts, WorkSessionInfo workSessionInfo) {
        List<UserSchedulerShift> filterUserSchedulerShifts = new ArrayList<>();
        Log.d(PublicConstants.LOG_TAG, "Filter work scheduler shift, name? " + workSessionInfo.getName());
        if (userSchedulerShifts != null && userSchedulerShifts.size() > 0) {
            for (UserSchedulerShift userSchedulerShift : userSchedulerShifts) {
                if (workSessionInfo.getName().equalsIgnoreCase(userSchedulerShift.getShiftName())) {
                    filterUserSchedulerShifts.add(userSchedulerShift);
                }
            }
        }
        return filterUserSchedulerShifts;
    }

    public static List<UserSchedulerShift> buildWorkSchedulerShifts(UserSchedulerData userSchedulerData, SchedulerData schedulerData, DateTime startDayOfWeek) {
        List<UserSchedulerShift> workingShifts = new ArrayList<>();

        // Build Monday shifts
        if (schedulerData.getMonShifts() != null && schedulerData.getMonShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getMonShifts();
            for (String shiftName : shiftNames) {
                UserSchedulerShift userSchedulerShift = createWorkSchdlrShift(startDayOfWeek, shiftName);
                userSchedulerShift.setUserId(userSchedulerData.getUserId());
                userSchedulerShift.setFullName(userSchedulerData.getFullName());
                workingShifts.add(userSchedulerShift);
            }
        }

        // Build Tuesday shifts
        if (schedulerData.getTueShifts() != null && schedulerData.getTueShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getTueShifts();
            for (String shiftName : shiftNames) {
                UserSchedulerShift userSchedulerShift = createWorkSchdlrShift(startDayOfWeek.plusDays(1), shiftName);
                userSchedulerShift.setUserId(userSchedulerData.getUserId());
                userSchedulerShift.setFullName(userSchedulerData.getFullName());
                workingShifts.add(userSchedulerShift);
            }
        }

        // Build Wednesday shifts
        if (schedulerData.getWedShifts() != null && schedulerData.getWedShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getWedShifts();
            for (String shiftName : shiftNames) {
                UserSchedulerShift userSchedulerShift = createWorkSchdlrShift(startDayOfWeek.plusDays(2), shiftName);
                userSchedulerShift.setUserId(userSchedulerData.getUserId());
                userSchedulerShift.setFullName(userSchedulerData.getFullName());
                workingShifts.add(userSchedulerShift);
            }
        }

        // Build Thusday shifts
        if (schedulerData.getThuShifts() != null && schedulerData.getThuShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getThuShifts();
            for (String shiftName : shiftNames) {
                UserSchedulerShift userSchedulerShift = createWorkSchdlrShift(startDayOfWeek.plusDays(3), shiftName);
                userSchedulerShift.setUserId(userSchedulerData.getUserId());
                userSchedulerShift.setFullName(userSchedulerData.getFullName());
                workingShifts.add(userSchedulerShift);
            }
        }

        // Build Friday shifts
        if (schedulerData.getFriShifts() != null && schedulerData.getFriShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getFriShifts();
            for (String shiftName : shiftNames) {
                UserSchedulerShift userSchedulerShift = createWorkSchdlrShift(startDayOfWeek.plusDays(4), shiftName);
                userSchedulerShift.setUserId(userSchedulerData.getUserId());
                userSchedulerShift.setFullName(userSchedulerData.getFullName());
                workingShifts.add(userSchedulerShift);
            }
        }

        // Build Saturday shifts
        if (schedulerData.getSatShifts() != null && schedulerData.getSatShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getSatShifts();
            for (String shiftName : shiftNames) {
                UserSchedulerShift userSchedulerShift = createWorkSchdlrShift(startDayOfWeek.plusDays(5), shiftName);
                userSchedulerShift.setUserId(userSchedulerData.getUserId());
                userSchedulerShift.setFullName(userSchedulerData.getFullName());
                workingShifts.add(userSchedulerShift);
            }
        }

        // Build Sunday shifts
        if (schedulerData.getSunShifts() != null && schedulerData.getSunShifts().size() > 0) {
            List<String> shiftNames = schedulerData.getSunShifts();
            for (String shiftName : shiftNames) {
                UserSchedulerShift userSchedulerShift = createWorkSchdlrShift(startDayOfWeek.plusDays(6), shiftName);
                userSchedulerShift.setUserId(userSchedulerData.getUserId());
                userSchedulerShift.setFullName(userSchedulerData.getFullName());
                workingShifts.add(userSchedulerShift);
            }
        }

        return workingShifts;
    }

    public static UserSchedulerShift createWorkSchdlrShift(DateTime shiftDate, String shiftName) {
        UserSchedulerShift workingShift = new UserSchedulerShift();
        workingShift.setShiftDate(DateTimeUtils.dateTimeToString(shiftDate, PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC));
        workingShift.setShiftName(shiftName);
        return workingShift;
    }

    public static List<WorkSchedulerDate> groupWorkSchedulerShiftsByDayShift(String weekId, List<UserSchedulerShift> userSchedulerShifts, WorkSessionInfo workSessionInfo) {
        // Create work scheduler data for all days of week
        List<WorkSchedulerDate> workSchedulerDates = new ArrayList<>();
        Map<String, WorkSchedulerDate> mapping = new HashMap<>();

        DateTime weekDate = DateTimeUtils.stringToDateTime(weekId, PublicConstants.WEEK_ID_FMT);
        DateTime startDayOfWeek = DateTimeUtils.getStartDayOfWeek(weekDate);
        Log.d(PublicConstants.LOG_TAG, "groupWorkSchedulerShiftsByDayShift, weekId? " + weekId +
                ", start day of week? " + DateTimeUtils.dateTimeToString(startDayOfWeek, PublicConstants.USER_PROFILE_DATE_FMT));
        for (int i=0; i<7; i++) {
            DateTime weekDay = startDayOfWeek.plusDays(i);
            WorkSchedulerDate newWorkingDate = new WorkSchedulerDate();
            String newWorkingDateKey = DateTimeUtils.dateTimeToString(weekDay, PublicConstants.WORKING_SCHEDULER_DAY_FMT);
            String newWorkingDateStr = DateTimeUtils.dateTimeToString(weekDay, PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            newWorkingDate.setDate(newWorkingDateStr);
            if (workSessionInfo != null) {
                newWorkingDate.setTotalTime(workSessionInfo.getTotalAllowWorkHours());
            }
            workSchedulerDates.add(newWorkingDate);
            mapping.put(newWorkingDateKey, newWorkingDate);
        }

        for (UserSchedulerShift workingShift : userSchedulerShifts) {
            DateTime shiftDateTime = DateTimeUtils.stringToDateTime(workingShift.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
            String shiftDateStr = DateTimeUtils.dateTimeToString(shiftDateTime, PublicConstants.WORKING_SCHEDULER_DAY_FMT);
            WorkSchedulerDate workingDate = mapping.get(shiftDateStr);
            if (workingDate == null) {
                // Group not exist, create new one
                workingDate = new WorkSchedulerDate();
                workingDate.setDate(workingShift.getShiftDate());
                if (workSessionInfo != null) {
                    workingDate.setTotalTime(workSessionInfo.getTotalAllowWorkHours());
                }
                workSchedulerDates.add(workingDate);
            }
            workingDate.getShifts().add(workingShift);

            int registerTime = workingDate.getShifts().size() * PublicConstants.WORKING_HOURS_PER_DAY_MAX;
            workingDate.setRegisteredTime(registerTime);
            mapping.put(shiftDateStr, workingDate);
        }
        mapping.clear();
        return workSchedulerDates;
    }

    public static List<UserSchedulerWeek> groupWorkSchedulerShiftsByUser(List<UserSchedulerShift> userSchedulerShifts) {
        List<UserSchedulerWeek> userSchedulerWeeks = new ArrayList<>();
        if (userSchedulerShifts != null && userSchedulerShifts.size() > 0) {
            Map<String, UserSchedulerWeek> mapping = new HashMap<>();
            for (UserSchedulerShift userSchedulerShift : userSchedulerShifts) {
                DateTime shiftDateTime = DateTimeUtils.stringToDateTime(userSchedulerShift.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                UserSchedulerWeek userSchedulerWeek = mapping.get(userSchedulerShift.getUserId());
                if (userSchedulerWeek == null) {
                    // Group not exist, create new one
                    userSchedulerWeek = new UserSchedulerWeek();
                    userSchedulerWeek.setUserId(userSchedulerShift.getUserId());
                    userSchedulerWeek.setFullName(userSchedulerShift.getFullName());
                    userSchedulerWeek.setWeekId(DateTimeUtils.dateTimeToString(shiftDateTime, PublicConstants.WEEK_ID_FMT));
                    userSchedulerWeeks.add(userSchedulerWeek);
                }
                userSchedulerWeek.getWorkScheduler().add(userSchedulerShift);
                mapping.put(userSchedulerWeek.getUserId(), userSchedulerWeek);
            }
            mapping.clear();
        }
        return userSchedulerWeeks;
    }

    public static List<UpdateUserSchedulerData> createUpdateWorkSchedulerDatas(List<UserSchedulerShift> userSchedulerShifts) {
        List<UpdateUserSchedulerData> userSchedulerDatas = new ArrayList<>();
        List<UserSchedulerWeek> userSchedulerWeeks = groupWorkSchedulerShiftsByUser(userSchedulerShifts);
        for (UserSchedulerWeek userSchedulerWeek : userSchedulerWeeks) {
            userSchedulerDatas.add(buildUpdateUserSchedulerData(userSchedulerWeek));
        }
        return userSchedulerDatas;
    }

    public static UpdateUserSchedulerData buildUpdateUserSchedulerData(UserSchedulerWeek userSchedulerWeek) {
        UpdateUserSchedulerData updateUserSchdlrData = new UpdateUserSchedulerData();
        updateUserSchdlrData.setUserId(userSchedulerWeek.getUserId());
        updateUserSchdlrData.setFullName(userSchedulerWeek.getFullName());
        updateUserSchdlrData.setWeekId(userSchedulerWeek.getWeekId());
        SchedulerData schedulerData = new SchedulerData();
        List<UserSchedulerShift> userSchedulerShifts = userSchedulerWeek.getWorkScheduler();
        if (userSchedulerShifts != null && userSchedulerShifts.size() > 0) {
            for (UserSchedulerShift userSchedulerShift : userSchedulerShifts) {
                DateTime shiftDate = DateTimeUtils.stringToDateTime(userSchedulerShift.getShiftDate(), PublicConstants.USER_PROFILE_DATE_FMT, DateTimeZone.UTC);
                switch (shiftDate.getDayOfWeek()) {
                    case DateTimeConstants.MONDAY:
                        schedulerData.addMonShifts(userSchedulerShift.getShiftName());
                        break;
                    case DateTimeConstants.TUESDAY:
                        schedulerData.addTueShifts(userSchedulerShift.getShiftName());
                        break;
                    case DateTimeConstants.WEDNESDAY:
                        schedulerData.addWedShifts(userSchedulerShift.getShiftName());
                        break;
                    case DateTimeConstants.THURSDAY:
                        schedulerData.addThuShifts(userSchedulerShift.getShiftName());
                        break;
                    case DateTimeConstants.FRIDAY:
                        schedulerData.addFriShifts(userSchedulerShift.getShiftName());
                        break;
                    case DateTimeConstants.SATURDAY:
                        schedulerData.addSatShifts(userSchedulerShift.getShiftName());
                        break;
                    case DateTimeConstants.SUNDAY:
                        schedulerData.addSunShifts(userSchedulerShift.getShiftName());
                        break;
                    default:
                        break;
                }
            }
        }
        updateUserSchdlrData.setWorkScheduler(schedulerData);
        return updateUserSchdlrData;
    }
}
