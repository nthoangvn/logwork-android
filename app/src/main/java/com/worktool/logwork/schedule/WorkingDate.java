package com.worktool.logwork.schedule;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THAIHOANG on 10/8/2017.
 */

public class WorkingDate {
    private String mDate;
    private List<WorkingShift> mShifts;

    public WorkingDate() {
        mShifts = new ArrayList<>();
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public List<WorkingShift> getShifts() {
        return mShifts;
    }

    public void setShifts(List<WorkingShift> mShifts) {
        this.mShifts = mShifts;
    }
}
