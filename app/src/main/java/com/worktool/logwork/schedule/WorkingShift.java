package com.worktool.logwork.schedule;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

public class WorkingShift {
    private String mShiftDate;
    private String mShiftName;
    private String mShiftStartTime;
    private String mShiftEndTime;
    private boolean mSelected;

    public String getShiftName() {
        return mShiftName;
    }

    public void setShiftName(String mShiftName) {
        this.mShiftName = mShiftName;
    }

    public String getShiftStartTime() {
        return mShiftStartTime;
    }

    public void setShiftStartTime(String mShiftTime) {
        this.mShiftStartTime = mShiftTime;
    }

    public String getShiftEndTime() {
        return mShiftEndTime;
    }

    public void setShiftEndTime(String mShiftEndTime) {
        this.mShiftEndTime = mShiftEndTime;
    }

    public String getShiftDate() {
        return mShiftDate;
    }

    public void setShiftDate(String shiftDate) {
        mShiftDate = shiftDate;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean mSelected) {
        this.mSelected = mSelected;
    }
}
