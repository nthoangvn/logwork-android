package com.worktool.logwork;

/**
 * Created by THAIHOANG on 10/7/2017.
 */

public class PublicConstants {
    public static final String LOG_TAG = "LogWork";

    public static final long ONE_HOUR_MS = 3600 * 1000;
    public static final long ONE_MINUTE_MS = 60 * 1000;
    public static final int WORKING_HOURS_PER_DAY_MAX = 8;

    public static final String USER_PROFILE_DATE_FMT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String BIRTHDAY_DATE_FMT = "dd-MM-yyyy";
    public static final String GENERAL_DATE_FMT = "dd-MM-yyyy";
    public static final String GENERAL_SLASH_DATE_FMT = "dd/MM/yyyy";
    public static final String DAY_OF_MONTH_FMT = "dd";
    public static final String WORK_SESSION_TIME_FMT = "HH:mm";
    public static final String WEEK_ID_FMT = "'W'w-yyyy";
    public static final String MONTH_ID_FMT = "MM-yyyy";

    public static final String WORK_SESSION_TITLE_DATE_FMT = "MMMMM";
    public static final String WORK_SESSION_LIST_DATE_FMT = "MMMMM/yyyy";
    public static final String WEEK_SCHEDULER_BUTTON_DAY_FMT = "dd/MM";
    public static final String WORKING_SCHEDULER_DAY_FMT = "dd-MM-yyyy";
    public static final String WORKING_SCHEDULER_HEADER_FMT = "EEEE, dd-MM-yyyy";
    public static final String NOTIFICATION_DATE_FMT = "dd-MM-yyyy";

    public static final int PASSWORD_LENGTH_MIN = 3;

    public static final String PREFS_APP_SETTINGS = "app_settings";
    public static final String PREFS_USER_TOKEN = "String_user_token";
    public static final String PREFS_USER_EMAIL = "String_user_email";
    public static final String PREFS_USER_ID = "String_user_id";
    public static final String PREFS_WORK_SESSION_ID = "String_work_session_id";

    public static final String PREFS_FIREBASE_MESSAGING_TOKEN = "String_firebase_messaging_token";

    // Notifications type
    public static final String NOTIF_TYPE_COMMON_OLD = "thong-bao";
    public static final String NOTIF_TYPE_COMMON = "commonNotification";
    public static final String NOTIF_TYPE_RULE = "ruleNotification";
    public static final String NOTIF_TYPE_PROMOTION = "promotionNotification";
    public static final String[] NOTIF_TYPES = {NOTIF_TYPE_COMMON, NOTIF_TYPE_PROMOTION, NOTIF_TYPE_RULE};

    // User roles
    public static final String USER_ROLE_ADMIN = "admin";
    public static final String USER_ROLE_MANAGER = "manager";

    // Recent activity actions
    public static final String ACTION_CHECKIN = "checkin";
    public static final String ACTION_CHECKOUT = "checkout";

    // Firebase notification key
    public static final int FIREBASE_NOTIF_ID = 0;
    public static final String FIREBASE_NOTIF_TITLE = "title";
    public static final String FIREBASE_NOTIF_BODY = "body";
}
