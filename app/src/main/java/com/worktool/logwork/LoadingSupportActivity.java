package com.worktool.logwork;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import dmax.dialog.SpotsDialog;

/**
 * Created by THAIHOANG on 11/3/2017.
 */

public abstract class LoadingSupportActivity extends AppCompatActivity {
    private AlertDialog mProgressDialog;

    public void showLoading(String message) {
//            mProgressDialog = new ProgressDialog(this);
//            mProgressDialog.setCancelable(false);
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.setMessage(message);
        } else {
            mProgressDialog = new SpotsDialog(this, message);
            mProgressDialog.setCancelable(false);
            try {
                mProgressDialog.show();
            } catch (Exception e) {
            }
        }
    }

    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            try {
                mProgressDialog.dismiss();
            } catch (Exception e) {
            }
        }
    }
}
