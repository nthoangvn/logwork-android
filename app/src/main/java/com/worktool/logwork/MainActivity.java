package com.worktool.logwork;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.worktool.logwork.admin.profile.UserManagementFragment;
import com.worktool.logwork.admin.schedule.WorkSchedulerManagementFragment;
import com.worktool.logwork.image.glide.GlideApp;
import com.worktool.logwork.notifications.NotificationFragment;
import com.worktool.logwork.notifications.RuleFragment;
import com.worktool.logwork.schedule.RegisterWorkSchedulerFragment;
import com.worktool.logwork.session.WorkSessionFragment;
import com.worktool.logwork.user.activity.RecentActivityFragment;
import com.worktool.logwork.user.info.UserProfileActivity;
import com.worktool.logwork.user.login.LoginActivity;
import com.worktool.logwork.util.DeviceUtils;
import com.worktool.logwork.util.ProfileUtils;
import com.worktool.logwork.util.UrlUtils;
import com.worktool.serverapi.logwork.LogWorkApi;
import com.worktool.serverapi.logwork.user.UserProfile;
import com.worktool.serverapi.logwork.user.checkin.StartWorkSessRequest;
import com.worktool.serverapi.logwork.user.checkin.UserStartWorkSessResponse;
import com.worktool.serverapi.logwork.user.checkout.StopWorkSessResponse;

import org.joda.time.DateTime;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.worktool.logwork.util.DateTimeUtils.dateTimeToString;


public class MainActivity extends LoadingSupportActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final int REQUEST_ACCESS_LOCATION_PERMISSION = 1;
    private static final int ENABLE_LOCATION_SERVICE_REQUEST = 2;
    private static final int REQUEST_CHECK_LOCATION_SETTINGS = 3;

    private SharedPreferences mAppSettings;
    private UserProfile mUserProfile;
//    private ProgressDialog mProgressDialog;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private boolean mRequestingLocationUpdates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(PublicConstants.LOG_TAG, "MainActivity onCreate");
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        checkGooglePlayServices();

        mAppSettings = getSharedPreferences(PublicConstants.PREFS_APP_SETTINGS, Context.MODE_PRIVATE);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
//        setupLocationService();
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    // ...
//                    Log.d(PublicConstants.LOG_TAG, "Location result: lat " + location.getLatitude() + ", long " + location.getLongitude());
                }
            }
        };
        createLocationRequest();
        checkLocationSettings();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.setVisibility(View.GONE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        setupDrawerLayout();
    }

    private void checkGooglePlayServices() {
        if (!isGooglePlayServicesAvailable(this)) {
            GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this);
        }
    }

    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            if(googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ENABLE_LOCATION_SERVICE_REQUEST:
                    break;
                case REQUEST_CHECK_LOCATION_SETTINGS:
                    checkLocationSettings();
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_LOCATION_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Log.d(PublicConstants.LOG_TAG, "Granted, location permission denied");
                        return;
                    }
                    checkLocationSettings();
                } else {
                    Log.d(PublicConstants.LOG_TAG, "Location permission denied");
                    Toast.makeText(this, R.string.request_location_permission_denied, Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }

    }

    private LocationRequest mLocationRequest;
    // The minimum distance to change updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 0; // 1 minute

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void checkLocationSettings() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(PublicConstants.LOG_TAG, "Check location settings, need to request location permission");
            String[] locationPermissions = new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            };
            ActivityCompat.requestPermissions(this, locationPermissions, REQUEST_ACCESS_LOCATION_PERMISSION);
            return;
        }

        // Check Location settings
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                Log.d(PublicConstants.LOG_TAG, "Check location updates setting success");
                mRequestingLocationUpdates = true;
                startLocationUpdates();
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                Log.d(PublicConstants.LOG_TAG, "Check location updates setting fail, status code? " + statusCode);
                switch (statusCode) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        Log.d(PublicConstants.LOG_TAG, "Location settings required");
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(MainActivity.this,
                                    REQUEST_CHECK_LOCATION_SETTINGS);
                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        Log.d(PublicConstants.LOG_TAG, "Location settings is unavailable");
                        break;
                }
            }
        });
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(PublicConstants.LOG_TAG, "Start location updates, need to request location permission");
            String[] locationPermissions = new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            };
            ActivityCompat.requestPermissions(this, locationPermissions, REQUEST_ACCESS_LOCATION_PERMISSION);
            return;
        }

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    private void getCurrentLocation() {
        Log.d(PublicConstants.LOG_TAG, "Get current location");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] locationPermissions = new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            };
            ActivityCompat.requestPermissions(this, locationPermissions, REQUEST_ACCESS_LOCATION_PERMISSION);
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            Log.d(PublicConstants.LOG_TAG, "On get last location success: lat " + location.getLatitude() + ", long " + location.getLongitude());
                            startWorkSessionWithLocation(location);
                        } else {
                            Log.d(PublicConstants.LOG_TAG, "On get last location fail");
                            startWorkSessionWithLocation(location);
                        }
                    }
                });
    }


    private void loadUserInfo() {
        Log.d(PublicConstants.LOG_TAG, "Load user info");
        showLoading(getString(R.string.loading_user_data));
        String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
        if (!TextUtils.isEmpty(userToken)) {
            Call<UserProfile> getUserInfoCall = LogWorkApi.getInstance().getService()
                    .getOwnInfo(userToken);
            getUserInfoCall.enqueue(new Callback<UserProfile>() {
                @Override
                public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                    Log.d(PublicConstants.LOG_TAG, "Load user info completed, success? " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        hideLoading();
                        mUserProfile = response.body();
                        updateUserInfo();

                        // Open notification fragment by default
                        openNotificationFragment();
                    } else {
                        Toast.makeText(MainActivity.this, R.string.loading_user_data_fail, Toast.LENGTH_LONG).show();
                        hideLoading();
                        goToLoginScreen();
                    }
                }

                @Override
                public void onFailure(Call<UserProfile> call, Throwable t) {
                    t.printStackTrace();
                    Log.d(PublicConstants.LOG_TAG, "Load user info failed, retry");
                    hideLoading();
                    Toast.makeText(MainActivity.this, R.string.loading_user_data_fail, Toast.LENGTH_LONG).show();
                    goToLoginScreen();
                }
            });
        }
    }

    private void goToLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void updateUserInfo() {
        if (mUserProfile != null) {
            ImageView imgProfile = findViewById(R.id.img_profile);
            String avatarUrl = UrlUtils.getAvatarDomain() + mUserProfile.getProfilePicture();
            GlideApp.with(this)
                    .load(avatarUrl)
                    .error(R.drawable.ic_avatar_default)
                    .fitCenter()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(imgProfile);

            TextView textName = findViewById(R.id.text_full_name);
            textName.setText(mUserProfile.getFullName() + " - " + mUserProfile.getEmpId());
            TextView textEmail = findViewById(R.id.text_email);
            textEmail.setText(mUserProfile.getEmpRole());

            // Hide management layout if not admin
            List<String> roles = mUserProfile.getRoles();
            boolean isAdminOrManager = ProfileUtils.hasManagerPermission(roles);
            Log.d(PublicConstants.LOG_TAG, "User roles? " + roles + ", has manager permission? " + isAdminOrManager);

            View layoutCurrWorkSess = findViewById(R.id.layout_curr_work_sess);
            View layoutAdmin = findViewById(R.id.layout_left_bar_management);
            View layoutEmp = findViewById(R.id.layout_left_bar_employee);
            BootstrapButton workSessionBtn = findViewById(R.id.btn_work_session);
            if (isAdminOrManager) {
                if (layoutEmp != null) {
                    layoutEmp.setVisibility(View.GONE);
                }
                if (layoutAdmin != null) {
                    layoutAdmin.setVisibility(View.VISIBLE);
                }
                if (layoutCurrWorkSess != null) {
                    layoutCurrWorkSess.setVisibility(View.GONE);
                }
                if (workSessionBtn != null) {
                    workSessionBtn.setVisibility(View.GONE);
                }
            } else {
                if (layoutEmp != null) {
                    layoutEmp.setVisibility(View.VISIBLE);
                }
                if (layoutAdmin != null) {
                    layoutAdmin.setVisibility(View.GONE);
                }
                if (layoutCurrWorkSess != null) {
                    layoutCurrWorkSess.setVisibility(View.VISIBLE);
                }
                if (workSessionBtn != null) {
                    workSessionBtn.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void setupDrawerLayout() {
        View workSessionBtn = findViewById(R.id.btn_work_session);
        workSessionBtn.setOnClickListener(this);
        invalidateWorkSessionButton();

        View imgProfile = findViewById(R.id.img_profile);
        imgProfile.setOnClickListener(this);

        View textWorkSessMonthly = findViewById(R.id.text_work_session_monthly);
        textWorkSessMonthly.setOnClickListener(this);
        View textRegWorkSchdlr = findViewById(R.id.text_register_scheduler);
        textRegWorkSchdlr.setOnClickListener(this);
        View textInformation = findViewById(R.id.text_information);
        textInformation.setOnClickListener(this);
        View leftBarNotif = findViewById(R.id.text_notification);
        leftBarNotif.setOnClickListener(this);
        View leftBarRecentActs = findViewById(R.id.text_recent_activities);
        leftBarRecentActs.setOnClickListener(this);
        View leftBarRule = findViewById(R.id.text_rules);
        leftBarRule.setOnClickListener(this);
        View leftBarUserList = findViewById(R.id.text_user_list);
        leftBarUserList.setOnClickListener(this);
        View leftBarWeekScheduler = findViewById(R.id.text_work_scheduler);
        leftBarWeekScheduler.setOnClickListener(this);
        View leftBarAdminRules = findViewById(R.id.text_admin_rules);
        leftBarAdminRules.setOnClickListener(this);

        loadUserInfo();
    }

    private void invalidateWorkSessionButton() {
        BootstrapButton workSessionBtn = findViewById(R.id.btn_work_session);
        if (workSessionBtn != null) {
            String sessionId = mAppSettings.getString(PublicConstants.PREFS_WORK_SESSION_ID, null);
            if (!TextUtils.isEmpty(sessionId)) {
                workSessionBtn.setText(R.string.stop_work_session);
                workSessionBtn.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
            } else {
                workSessionBtn.setText(R.string.start_work_session);
                workSessionBtn.setBootstrapBrand(DefaultBootstrapBrand.PRIMARY);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void closeDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openUserProfileActivity() {
        if (mUserProfile != null) {
            Intent intent = new Intent(this, UserProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(UserProfileActivity.EXTRA_USER_PROFILE, mUserProfile);
            startActivity(intent);
        }
    }

    private void stopWorkSession(String sessionId) {
        if (mUserProfile != null) {
            showLoading(getString(R.string.stop_work_session_waiting));
            String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
            if (!TextUtils.isEmpty(userToken)) {
                String userId = mUserProfile.getEmpId();
                Call<StopWorkSessResponse> checkOutResponseCall = LogWorkApi.getInstance().getService()
                        .userStopWorkSession(userId, sessionId, userToken);
                checkOutResponseCall.enqueue(new Callback<StopWorkSessResponse>() {
                    @Override
                    public void onResponse(Call<StopWorkSessResponse> call, Response<StopWorkSessResponse> response) {
                        Log.d(PublicConstants.LOG_TAG, "Check out completed, success? " + response.isSuccessful());
                        // Clear sessionId after checkout
                        hideLoading();
                        if (response.isSuccessful()) {
                            SharedPreferences.Editor editor = mAppSettings.edit();
                            editor.remove(PublicConstants.PREFS_WORK_SESSION_ID);
                            editor.apply();

                            invalidateWorkSessionButton();
                            Toast.makeText(MainActivity.this, R.string.stop_work_session_success, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, R.string.stop_work_session_fail, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<StopWorkSessResponse> call, Throwable t) {
                        Log.d(PublicConstants.LOG_TAG, "Check out failed");
                        t.printStackTrace();
                        hideLoading();
                        Toast.makeText(MainActivity.this, R.string.stop_work_session_fail, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    private void startWorkSession() {
        if (mUserProfile != null) {
            showLoading(getString(R.string.start_work_session_waiting));
            getCurrentLocation();
        } else {
            Log.d(PublicConstants.LOG_TAG, "No user profile!");
        }
    }

    private void startWorkSessionWithLocation(final Location location) {
        if (mUserProfile != null) {
            if (location != null) {
                String userToken = mAppSettings.getString(PublicConstants.PREFS_USER_TOKEN, null);
                if (!TextUtils.isEmpty(userToken)) {
                    String userId = mUserProfile.getEmpId();
                    StartWorkSessRequest startWorkSessReq = new StartWorkSessRequest();
                    startWorkSessReq.setDeviceId(DeviceUtils.getSecureId(getApplicationContext()));
                    startWorkSessReq.setLatitude(location.getLatitude());
                    startWorkSessReq.setLongitude(location.getLongitude());
                    Call<UserStartWorkSessResponse> checkInResponseCall = LogWorkApi.getInstance().getService()
                            .userStartWorkSession(userId, userToken, startWorkSessReq);
                    checkInResponseCall.enqueue(new Callback<UserStartWorkSessResponse>() {
                        @Override
                        public void onResponse(Call<UserStartWorkSessResponse> call, Response<UserStartWorkSessResponse> response) {
                            Log.d(PublicConstants.LOG_TAG, "Start work session completed, success? " + response.isSuccessful());
                            hideLoading();
                            if (response.isSuccessful()) {
                                UserStartWorkSessResponse userStartWorkSessResponse = response.body();
                                String sessionId = userStartWorkSessResponse.getSessionId();
                                SharedPreferences.Editor editor = mAppSettings.edit();
                                editor.putString(PublicConstants.PREFS_WORK_SESSION_ID, sessionId);
                                editor.apply();

                                invalidateWorkSessionButton();
                                Toast.makeText(MainActivity.this, R.string.start_work_session_success, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MainActivity.this, R.string.start_work_session_fail, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<UserStartWorkSessResponse> call, Throwable t) {
                            Log.d(PublicConstants.LOG_TAG, "Start work session failed");
                            t.printStackTrace();
                            hideLoading();
                            Toast.makeText(MainActivity.this, R.string.start_work_session_fail, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } else {
                hideLoading();
                Log.d(PublicConstants.LOG_TAG, "Can't get current device location!");
                Toast.makeText(this, R.string.get_current_location_fail, Toast.LENGTH_SHORT).show();
            }
        } else {
            hideLoading();
            Log.d(PublicConstants.LOG_TAG, "Start work session fail: no user profile!");
        }
    }

    private void openRuleFragment() {
        String rulesTitle = getString(R.string.left_bar_rules);
        setTitle(rulesTitle);
        RuleFragment ruleFrag = new RuleFragment();
        Bundle args = new Bundle();
        args.putParcelable(RuleFragment.EXTRA_USER_PROFILE, mUserProfile);
        ruleFrag.setArguments(args);
        switchToFragment(ruleFrag, false);
    }

    private void openNotificationFragment() {
        String notifTitle = getString(R.string.left_bar_notification);
        setTitle(notifTitle);
        NotificationFragment notifFrag = new NotificationFragment();
        Bundle args = new Bundle();
        args.putParcelable(NotificationFragment.EXTRA_USER_PROFILE, mUserProfile);
        notifFrag.setArguments(args);
        switchToFragment(notifFrag, false);
    }

    private void openRecentActivityFrag() {
        String title = getString(R.string.left_bar_recent_activities);
        setTitle(title);
        RecentActivityFragment frag = new RecentActivityFragment();
        switchToFragment(frag, false);
    }

    private void openUserListFragment() {
        String userListTitle = getString(R.string.left_bar_emp_management);
        setTitle(userListTitle);
        UserManagementFragment userListFrag = new UserManagementFragment();
        Bundle args = new Bundle();
        args.putParcelable(UserManagementFragment.EXTRA_USER_PROFILE, mUserProfile);
        userListFrag.setArguments(args);
        switchToFragment(userListFrag, false);
    }

    private void openWeekSchedulerFragment() {
        String userListTitle = getString(R.string.left_bar_week_scheduler_title);
        setTitle(userListTitle);
        WorkSchedulerManagementFragment workSchdlrFrag = new WorkSchedulerManagementFragment();
        Bundle args = new Bundle();
        args.putParcelable(WorkSchedulerManagementFragment.EXTRA_USER_PROFILE, mUserProfile);
        workSchdlrFrag.setArguments(args);
        switchToFragment(workSchdlrFrag, false);
    }

    public void switchToFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment, null);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void showStopWorkSessConfirm(final String sessionId) {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.stop_work_session))
                .setContentText(getString(R.string.stop_work_session_confirm))
                .setCancelText(getString(R.string.cancel))
                .setConfirmText(getString(R.string.agree))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        // Stop work session
                        stopWorkSession(sessionId);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
    }

    private void showStartWorkSessConfirm() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.start_work_session))
                .setContentText(getString(R.string.start_work_session_confirm))
                .setCancelText(getString(R.string.cancel))
                .setConfirmText(getString(R.string.agree))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        // Start work session
                        startWorkSession();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_work_session:
                String sessionId = mAppSettings.getString(PublicConstants.PREFS_WORK_SESSION_ID, null);
                if (!TextUtils.isEmpty(sessionId)) {
                    showStopWorkSessConfirm(sessionId);
                } else {
                    showStartWorkSessConfirm();
                }
                break;
            case R.id.img_profile:
                closeDrawer();
                openUserProfileActivity();
                break;
            case R.id.text_work_session_monthly:
                closeDrawer();
                if (mUserProfile != null) {
                    String workSessMonth = dateTimeToString(DateTime.now(), PublicConstants.WORK_SESSION_TITLE_DATE_FMT);
                    setTitle(workSessMonth);
                    WorkSessionFragment workSessFrag = new WorkSessionFragment();
                    Bundle args = new Bundle();
                    args.putParcelable(WorkSessionFragment.EXTRA_USER_PROFILE, mUserProfile);
                    workSessFrag.setArguments(args);
                    switchToFragment(workSessFrag, false);
                }
                break;
            case R.id.text_register_scheduler:
                closeDrawer();
                if (mUserProfile != null) {
                    String regWorkSchdlrTitle = getString(R.string.left_bar_register_scheduler);
                    setTitle(regWorkSchdlrTitle);
                    RegisterWorkSchedulerFragment regWorkSchdlrFrag = new RegisterWorkSchedulerFragment();
                    Bundle args = new Bundle();
                    args.putParcelable(RegisterWorkSchedulerFragment.EXTRA_USER_PROFILE, mUserProfile);
                    regWorkSchdlrFrag.setArguments(args);
                    switchToFragment(regWorkSchdlrFrag, false);
                }
                break;

            case R.id.text_information:
            case R.id.text_notification:
                closeDrawer();
                openNotificationFragment();
                break;

            case R.id.text_recent_activities:
                closeDrawer();
                openRecentActivityFrag();
                break;
            case R.id.text_rules:
            case R.id.text_admin_rules:
                closeDrawer();
                openRuleFragment();
                break;
            case R.id.text_user_list:
                closeDrawer();
                openUserListFragment();
                break;
            case R.id.text_work_scheduler:
                closeDrawer();
                openWeekSchedulerFragment();
                break;
        }
    }
}
