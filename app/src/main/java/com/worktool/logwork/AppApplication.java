package com.worktool.logwork;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.worktool.serverapi.logwork.LogWorkApi;

/**
 * Created by THAIHOANG on 10/6/2017.
 */

public class AppApplication extends MultiDexApplication {

    public static AppApplication appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        Log.d(PublicConstants.LOG_TAG, getString(R.string.app_name) + " app created");
        LogWorkApi.init(getApplicationContext());
        TypefaceProvider.registerDefaultIconSets();
    }
}
