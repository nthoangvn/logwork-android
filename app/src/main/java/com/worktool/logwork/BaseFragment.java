package com.worktool.logwork;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import dmax.dialog.SpotsDialog;

/**
 * Created by THAIHOANG on 10/24/2017.
 */

public abstract class BaseFragment extends Fragment {

    protected void showLoading(String message) {
        if (getActivity() != null) {
            ((LoadingSupportActivity)getActivity()).showLoading(message);
        }
    }

    protected void hideLoading() {
        if (getActivity() != null) {
            ((LoadingSupportActivity)getActivity()).hideLoading();
        }
    }

    protected void showToast(String msg, int duration) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), msg, duration).show();
        }
    }
}
